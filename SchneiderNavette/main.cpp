#include "UserInteraction/UI/mainwindow.h"
#include <QApplication>
#include <QDebug>
#include  <iostream>
using namespace std;

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;

    w.showMaximized();
//    QGLFormat glFormat;
//    glFormat.setVersion( 2, 1 );
//    glFormat.setProfile( QGLFormat::CompatibilityProfile );
//    //glFormat.setProfile( QGLFormat::CoreProfile );
//    glFormat.setSampleBuffers( true );


    return a.exec();
}
