#-------------------------------------------------
#
# Project created by QtCreator 2013-09-29T23:45:12
#
#-------------------------------------------------
#define GL_GLEXT_PROTOTYPES
QT       += core gui opengl sql
#QMAKE_CXXFLAGS += -std=c++11
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = SchneiderNavette
TEMPLATE = app

win32{
INCLUDEPATH += H:\DevelopmentEnvironment\opencv\include \
    H:\DevelopmentEnvironment\opencv\build\include \
    C:\LeapDeveloperKit\LeapSDK\include \
    "C:\Program Files\MySQL\MySQL Server 5.6\include"

    LIBS += -LH:\DevelopmentEnvironment\opencv\build\x64\vc10\lib
    LIBS += -LC:\Windows\System32
    LIBS += -L"C:\Program Files\MySQL\MySQL Server 5.6\lib"
    LIBS += -llibmysql
    LIBS += -lmysqlclient -lGLU32 -lOpenGL32
    #LIBS += -LC:\LeapDeveloperKit\LeapSDK\lib\x64
    #LIBS += -lLeap -lLeapd

DEFINES += GL_GLEXT_PROTOTYPES
DEFINES += define WIN32_LEAN_AND_MEAN
}

unix{
INCLUDEPATH += /usr/include/mysql
CONFIG += link_pkgconfig
#LIBS += -lmysqlclient
#INCLUDEPATH += /usr/local/mysql/include/
INCLUDEPATH += /Users/dinayouakim/boost_1_55_0
#LIBS+= -L/usr/local/mysql/lib
LIBS += -L/usr/local/lib
LIBS += -lboost_serialization
#LIBS += -L/usr/lib/Leap -lLeap
#DEFINES += GL_GLEXT_PROTOTYPES
}

SOURCES += main.cpp \
    Data/routepreprocessor.cpp \
    #Data/databasehandler.cpp \
    Model/routerequest.cpp \
    Model/road.cpp \
    Model/rawdata.cpp \
    Model/pointofinterest.cpp \
    Model/node.cpp \
    Processing/pathresult.cpp \
    Processing/pathfinderengine.cpp \
    Processing/metrics.cpp \
    UserInteraction/Rendering/openglrenderer.cpp \
    UserInteraction/Rendering/geometryengine.cpp \
    UserInteraction/UI/poi_ui.cpp \
    UserInteraction/UI/mainwindow.cpp \
    UserInteraction/UI/addnewpoi.cpp
#   UserInteraction/UI/leapuihandler.cpp \
#   UserInteraction/UI/myleapdevicelistener.cpp \
HEADERS  += \
    Data/routepreprocessor.h \
   # Data/databasehandler.h \
    Model/routerequest.h \
    Model/road.h \
    Model/rawdata.h \
    Model/pointofinterest.h \
    Model/node.h \
    Processing/pathresult.h \
    Processing/pathfinderengine.h \
    Processing/metrics.h \
    UserInteraction/Rendering/vertexdata.h \
    UserInteraction/Rendering/openglrenderer.h \
    UserInteraction/Rendering/geometryengine.h \
    UserInteraction/UI/poi_ui.h \
    UserInteraction/UI/mainwindow.h \
#    UserInteraction/UI/myleapdevicelistener.h \
#    UserInteraction/UI/leapuihandler.h \
    UserInteraction/UI/addnewpoi.h

FORMS    += \
    UserInteraction/UI/addnewpoi.ui \
     UserInteraction/UI/poi_ui.ui \
    UserInteraction/UI/mainwindow.ui

OTHER_FILES += \
    UserInteraction/Rendering/Basic.vsh \
    UserInteraction/Rendering/Basic.fsh \

RESOURCES += \
    shaders.qrc \
    resources.qrc \

unix: files.extra = mkdir /usr/local/SchneiderNavette;

target.path =/usr/bin

appFolder.path = /usr/share/applications

appFolder.files =Images/SchneiderNavette.png Data/SchneiderNavette.desktop

schneiderFolder.path = /usr/local/SchneiderNavette
schneiderFolder.files = README.txt
dataFolder.path = /usr/local/SchneiderNavette/Data
dataFolder.files = Data/*.xml
imageFolder.path =/usr/local/SchneiderNavette/Images
imageFolder.files += Images/*.jpg


INSTALLS += target dataFolder imageFolder schneiderFolder appFolder

