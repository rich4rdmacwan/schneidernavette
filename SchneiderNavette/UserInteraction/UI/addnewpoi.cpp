#include "addnewpoi.h"
#include "ui_addnewpoi.h"
#include <QDebug>


AddNewPoi::AddNewPoi(std::vector<PointOfInterest> &myPois, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AddNewPoi),
    pois(myPois),
    poiTypes(std::vector<QString>())
{
    ui->setupUi(this);

    // populate list of POI names
    std::vector<PointOfInterest>::iterator it = pois.begin();
    for (;it!=pois.end();it++) {
        ui->cmbName->addItem((*it).toString());
        QString poiType = (*it).getType();
        if (std::find(poiTypes.begin(),poiTypes.end(),poiType) == poiTypes.end()) { // there's no such type yet
            poiTypes.push_back(poiType); // add it
            ui->cmbCategory->addItem(poiType);
        }
    }
    ui->cmbName->setCurrentIndex(-1);
    ui->cmbCategory->setCurrentIndex(-1);

    visiblePOIs = pois;
    ui->txtNumOfPOIs->setVisible(false);

    // hook up signals
    connect(ui->cmbCategory, SIGNAL(currentIndexChanged(int)),this,SLOT(slotTypeIndexChanged()));
    connect(ui->cmbName, SIGNAL(currentIndexChanged(int)),this,SLOT(slotNameIndexChanged()));
    connect(ui->btnClearCategory, SIGNAL(clicked()),this,SLOT(slotBtnClearSelectionClicked()));

    connect(ui->btnAddPOI, SIGNAL(clicked()), this, SLOT(slotBtnAddPOIClicked()));
}

AddNewPoi::~AddNewPoi()
{
    delete ui;
}
bool AddNewPoi::getAddingSource() const
{
    return addingSource;
}

void AddNewPoi::setAddingSource(bool value)
{
    addingSource = value;
}





void AddNewPoi::slotTypeIndexChanged()
{
    // filter out visible POIs based on selected type
    visiblePOIs.clear();
    ui->cmbName->clear();

    int typeIdx = ui->cmbCategory->currentIndex();
    if (typeIdx == -1) { // user typed in sometthing not from the list, reset the field
        ui->cmbCategory->setCurrentText("");
        ui->cmbName->setCurrentIndex(-1);
        ui->txtNumOfPOIs->setVisible(false);

        //
        visiblePOIs = pois;
        for (int i=0;i<visiblePOIs.size();i++)
            ui->cmbName->addItem(visiblePOIs[i].toString());

        return;
    }
    ui->txtNumOfPOIs->setVisible(true);

    // get selected type
    QString selectedType = ui->cmbCategory->currentText();
    // go through all POIs
    std::vector<PointOfInterest>::iterator it = pois.begin();
    for (;it!=pois.end();it++) {
        if ((*it).getType() == selectedType) {
            visiblePOIs.push_back(*it);
            ui->cmbName->addItem((*it).toString());
        }
    }

    // reset fields
    ui->cmbName->setCurrentIndex(-1);
    ui->txtAddress->setText("");
    ui->txtInformation->setText("");

    ui->txtNumOfPOIs->setText(QString("There are ") + QString::number(visiblePOIs.size()) + QString(" POIs of this type."));
}

void AddNewPoi::slotNameIndexChanged()
{
    int nameIdx = ui->cmbName->currentIndex();
    if (nameIdx == -1) {
        ui->cmbName->setCurrentText("");
        return;
    }

    PointOfInterest selectedPOI = visiblePOIs[nameIdx];

    ui->cmbCategory->setCurrentText(selectedPOI.getType());
    ui->txtAddress->setText(selectedPOI.getAddress());
    ui->txtInformation->setText(selectedPOI.getInfo());
}

void AddNewPoi::slotBtnClearSelectionClicked()
{
    ui->cmbCategory->setCurrentIndex(-1);
    ui->cmbCategory->setCurrentText("");
    ui->cmbName->setCurrentIndex(-1);
    ui->cmbName->setCurrentText("");
    ui->txtAddress->setText("");
    ui->txtInformation->setText("");
}

void AddNewPoi::slotBtnAddPOIClicked()
{

    int idx = ui->cmbName->currentIndex();


    if (idx != -1) { // if name selected, emit that
       PointOfInterest selectedPoi = visiblePOIs[idx];
       emit signalSelectedPOI(selectedPoi);
       this->close();
       return;
    }

    idx = ui->cmbCategory->currentIndex();
    if (idx != -1) {
        if(addingSource){
            ui->txtNumOfPOIs->setText("Please select a single point of interest as the source");
        }
        else{
        emit signalSelectedCategory(poiTypes[idx]);
        this->close();

        }
        return;
    }

}
