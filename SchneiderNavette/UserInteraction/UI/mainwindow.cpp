#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFileDialog>
#include <QScrollBar>
#include <iostream>
#include <QResizeEvent>
#include <QSize>
#include "Processing/metrics.h"
#include "Model/routerequest.h"
#include "Data/routepreprocessor.h"
#include <QResource>


#include "Model/node.h"

#include <QDebug>
#include <QDataStream>
#include <QFile>
using namespace std;
//LeapUIHandler *leapUIHandler;


//Initialize mainwindow's instance holder to NULL
MainWindow* MainWindow::instance=NULL;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{

    instance=this;
    addingSource=false;
    qRegisterMetaType< vector<PointOfInterest> >("vector<PointOfInterest>");
    qRegisterMetaType<PathResult>("PathResult");
    isPoiListModified = false;

    ui->setupUi(this);

    ui->label->setVisible(false);


    QScrollBar *vb = new QScrollBar();
    ui->lstPOIs->setVerticalScrollBar(vb);

    QScrollBar *hb = new QScrollBar();
    ui->lstPOIs->setHorizontalScrollBar(hb);


    openglRenderer=NULL;
    /*
    //IplImage *image;

    QString fileName = QFileDialog::getOpenFileName(this,tr("Open Image"),QDir::currentPath(),tr("Image Files [ *.jpg , *.jpeg , *.bmp , *.png , *.gif]"));

    char* charFileName = fileName.toLocal8Bit().data();
    cv::Mat image = cv::imread(charFileName);
    //image = cvLoadImage(charFileName); */

    qDebug() << "Trying to construct LeapUIHandler..." << endl;
    //leapUIHandler = new LeapUIHandler();
    //leapUIHandler->SetOwner(this);
    qDebug() << "Constructed." << endl;

    //QObject::connect(ui->btnConnect, SIGNAL(clicked()), leapUIHandler, SLOT(ConnectToLeap()));
    //QObject::connect(ui->btnDisconnect, SIGNAL(clicked()), leapUIHandler, SLOT(DisconnectFromLeap()));
    //
    QGLFormat glFormat;
    qDebug() << "OpenGL Versions Supported: " << glFormat.majorVersion()<<"."<<glFormat.minorVersion();


    //glFormat.setVersion( 3, 3 );
    //glFormat.setProfile( QGLFormat::CoreProfile ); // Requires >=Qt-4.8.0
    //glFormat.setSampleBuffers( true );


    init();
    // UI for POI editing

    QGridLayout *grid = new QGridLayout;
    POI_UI * poiForEdit = new POI_UI(leCreusot.getPOIs(),this);
    //Connect poiForEdit's newPOIAdded signal to geometry engines slot for updating.
    //connect(poiForEdit,SIGNAL(poiListUpdated()),openglRenderer->getGeometryEngine(),SLOT(reinitPOIData()));

    ui->tab_2->setLayout(grid);
    ui->tab_2->layout()->addWidget(poiForEdit);
    //myRenderer = new MapRenderer(640, 480);
    //UpdateUI();

    ConnectAllHandlers();
    numberOfMapClicks = 0;

    // set default UI values
    this->routeType = 1; // driving

    lblStatus=new QLabel(this);
    lblStatus->setText("Road Legend : ");
    ui->statusBar->addWidget(lblStatus);

    lblPrimary=new QLabel(this);
    lblPrimary->setText("Primary");
    lblPrimary->setStyleSheet("background:#ffea87;color:#1f1f1f");
    ui->statusBar->addWidget(lblPrimary);

    lblSecondary=new QLabel(this);
    lblSecondary->setText("Secondary");
    lblSecondary->setStyleSheet("background:#4e7b13;color:#1f1f1f");
    ui->statusBar->addWidget(lblSecondary);

    lblTertiary=new QLabel(this);
    lblTertiary->setText("Tertiary");
    lblTertiary->setStyleSheet("background:#f8857c;color:#1f1f1f");
    ui->statusBar->addWidget(lblTertiary);

    lblOneway=new QLabel(this);
    lblOneway->setText("One Way");
    lblOneway->setStyleSheet("background:#aed5e7;color:#1f1f1f");
    ui->statusBar->addWidget(lblOneway);

    lblOther=new QLabel(this);
    lblOther->setText("Others");
    lblOther->setStyleSheet("background:white;color:#1f1f1f");
    ui->statusBar->addWidget(lblOther);


    //Default startNode and endNode
    startNode=NULL;
    endNode=NULL;
    distanceFromPOI=0;
    newPoi=NULL;
}


void MainWindow::ConnectAllHandlers()
{
    connect(ui->btnAddPOI, SIGNAL(clicked()), this, SLOT(slotAddNewPoiClicked()));
    connect(ui->btnRemovePOI, SIGNAL(clicked()), this, SLOT(slotRemovePoiItemClicked()));

    // connect map mouse
    //connect(openglRenderer->getGeometryEngine(), SIGNAL(clickReturn(QVector2D)), this, SLOT(slotMapClicked(QVector2D)));

    // other UI elements
    connect(ui->distanceSlider, SIGNAL(valueChanged(int)), this, SLOT(slotDistanceSliderMoved(int)));
    connect(ui->btnDriving, SIGNAL(clicked()), this, SLOT(slotBtnDrivingClicked()));
    connect(ui->btnWalking, SIGNAL(clicked()), this, SLOT(slotBtnWalkingClicked()));


    connect(ui->btnFindPath, SIGNAL(clicked()), this, SLOT(slotBtnFindPathClicked()));
    connect(ui->btnStartNewRequest, SIGNAL(clicked()), this, SLOT(slotBtnStartNewRequestClicked()));
    connect(ui->btnSwapSrcDest, SIGNAL(clicked()), this, SLOT(slotBtnSwapSrcDestClicked()));

    // text changed handlers for text boxes
    connect(ui->txtStartLat, SIGNAL(textChanged(QString)), this, SLOT(slotStartPointChanged(QString)));
    connect(ui->txtEndLat, SIGNAL(textChanged(QString)), this, SLOT(slotStartPointChanged(QString)));


    // validators
    ui->txtStartLat->setValidator(new QDoubleValidator(Metrics::minLat, Metrics::maxLat, 8, this));
    ui->txtEndLat->setValidator(new QDoubleValidator(Metrics::minLat, Metrics::maxLat, 8, this));
    ui->txtStartLon->setValidator(new QDoubleValidator(Metrics::minLon, Metrics::maxLon, 8, this));
    ui->txtEndLon->setValidator(new QDoubleValidator(Metrics::minLon, Metrics::maxLon, 8, this));
}
bool MainWindow::getIsPoiListModified() const
{
    return isPoiListModified;
}

void MainWindow::setIsPoiListModified(bool value)
{
    isPoiListModified = value;
}


PathFinderEngine MainWindow::getPathFinderengine() const
{
    return pathFinderengine;
}


void MainWindow::setPathFinderengine(const PathFinderEngine &value)
{
    pathFinderengine = value;
}



void MainWindow::slotDistanceSliderMoved(int newValue)
{
    ui->lblDistance->setText(QString::number(newValue) + " km");
    this->setDistanceFromPOI(newValue);
}

void MainWindow::slotBtnWalkingClicked()
{
    ui->btnWalking->setFlat(false);
    ui->btnDriving->setFlat(true);

    ui->btnDriving->setEnabled(true);
    ui->btnWalking->setEnabled(false);

    this->routeType = 0;
}

void MainWindow::slotBtnDrivingClicked()
{
    ui->btnWalking->setFlat(true);
    ui->btnDriving->setFlat(false);

    ui->btnDriving->setEnabled(false);
    ui->btnWalking->setEnabled(true);

    this->routeType = 1;
}

void MainWindow::slotAddPOISelectedPOI(PointOfInterest poi)
{
    RouteRequest req(poi);
    Node *poiNode = leCreusot.getNearestNode(QVector2D(poi.getLatitude(), poi.getLongitude()));
    req.setNode(poiNode);
    //If the source is empty, then add this poi as the source
    if(addingSource){
        addingSource=false;
        if(newPoi!=NULL){
            newPoi->setAddingSource(false);
        }
        ui->btnAddSource->setDisabled(true);
        ui->txtStartLat->setText(QString::number(poiNode->getLatitude()));
        ui->txtStartLon->setText(QString::number(poiNode->getLongitude()));
    }
    else{
        this->throughList.push_back(req);
        ui->lstPOIs->addItem(req.toString());
    }
    //qDebug() << "lat & long for nearest node: " << poiNode << " -> " << req.getNode() << endl;
    //qDebug() << req.getNode()->getId() << ": "  << req.getNode()->getLatitude() << ", " << req.getNode()->getLongitude() << endl;

    emit POIAdded(poi);
}

void MainWindow::slotAddPOISelectedCategory(QString currentCat)
{
    RouteRequest req(currentCat);
    this->throughList.push_back(req);
    ui->lstPOIs->addItem(req.toString());
//    std::string cat=currentCat.toStdString();
//    std::transform(cat.begin(), cat.end(), cat.begin(), ::tolower);

//    vector<PointOfInterest> pois= pathFinderengine.getPoiMap()[cat];
    //qDebug()<<"Emit Draw pois in category";
    emit drawPOIsInCategory(req.getCategory());
}

void MainWindow::slotBtnFindPathClicked()
{


    //qDebug()<<ui->distanceSlider->value();
    //Clear existing route if present
    emit clearRoute();
    PathResult* pathResult=NULL;
    std::vector<PointOfInterest> poisInDistance;


    //Update startNode and endNode pointers
    if(ui->txtStartLat->text()!="" && ui->txtStartLon->text()!="") {
        if(startNode!=NULL){
            //delete startNode;
            //startNode=NULL;
        }
        startNode=leCreusot.getNearestNode(QVector2D(ui->txtStartLat->text().toDouble(),ui->txtStartLon->text().toDouble()));
    }
    else startNode=NULL;


    if(ui->txtEndLat->text()!="" && ui->txtEndLon->text()!="") {
        if(endNode!=NULL){
            //delete endNode;
            //endNode=NULL;
        }
        endNode=leCreusot.getNearestNode(QVector2D(ui->txtEndLat->text().toDouble(),ui->txtEndLon->text().toDouble()));
    }
    else endNode=NULL;
    if(startNode!=NULL)
        qDebug()<<"Start Node : "<<startNode->getLatitude()<<","<<startNode->getLongitude();
    if(endNode!=NULL)
        qDebug()<<"End Node : "<<endNode->getLatitude()<<","<<endNode->getLongitude();
    qDebug()<<"Distance from POI : "<<distanceFromPOI;
    qDebug()<<"Number of selected pois: "<<throughList.size();


    //1 Only Start node + endnode,throughlist is empty,distance is default
    if(startNode!=NULL && endNode!=NULL  && distanceFromPOI== 0 && throughList.empty()){
        qDebug()<<"PathResult 1";
        pathResult=pathFinderengine.findPath(startNode->getId(),endNode->getId(),routeType);
        pathFinderengine.resetNodes();
        emit drawRoute(*pathResult);
    }
    //2 Startnode + distance
    //We can only check if the size of throughlist is one to find pois within distance for a category
    //If the user needs to find a shortest route to one of these pois, he has add that poi manually
    else if(startNode!=NULL && distanceFromPOI!=0 && endNode==NULL && (throughList.empty() || (throughList.size()==1 && throughList.at(0).getRequestType()==1))){
        qDebug()<<"PathResult 2";

        poisInDistance=pathFinderengine.findPoiWithinDistance(distanceFromPOI,throughList,startNode->getId());
        emit drawPOIsInDistance(poisInDistance);
    }
    //3 StartNode +endNode + throughList
    else if(startNode!=NULL && !throughList.empty() && distanceFromPOI==0){
        qDebug()<<"PathResult 3.1";
        if(endNode!=NULL){
            //Send endNode
            pathResult=pathFinderengine.findPathThroughPoi(startNode->getId(),throughList,endNode->getId(),routeType);
            qDebug()<<"Founr path 3.1";
        }
        else{
            qDebug()<<"Draw pathResult 3.2";
            //Send -1 for endNode id
            pathResult=pathFinderengine.findPathThroughPoi(startNode->getId(),throughList,-1,routeType);
        }
        if(pathResult->getnodes().size()>0)
            emit drawRoute(*pathResult);
    }
    //4
    else if(startNode!=NULL && !throughList.empty() && distanceFromPOI!=0){
        if(endNode!=NULL){
            //Send endNode
            qDebug()<<"Path Result 4.1";
            pathResult=pathFinderengine.findPathThroughPoiWithinDistance(startNode->getId(),throughList,endNode->getId(),distanceFromPOI, routeType);
            qDebug()<<"Path Result 4.1 end";
        }
        else{
            //Send -1
            qDebug()<<"Path Result 4.2";
            pathResult=pathFinderengine.findPathThroughPoiWithinDistance(startNode->getId(),throughList,-1,distanceFromPOI, routeType);
        }
        if(pathResult->getnodes().size()>0)
            emit drawRoute(*pathResult);
    }


    // display path in UI list
    if (pathResult!=NULL) {
        if(pathResult->getnodes().size()>0){
            ui->lstPathResult->clear();
            qDebug() << "path result size: " << pathResult->getnodes().size() << endl;

            for (int i=0;i<pathResult->getnodes().size();i++) {
                Node myNode = pathResult->getnodes()[i];
                ui->lstPathResult->addItem(
                            QString::number(i+1) +
                            QString(" -> [ Latitude: ") +
                            QString::number(myNode.getLatitude()) +
                            QString(" ] | [ Longitude: " +
                                    QString::number(myNode.getLongitude()) +
                                    QString(" ]")));
                //qDebug() << "path result display..." << endl;
            }

            ui->txtRouteDistance->setText(QString::number(pathResult->getPathDistance()));
            ui->txtRouteTime->setText(QString::number(pathResult->getPathTravelTime()));
            //emit drawRoute(PathFinderEngine)
            //or emit void drawRoute(vector<PointOfInterest>) based on the request
        }
        else{
            ui->lstPathResult->addItem("No shortest path for the given request");
        }

    }
    else{
        ui->lstPathResult->clear();
    }



    //USe these to reset the variables for FindPAth
    //Note to Igor:
    //startNode=NULL; endNode=NULL; update distanceFromPOI from slider, throughList.clear();
}

void MainWindow::slotBtnStartNewRequestClicked()
{
    ui->txtEndLat->clear();
    ui->txtEndLon->clear();
    ui->txtRouteDistance->setText("---");
    ui->txtRouteTime->setText("---");
    ui->txtStartLat->clear();
    ui->txtStartLon->clear();
    throughList.clear();
    ui->lstPathResult->clear();
    ui->lstPOIs->clear();
    ui->btnAddSource->setDisabled(false);
    startNode = NULL;
    endNode = NULL;
    distanceFromPOI = ui->distanceSlider->value();
    //Inform geometry engine to clear the current route
    emit clearRoute();
    emit clearSelectedPOIs();
}

void MainWindow::slotStartPointChanged(QString text)
{
    QString startLat = ui->txtStartLat->text();
    double lat = startLat.toDouble();

    QString startLon = ui->txtStartLon->text();
    double lon = startLon.toDouble();

    // get text from end text boxes
    QString endLat = ui->txtEndLat->text();
    QString endLon = ui->txtEndLon->text();
    bool isStartDefined = false, isEndDefined = false;
    if (endLat.length() > 0 || endLon.length() > 0)
        isEndDefined = true;
    if (startLat.length() > 0 || startLon.length() > 0)
        isStartDefined = true;

    if (isStartDefined && !isEndDefined) ui->distanceSlider->setEnabled(true);
    else ui->distanceSlider->setEnabled(false);
}

void MainWindow::slotEndPointChanged(QString text)
{
    QString endLat = ui->txtEndLat->text();
    double lat = endLat.toDouble();

    QString endLon = ui->txtEndLat->text();
    double lon = endLon.toDouble();


    // get text from end text boxes
    QString startLat = ui->txtStartLat->text();
    QString startLon = ui->txtStartLon->text();
    bool isStartDefined = false, isEndDefined = false;
    if (endLat.length() > 0 || endLon.length() > 0)
        isEndDefined = true;
    if (startLat.length() > 0 || startLon.length() > 0)
        isStartDefined = true;

    if (isStartDefined && !isEndDefined) ui->distanceSlider->setEnabled(true);
    else ui->distanceSlider->setEnabled(false);
}

MainWindow::~MainWindow()
{
    if(isPoiListModified)
    {
        RoutePreprocessor::savePOI(leCreusot.getPOIs());
    }
    delete ui;
}

//void MainWindow::slotBrowseButtonClicked()
//{
//    QFileDialog browseImage();
//    browseImage.show();
//}

void MainWindow::slotAddNewPoiClicked()
{

    if(newPoi==NULL){
        newPoi = new AddNewPoi(leCreusot.getPOIs(), this);

        connect(newPoi, SIGNAL(signalSelectedPOI(PointOfInterest)), this, SLOT(slotAddPOISelectedPOI(PointOfInterest)));
        connect(newPoi, SIGNAL(signalSelectedCategory(QString)), this, SLOT(slotAddPOISelectedCategory(QString)));
    }
    if(addingSource)
        newPoi->setAddingSource(true);
    newPoi->showNormal();

}


void MainWindow::slotRemovePoiItemClicked()
{
    QModelIndexList indexes = ui->lstPOIs->selectionModel()->selectedIndexes();

    QList<QListWidgetItem *> itemList = ui->lstPOIs->selectedItems();
    for (int i=0; i<itemList.size(); i++) {
        int idx = indexes[i].row();
        //qDebug()<<"Request to remove: "<<(*(throughList.begin()+idx)).getPOI().getId()<<":"<< (*(throughList.begin()+idx)).getPOI().getName();
        //If the item to be removed is a category,send the category string in the signal
        //otherwise, send the poi to be removed
        if((*(throughList.begin() + idx)).getRequestType()==1){
            qDebug()<<"Request to remove " <<(*(throughList.begin() + idx)).getCategory();
            emit POIsRemoved((*(throughList.begin() + idx)).getCategory());
        }
        else{
            emit POIRemoved((*(throughList.begin()+idx)).getPOI());
        }
        throughList.erase(throughList.begin() + idx);

        delete ui->lstPOIs->takeItem(idx);
    }

}

void MainWindow::on_btnAddSource_clicked()
{
    addingSource=true;

    emit ui->btnAddPOI->clicked();
}

void MainWindow::slotMapClicked(QVector2D)
{
    qDebug()<<"Got event mainwindow";
}

void MainWindow::slotBtnSwapSrcDestClicked()
{
    QString tempLat = ui->txtStartLat->text();
    QString tempLong = ui->txtStartLon->text();
    ui->txtStartLat->setText(ui->txtEndLat->text());
    ui->txtStartLon->setText(ui->txtEndLon->text());
    ui->txtEndLat->setText(tempLat);
    ui->txtEndLon->setText(tempLong);
}

void MainWindow::UpdateStatusText(QString message) {
    //ui->lblLeapX->setText(message);
}

void MainWindow::UpdateUI()
{
    //myRenderer->DoRender(leCreusot);
    //QImage myImage = myRenderer->GetLastRenderImage();
    //ui->label->setPixmap(QPixmap::fromImage(myImage));
}

void MainWindow::resizeEvent(QResizeEvent *event)
{
    int w=event->size().width();
    int h=event->size().height();
    if(openglRenderer!=NULL)
        openglRenderer->resize(w-glWidgetX-glWidgetGapX,h-glWidgetY-glWidgetGapY-ui->statusBar->height());

}

void MainWindow::addOpenGLRenderer(RawData *mapData)
{
    openglRenderer=new OpenGLRenderer(mapData, ui->scrollArea);
    glWidgetX=0; glWidgetY=0;
    glWidgetGapX=5; glWidgetGapY=5;
    openglRenderer->setGeometry(glWidgetX,glWidgetY,this->width()-glWidgetGapX,this->height()-ui->statusBar->height()-glWidgetGapY);
    openglRenderer->setFocus();
}


void MainWindow::init()
{
    ui->label->setVisible(false);

    //Loading data from XML files instead of DB, rawData is the whole map data needed to process different functionlities
    std::map<long,Node> nodes;
    RoutePreprocessor::loadNodes(nodes);
    std::map<std::string,Road> roads;
    RoutePreprocessor::loadRoads(roads);
    std::vector<PointOfInterest> pois;
    RoutePreprocessor::loadPois(pois);
    leCreusot.setNodes(nodes);
    leCreusot.setRoads(roads);
    leCreusot.setPOIs(pois);

    pathFinderengine.setMapData(leCreusot);
    pathFinderengine.convertPoiListIntoMap();

    addOpenGLRenderer(&leCreusot);
}

void MainWindow::setStartPoint(QVector2D &start)
{
    this->startPoint = start;
}

QVector2D MainWindow::getStartPoint()
{
    return this->startPoint;
}

void MainWindow::setEndPoint(QVector2D &end)
{
    this->endPoint = end;
}

QVector2D MainWindow::getEndPoint()
{
    return this->endPoint;
}

void MainWindow::setDistanceFromPOI(int distance)
{
    this->distanceFromPOI = distance;
}

int MainWindow::getDistanceFromPOI()
{
    return this->distanceFromPOI;
}

std::vector<RouteRequest> MainWindow::getThroughtList()
{
    return this->throughList;
}

void MainWindow::setThroughList(std::vector<RouteRequest> list)
{
    this->throughList = list;
}


