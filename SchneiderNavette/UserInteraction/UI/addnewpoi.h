#ifndef ADDNEWPOI_H
#define ADDNEWPOI_H

#include <QDialog>
#include "Model/pointofinterest.h"

namespace Ui {
class AddNewPoi;
}

class AddNewPoi : public QDialog
{
    Q_OBJECT

public:
    explicit AddNewPoi(std::vector<PointOfInterest>&, QWidget *parent = 0);
    ~AddNewPoi();
    static AddNewPoi* getInstance();

    bool getAddingSource() const;
    void setAddingSource(bool value);

private:
    Ui::AddNewPoi *ui;
    std::vector<PointOfInterest> &pois;
    std::vector<QString> poiTypes;
    std::vector<PointOfInterest> visiblePOIs;
    bool addingSource;
public slots:
    void slotTypeIndexChanged();
    void slotNameIndexChanged();
    void slotBtnClearSelectionClicked();

    void slotBtnAddPOIClicked();

signals:
    void signalSelectedPOI(PointOfInterest selectedPOI);
    void signalSelectedCategory(QString category);

};

#endif // ADDNEWPOI_H
