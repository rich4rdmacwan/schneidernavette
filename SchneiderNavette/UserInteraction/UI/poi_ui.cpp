#include "poi_ui.h"
#include "ui_poi_ui.h"
#include "QMessageBox"
#include <QDebug>
#include "Processing/metrics.h"
#include "mainwindow.h"

void POI_UI::PopulateComboBoxes() {

    ui->cmbName->setCurrentIndex(-1);
    ui->cmbCategory->setCurrentIndex(-1);

    poiTypes.clear();
    ui->cmbCategory->clear();
    ui->cmbName->clear();

    // populate list of POI names
    std::vector<PointOfInterest>::iterator it = pois.begin();
    for (;it!=pois.end();it++) {
        ui->cmbName->addItem((*it).toString());
        QString poiType = (*it).getType();
        if (std::find(poiTypes.begin(),poiTypes.end(),poiType) == poiTypes.end()) { // there's no such type yet
            poiTypes.push_back(poiType); // add it
        }
    }
    sort(poiTypes.begin(), poiTypes.end());
    for (int i=0;i<poiTypes.size();i++)
        ui->cmbCategory->addItem(poiTypes[i]);

    visiblePOIs = pois;

    ui->cmbCategory->setCurrentText("");
    ui->cmbName->setCurrentText("");
    ui->txtAddress->setText("");
    ui->txtInformation->setText("");
    ui->txtStartLat->setText("");
    ui->txtStartLon->setText("");

    isEditMode = true;

    ui->cmbName->blockSignals(true);
    ui->cmbName->setCurrentIndex(-1);
    ui->cmbName->blockSignals(false);

}

void POI_UI::ClearComboBoxes() {

    visiblePOIs.clear();

    ui->cmbName->setCurrentIndex(-1);
    ui->cmbCategory->setCurrentIndex(-1);

    ui->cmbCategory->setCurrentText("");
    ui->cmbName->setCurrentText("");

    //ui->cmbCategory->clear();
    ui->cmbName->clear();
    ui->txtAddress->clear();
    ui->txtInformation->clear();
    ui->txtStartLat->clear();
    ui->txtStartLon->clear();
}

POI_UI::POI_UI(std::vector<PointOfInterest> &myPois, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::POI_UI),
    pois(myPois),
    poiTypes(std::vector<QString>())
{
    ui->setupUi(this);

    ui->btnDiscardEntry->setVisible(false);
    ui->btnDeletePOI->setVisible(false);
    ui->btnSavePOI->setVisible(false);
    ui->btnClearCategory->setVisible(false);

    // default is ADD mode
    this->isEditMode = true;

    ui->txtNumOfPOIs->setVisible(false);

    PopulateComboBoxes();

    // hook up signals
    connect(ui->cmbCategory, SIGNAL(currentIndexChanged(int)),this,SLOT(slotTypeIndexChanged()));
    connect(ui->cmbName, SIGNAL(currentIndexChanged(int)),this,SLOT(slotNameIndexChanged()));
    connect(ui->btnClearCategory, SIGNAL(clicked()),this,SLOT(slotBtnClearSelectionClicked()));
    connect(ui->btnDiscardEntry, SIGNAL(clicked()), this, SLOT(slotBtnDiscardPOIClicked()));

    connect(ui->btnAddPOI, SIGNAL(clicked()), this, SLOT(slotBtnAddPOIClicked()));
    connect(ui->btnDeletePOI, SIGNAL(clicked()), this, SLOT(slotBtnDeletePOIClicked()));
    connect(ui->btnSavePOI, SIGNAL(clicked()), this, SLOT(slotBtnSavePOIClicked()));

    // validators
    ui->txtStartLat->setValidator(new QDoubleValidator(Metrics::minLat, Metrics::maxLat, 8, ui->txtStartLat));
    ui->txtStartLon->setValidator(new QDoubleValidator(Metrics::minLon, Metrics::maxLon, 8, ui->txtStartLon));
}

POI_UI::~POI_UI()
{
    delete ui;
}


void POI_UI::slotTypeIndexChanged()
{

    int typeIdx = ui->cmbCategory->currentIndex();
    if (typeIdx == -1) { // user typed in sometthing not from the list, reset the field
        ui->cmbCategory->setCurrentText("");
        ui->txtNumOfPOIs->setVisible(false);

        //
        visiblePOIs = pois;
        for (int i=0;i<visiblePOIs.size();i++)
            ui->cmbName->addItem(visiblePOIs[i].toString());

        return;
    }

    if (isEditMode) {
        // filter out visible POIs based on selected type
        visiblePOIs.clear();
        ui->cmbName->clear();
        ui->txtNumOfPOIs->setVisible(true);

        // get selected type
        QString selectedType = ui->cmbCategory->currentText();
        // go through all POIs
        std::vector<PointOfInterest>::iterator it = pois.begin();
        for (;it!=pois.end();it++) {
            if ((*it).getType() == selectedType) {
                visiblePOIs.push_back(*it);
               // qDebug() << "added this poi to visible: " << (*it).getName()  << ", " << (*it).getAddress() << endl;
                ui->cmbName->addItem((*it).toString());
            }
        }
    }

    // reset fields
    ui->cmbName->setCurrentText("");
    ui->txtAddress->setText("");
    ui->txtInformation->setText("");
    ui->txtStartLat->clear();
    ui->txtStartLon->clear();

    ui->txtNumOfPOIs->setText(QString("There are ") + QString::number(visiblePOIs.size()) + QString(" POIs of this type."));

    if (isEditMode) {
        ui->btnClearCategory->setVisible(true);
        ui->btnSavePOI->setVisible(false);
        ui->btnDeletePOI->setVisible(false);
    }
    else {
        ui->btnClearCategory->setVisible(false);
    }

    ui->cmbName->blockSignals(true);
    ui->cmbName->setCurrentIndex(-1);
    ui->cmbName->blockSignals(false);
}

PointOfInterest *selectedPOI = NULL;

void POI_UI::slotNameIndexChanged()
{
    QString nameText = ui->cmbName->currentText();
    if (nameText.isEmpty()) {
        //ui->btnDeletePOI->setVisible(false);
        //ui->btnSavePOI->setVisible(false);

        return;
    }


    int nameIdx = ui->cmbName->currentIndex();

    //qDebug() << "visiblePOI size: " << visiblePOIs.size() << ", selected idx: " << nameIdx << endl;

    selectedPOI = &visiblePOIs[nameIdx];

    if (isEditMode) {
        ui->btnDeletePOI->setVisible(true);
        ui->btnSavePOI->setVisible(true);
        ui->btnClearCategory->setVisible(true);
    }

    ui->cmbCategory->setCurrentText(selectedPOI->getType());
    ui->txtAddress->setText(selectedPOI->getAddress());
    ui->txtInformation->setText(selectedPOI->getInfo());
    ui->txtStartLat->setText(QString::number(selectedPOI->getLatitude()));
    ui->txtStartLon->setText(QString::number(selectedPOI->getLongitude()));
}

void POI_UI::slotBtnClearSelectionClicked()
{
    ui->cmbCategory->setCurrentIndex(-1);
    ui->cmbCategory->setCurrentText("");
    ui->cmbName->setCurrentIndex(-1);
    ui->cmbName->setCurrentText("");
    ui->txtAddress->setText("");
    ui->txtInformation->setText("");
    ui->txtStartLat->setText("");
    ui->txtStartLon->setText("");

    isEditMode = true;
    selectedPOI = NULL;

    ui->btnClearCategory->setVisible(false);
    ui->btnDeletePOI->setVisible(false);
    ui->btnSavePOI->setVisible(false);
}

void POI_UI::slotBtnAddPOIClicked()
{
    this->isEditMode = false;

    ClearComboBoxes();
    ui->cmbCategory->setFocus();

    ui->btnAddPOI->setVisible(false);
    ui->btnDiscardEntry->setVisible(true);

    ui->btnSavePOI->setVisible(true);
    ui->btnDeletePOI->setVisible(false);

    ui->btnClearCategory->setVisible(false);
/*
    int idx = ui->cmbName->currentIndex();

    if (idx != -1) { // if name selected, emit that
       PointOfInterest selectedPoi = visiblePOIs[idx];
       emit signalSelectedPOI(selectedPoi);
       this->close();
       return;
    }

    idx = ui->cmbCategory->currentIndex();
    if (idx != -1) {
        emit signalSelectedCategory(poiTypes[idx]);
        this->close();
        return;
    }

    // if it's not (dialog is used as selection window), ignore click
    QString strMessage = "Please select specific POI.";
    QMessageBox::information(NULL, "Error", strMessage);
*/
}

void POI_UI::slotBtnDiscardPOIClicked()
{
    this->isEditMode = true;
    selectedPOI = NULL;

    PopulateComboBoxes();
    ui->cmbCategory->setFocus();

    ui->btnAddPOI->setVisible(true);
    ui->btnSavePOI->setVisible(false);
    ui->btnDeletePOI->setVisible(false);
    ui->btnDiscardEntry->setVisible(false);
    ui->btnClearCategory->setVisible(false);
}

void POI_UI::slotBtnSavePOIClicked()
{
    if (isEditMode == false) {
        // if widget is in ADD mode...
        selectedPOI = new PointOfInterest();

        // calculate new ID for new POI
        long maxID = pois[0].getId();
        for (int i=0;i<pois.size();i++)
            if (pois[i].getId() > maxID)
                maxID = pois[i].getId();
        selectedPOI->setID(maxID + 1);
    }
    else {
        // find actual POI in pois list
        // (it is list of references, so changes will be reflected outside)
        for (int i=0;i<pois.size();i++)
            if (pois[i].getId() == selectedPOI->getId()) {
                selectedPOI = &pois[i];
                break;
            }
    }

    qDebug()<<ui->cmbCategory->objectName()<<","<<ui->cmbCategory->currentText()<<","<<ui->cmbCategory->currentIndex();
    selectedPOI->setType(ui->cmbCategory->currentText());
    selectedPOI->setName(ui->cmbName->currentText());
    selectedPOI->setAddress(ui->txtAddress->text());
    selectedPOI->setInfo(ui->txtInformation->toPlainText());
    selectedPOI->setLatitude(ui->txtStartLat->text().toDouble());
    selectedPOI->setLongitude(ui->txtStartLon->text().toDouble());

    //set main window edit POI flag to true, to save the result to xml on program quit
    MainWindow::instance->setIsPoiListModified (true);
    //call pathfinder convert POI ist to map again for the shortest path algorithm to work correctly
    MainWindow::instance->getPathFinderengine().convertPoiListIntoMap();


    if (isEditMode)
        QMessageBox::information(NULL, "POI saved", "Changes have been saved to selected POI.");
    else {
        pois.push_back(*selectedPOI);

        QMessageBox::information(NULL, "POI added", "New POI has been saved.");
    }

    isEditMode = true;
    PopulateComboBoxes();

    ui->btnDeletePOI->setVisible(false);
    ui->btnSavePOI->setVisible(false);
    ui->btnAddPOI->setVisible(true);
    ui->btnDiscardEntry->setVisible(false);
    ui->btnClearCategory->setVisible(false);

    ui->txtNumOfPOIs->setVisible(false);
    emit poiListUpdated();
}

void POI_UI::slotBtnDeletePOIClicked()
{
    // confirmation
    QMessageBox::StandardButton reply;
    reply = QMessageBox::question(this, "Confirm delete", QString("Are you sure you want to delete ")
                                  + QString(selectedPOI->toString()) + QString("?"),
                                  QMessageBox::Yes|QMessageBox::No);

    if (reply == QMessageBox::No)
        return;

    // find actual POI in pois list
    // (it is list of references, so changes will be reflected outside)
    for (int i=0;i<pois.size();i++)
        if (pois[i].getId() == selectedPOI->getId()) {
            pois.erase(pois.begin() + i);
            break;
        }

    //set main window edit POI flag to true, to save the result to xml on program quit
    MainWindow::instance->setIsPoiListModified (true);
    //call pathfinder convert POI ist to map again for the shortest path algorithm to work correctly
    MainWindow::instance->getPathFinderengine().convertPoiListIntoMap();

    PopulateComboBoxes();

    ui->btnDeletePOI->setVisible(false);
    ui->btnSavePOI->setVisible(false);
    ui->btnDiscardEntry->setVisible(false);

    ui->btnAddPOI->setVisible(true);
    ui->btnClearCategory->setVisible(false);
    emit poiListUpdated();
}
