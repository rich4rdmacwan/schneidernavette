#ifndef POI_UI_H
#define POI_UI_H

#include <QDialog>
#include <QGroupBox>
#include "Model/pointofinterest.h"

namespace Ui {
class POI_UI;
}

class POI_UI : public QDialog
{
    Q_OBJECT

public:
    explicit POI_UI(std::vector<PointOfInterest>&, QWidget *parent = 0);
    ~POI_UI();

private:
    Ui::POI_UI *ui;
    std::vector<PointOfInterest> &pois;
    std::vector<QString> poiTypes;

    std::vector<PointOfInterest> visiblePOIs;

    void PopulateComboBoxes();
    void ClearComboBoxes();

    bool isEditMode; // 0 - edit selected POI mode, 1 - add new POI mode

public slots:
    void slotTypeIndexChanged();
    void slotNameIndexChanged();
    void slotBtnClearSelectionClicked();

    void slotBtnAddPOIClicked();
    void slotBtnDiscardPOIClicked();
    void slotBtnSavePOIClicked();
    void slotBtnDeletePOIClicked();

signals:
    void signalSelectedPOI(PointOfInterest selectedPOI);
    void signalSelectedCategory(QString category);
    void poiListUpdated();
};

#endif // POI_UI
