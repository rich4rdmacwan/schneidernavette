#include "leapuihandler.h"
#include <QDebug>

LeapUIHandler::LeapUIHandler(QObject *parent)
    : QObject(parent){
    //qDebug() << "Wrong constructor!" << endl;
}

void LeapUIHandler::SetOwner(MainWindow *myOwner)
{
    qDebug() << "Setting owner." << endl;
    leapDevice.ownerWindow = myOwner;
}

void LeapUIHandler::ConnectToLeap() {
    qDebug() << "CONNECTING TO LEAP" << endl;
    controller.addListener(leapDevice);
    qDebug() << "CONNECTED." << endl;
}

void LeapUIHandler::DisconnectFromLeap() {
    controller.removeListener(leapDevice);
}

