#ifndef LEAPUIHANDLER_H
#define LEAPUIHANDLER_H

#include <QObject>
#include "myleapdevicelistener.h"
#include "mainwindow.h"
class LeapUIHandler : public QObject
{
    Q_OBJECT
public:
    explicit LeapUIHandler(QObject *parent = 0);

    //Controller controller;
    //MyLeapDeviceListener leapDevice;

    void SetOwner(MainWindow*);

signals:

public slots:
    void ConnectToLeap();
    void DisconnectFromLeap();
};

#endif // LEAPUIHANDLER_H
