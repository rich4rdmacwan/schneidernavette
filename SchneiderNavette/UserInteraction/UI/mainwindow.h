#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#define QT_STATICPLUGIN
#include <QMainWindow>
#include <QGridLayout>
#include <QIcon>
#include "UserInteraction/Rendering/openglrenderer.h"
//#include "Data/databasehandler.h"
#include "poi_ui.h"
#include "addnewpoi.h"
#include "Model/pointofinterest.h"
#include "Model/routerequest.h"
#include "Processing/pathfinderengine.h"
#include <QLabel>
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    //MapRenderer *myRenderer;

    void UpdateStatusText(QString);
    void UpdateUI();
    void resizeEvent(QResizeEvent *event);
    void addOpenGLRenderer(RawData *mapData);
    void init();

    // getters and setters for private fields
    void setRouteType(int routeType);
    int getRouteType();
    void setStartPoint(QVector2D &start);
    QVector2D getStartPoint();
    void setEndPoint(QVector2D &end);
    QVector2D getEndPoint();
    void setDistanceFromPOI(int distance);
    int getDistanceFromPOI();
    void setThroughList(std::vector<RouteRequest> list);
   std::vector<RouteRequest> getThroughtList();
    static MainWindow* instance;
    PathFinderEngine getPathFinderengine() const;
    void setPathFinderengine(const PathFinderEngine &value);

    bool getIsPoiListModified() const;
    void setIsPoiListModified(bool value);
    RawData leCreusot;
signals:
    void POIAdded(PointOfInterest);
    void POIRemoved(PointOfInterest);
    void POIsRemoved(QString);
    void drawRoute(PathResult);
    void drawPOIsInCategory(QString);
    void drawPOIsInDistance(vector<PointOfInterest>);
    void clearRoute();
    void clearSelectedPOIs();
public slots:
    void slotAddNewPoiClicked();
    void slotRemovePoiItemClicked();
    void on_btnAddSource_clicked();
    // slots for map area mouse handling
    void slotMapClicked(QVector2D);
    void slotDistanceSliderMoved(int);
    void slotBtnWalkingClicked();
    void slotBtnDrivingClicked();

    void slotAddPOISelectedPOI(PointOfInterest);
    void slotAddPOISelectedCategory(QString);

    void slotBtnFindPathClicked();
    void slotBtnStartNewRequestClicked();
    void slotBtnSwapSrcDestClicked();

    void slotStartPointChanged(QString);
    void slotEndPointChanged(QString);

private:
    Ui::MainWindow *ui;
    OpenGLRenderer* openglRenderer;
    int glWidgetX,glWidgetY;
    int glWidgetGapX,glWidgetGapY;
    //DatabaseHandler dbHandler;
    GLfloat mapScale;
    AddNewPoi *newPoi;

    void ConnectAllHandlers();
    int numberOfMapClicks;

    // data storage for parameters set in UI
    int routeType; // 0 - walking, 1 - driving
    QVector2D startPoint;
    QVector2D endPoint;
    Node *startNode, *endNode;
    std::vector<RouteRequest> throughList; // list of POIs to go through
    int distanceFromPOI; // value of slider
    PathFinderEngine pathFinderengine;
    QLabel *lblPrimary,*lblSecondary,*lblTertiary,*lblOther,*lblOneway;
    QLabel* lblStatus;
    bool isPoiListModified;
    /*This is used to indicate that we are adding a source. In this case,
    the POI shouldn't be added to the throughlist. Instead, its coordinates must be added to
    the start point. This flag is set to true and the regular handler to add a poi is called.
    After the poi is selected, this flag should be set to false and ui->btnAddSource should be disabled.
    On clicking Start New Request, ui->btnAddSource should be enabled again.*/
    bool addingSource;
};

#endif // MAINWINDOW_H
