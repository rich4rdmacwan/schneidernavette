#ifndef OPENGLWIDGET_H
#define OPENGLWIDGET_H

#include "geometryengine.h"
#include <QGLWidget>
#include <QGLFunctions>
#include <QMatrix4x4>
#include <QQuaternion>
#include <QVector2D>
#include <QBasicTimer>
#include <QGLShaderProgram>
#include "vertexdata.h"
#include "Model/rawdata.h"

namespace Ui {
class OpenGLRenderer;
}

class GeometryEngine;
class OpenGLRenderer : public QGLWidget, protected QGLFunctions
{
    Q_OBJECT
public:
    explicit OpenGLRenderer( RawData *mapData, QWidget *parent = 0);
    ~OpenGLRenderer();

    GLuint cubeTexture,terrainTexture,lineTexture,roadTexture,poiTexture,selectedPoiTexture,startEndRouteTexture;
    GLfloat getCameraZ();

    void translateTo(GLfloat X,GLfloat Y,GLfloat Z);
    GLfloat getMinWorldZ();
    GLfloat getMaxWorldZ();
    GLfloat getWorldX();
    GLfloat getWorldY();
    GeometryEngine* getGeometryEngine();


signals:
    void clicked(QVector2D);

public slots:

protected:
    void keyPressEvent(QKeyEvent *);
    void keyReleaseEvent(QKeyEvent *);
    void mousePressEvent(QMouseEvent *e);
    void mouseReleaseEvent(QMouseEvent *);
    void mouseMoveEvent(QMouseEvent *e);
    void timerEvent(QTimerEvent *e);
    void wheelEvent(QWheelEvent *wevent);
    void initializeGL();
    void resizeGL(int w, int h);
    void paintGL();
    void zoomStep();
    void initShaders();
    void initTextures();
    QVector3D getGLCoordinates(int x, int y);


private:
    Ui::OpenGLRenderer *ui;
    QBasicTimer timer;
    QGLShaderProgram program;
    GeometryEngine geometries;


    QMatrix4x4 projection;

    QVector2D mousePressPosition;
    QVector2D mouseMovePosition;
    QVector3D rotationAxis;
    qreal rotationAngle;
    QQuaternion rotation;
    GLfloat worldX,worldY,worldZ,minWorldZ,maxWorldZ,prevWorldZ;
    GLfloat xStep,yStep,zStep;
    void defaults();
    GLfloat pitchAngle,yawAngle;
    QMatrix4x4 modelViewMatrix;
    qreal aspect, zNear,zFar,fov;
    GLfloat targetWorldX,targetWorldY,targetWorldZ;
    GLfloat offsetWorldX,offsetWorldY,offsetWorldZ;
    //This governs how fast/slow the map will move to a point when a poi as added or removed
    GLfloat mapMoveSpeed;
    GLfloat routeWidthPx,routeWidthGL;
};

#endif // OPENGLWIDGET_H
