#include "geometryengine.h"
#include "Processing/pathfinderengine.h"
#include "Data/routepreprocessor.h"
#include "UserInteraction/UI/mainwindow.h"
#include <QFile>
#include <QFileInfo>
#include <QDir>
GeometryEngine::GeometryEngine()
{
    inited=false;
    poisUpdating=false;
}

GeometryEngine::~GeometryEngine()
{
    glDeleteBuffers(2, cubeBuffer);
    glDeleteBuffers(2, mapBuffer);
    glDeleteBuffers(2, roadsBuffer);
    glDeleteBuffers(2, roadQuadsBuffer);
    glDeleteBuffers(2, verticesBuffer);
    glDeleteBuffers(2, poiBuffer);
    glDeleteBuffers(1, &selectedPOIBuffer);
    glDeleteBuffers(1, &selectedPointBuffer);
    glDeleteBuffers(1, &startEndRouteBuffer);
    for(int i=0;i<selectedPOIImagesBuffer.size();i++){
        glDeleteBuffers(1,&(selectedPOIImagesBuffer.at(i)));
    }
}

void GeometryEngine::init(QGLShaderProgram *program, RawData *rawMapData)
{

    mapSize=10.0f;
    mapScale=15;
    mapZ=0.9f;
    nodeZ=1.0f;
    roadQuadZ=1.001f;
    roadZ=1.002f;
    poiZ=1.01f;
    poiCount=0;
    roadIndexCount=0;
    nodeCount=0;

    this->program=program;
    initializeGLFunctions();


    leCreusot = &(MainWindow::instance->leCreusot);

    double minLatitude=100;
    double maxLatitude=0;
    double minLongitude=100;
    double maxLongitude=0;

    Node node,minLatNode,minLongNode,maxLatNode,maxLongNode;
    std::map<long,Node>::iterator nodeit;
    int i=0;
    for(nodeit=leCreusot->getNodes().begin();nodeit!=leCreusot->getNodes().end();++nodeit){
        node=nodeit->second;
        if(i==0)
            //qDebug()<<i++<<" : "<<node.getId();
        //Update max and min latitudes and longitude values
        if(minLatitude > node.getLatitude()){
            minLatitude=node.getLatitude();
            minLatNode=node;
        }

        if(minLongitude > node.getLongitude()){
            minLongitude=node.getLongitude();
            minLongNode=node;
        }

        if(maxLatitude < node.getLatitude()){
            maxLatitude=node.getLatitude();
            maxLatNode=node;
        }
        if(maxLongitude < node.getLongitude()){
            maxLongitude=node.getLongitude();
            maxLongNode=node;
        }

    }
    Metrics::minLat=minLatitude;
    Metrics::maxLat=maxLatitude;
    Metrics::minLon=minLongitude;
    Metrics::maxLon=maxLongitude;
    // qDebug()<<"minLAt "<<minLatitude<<" maxLat "<<maxLatitude<<" minLon "<<minLongitude<<" maxLon "<<maxLongitude;
//    qDebug()<<"G--number of nodes in the file "<<leCreusot->getNodes().size();
//    qDebug()<<"G--number of roads in the file "<<leCreusot->getRoads().size();
//    qDebug()<<"G--number of pois in the file "<<leCreusot->getPOIs().size();
   // RoutePreprocessor::process(&leCreusot);

    //Set dimensions extents for the map
    Metrics::ScaleQuotX = (double)openglRenderer->width()/ (Metrics::maxLon - Metrics::minLon);
    Metrics::ScaleQuotY = (double)openglRenderer->height()/ (Metrics::maxLat - Metrics::minLat);

    glGenBuffers(3, mapBuffer);

    initMapBoundary();;
    glGenBuffers(2,verticesBuffer);
    initVertexData();

    glGenBuffers(2,roadsBuffer);
    glGenBuffers(2,roadQuadsBuffer);
    initRoadData();

    glGenBuffers(2,poiBuffer);
    initPOIData();
    //    //    PathFinderEngine pfe;
    //    pfe.loadInitialization(leCreusot);
    glGenBuffers(1,&selectedPointBuffer);
    glGenBuffers(2,routeBuffer);
    glGenBuffers(1,&selectedPOIBuffer);


    selectedPoiCount=0;
    //Connect POIAdded signal to geometryengine's slot to handle display of added POIs
    connect(MainWindow::instance,SIGNAL(POIAdded(PointOfInterest)),this,SLOT(POIAdded(PointOfInterest)));
    connect(MainWindow::instance,SIGNAL(POIRemoved(PointOfInterest)),this,SLOT(POIRemoved(PointOfInterest)));
    connect(MainWindow::instance,SIGNAL(drawRoute(PathResult)),this,SLOT(drawRoute(PathResult)));
    connect(MainWindow::instance,SIGNAL(drawPOIsInDistance(vector<PointOfInterest>)),this,SLOT(drawPOIsInDistance(vector<PointOfInterest>)));
    connect(MainWindow::instance,SIGNAL(drawPOIsInCategory(QString)),this,SLOT(drawPOIsInCategory(QString)));
    connect(MainWindow::instance,SIGNAL(POIsRemoved(QString)),this,SLOT(POIsRemoved(QString)));
    connect(MainWindow::instance,SIGNAL(clearRoute()),this,SLOT(clearRoute()));
    connect(MainWindow::instance,SIGNAL(clearSelectedPOIs()),this,SLOT(clearSelectedPOIs()));
    connect(openglRenderer,SIGNAL(clicked(QVector2D)),this,SLOT(clicked(QVector2D)));
    routeVerticesCount=0;

    glGenBuffers(1,&startEndRouteBuffer);
    //Initialize VertexData for start and end quads
    initStartEndRouteQuads();
    inited=true;
}

void GeometryEngine::drawStuff()
{
    if(inited){

        drawMap();
        drawVertices();

        drawRoads();
        if(!poisUpdating){
            drawPOIs();
        }
        drawSelectedPoint();
        if(selectedPoiCount>0){
            drawSelectedPOIs();
            drawSelectedPOIImages();
        }

        if(drawPath){
            drawRoute();
            drawRouteStartEnd();
        }

    }
}

void GeometryEngine::drawMap()
{
    glBindBuffer(GL_ARRAY_BUFFER, mapBuffer[0]);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mapBuffer[1]);


    // Offset for position
    offset = 0;

    // Tell OpenGL programmable pipeline how to locate vertex position data
    int vertexLocationBG = program->attributeLocation("a_position");
    program->enableAttributeArray(vertexLocationBG);
    //program->setAttributeBuffer(vertexLocation,GL_FLOAT,offset,3);
    glVertexAttribPointer(vertexLocationBG, 3, GL_FLOAT, GL_FALSE, sizeof(VertexData), (const void *)offset);

    // Offset for texture coordinate
    offset += sizeof(QVector3D);

    // Tell OpenGL programmable pipeline how to locate vertex texture coordinate data
    int texcoordLocationBG = program->attributeLocation("a_texcoord");
    program->enableAttributeArray(texcoordLocationBG);
    glVertexAttribPointer(texcoordLocationBG, 2, GL_FLOAT, GL_FALSE, sizeof(VertexData), (const void *)offset);
    //program->setAttributeBuffer(texcoordLocation,GL_FLOAT,offset,2);
    offset += sizeof(QVector2D);


    glBindBuffer(GL_ARRAY_BUFFER, mapBuffer[2]);
    int colorLocationBG = program->attributeLocation("objectColor");
    program->enableAttributeArray(colorLocationBG);
    glColor3f(0.6f,0.5f,0.6f);
    program->setUniformValue("useColor",true);

    glBindTexture(GL_TEXTURE_2D,textures[0]);
    //glBindTexture(GL_TEXTURE_2D,0);
    //glDrawArrays(GL_TRIANGLE_STRIP,0,4);
    glDrawElements(GL_TRIANGLE_STRIP, ( 4), GL_UNSIGNED_INT, 0);
    //glBindBuffer(GL_ARRAY_BUFFER, 0);
    program->setUniformValue("useColor",false);

}

void GeometryEngine::drawVertices()
{
    //Draw nodes
    glBindBuffer(GL_ARRAY_BUFFER, verticesBuffer[0]);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, verticesBuffer[1]);
    offset=0;
    int vertexLocationNodes = program->attributeLocation("a_position");
    program->enableAttributeArray(vertexLocationNodes);
    glVertexAttribPointer(vertexLocationNodes, 3, GL_FLOAT, GL_FALSE, sizeof(VertexData), (const void *)offset);
    program->setUniformValue("useColor",false);

    // Offset for texture coordinate
    offset += sizeof(QVector3D);

    // Tell OpenGL programmable pipeline how to locate vertex texture coordinate data
    int texcoordLocationNodes = program->attributeLocation("a_texcoord");
    program->enableAttributeArray(texcoordLocationNodes);
    glVertexAttribPointer(texcoordLocationNodes, 2, GL_FLOAT, GL_FALSE, sizeof(VertexData), (const void *)offset);

    //    program->setUniformValue("useColor",true);
    //    glBindTexture(GL_TEXTURE_2D,textures[1]);
    //    glDrawElements(GL_QUADS, ( nodeCount*4), GL_UNSIGNED_INT, 0);
    //    glBindBuffer(GL_ARRAY_BUFFER, 0);
    //  program->setUniformValue("useColor",false);

}

void GeometryEngine::drawRoads()
{
    //Draw roads
    //    glBindBuffer(GL_ARRAY_BUFFER, roadsBuffer[0]);
    //    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, roadsBuffer[1]);
    //    offset=0;
    //    int vertexLocationRoads = program->attributeLocation("a_position");

    //    program->enableAttributeArray(vertexLocationRoads);
    //    glVertexAttribPointer(vertexLocationRoads, 3, GL_FLOAT, GL_FALSE, sizeof(VertexData), (const void *)offset);

    //    // Offset for texture coordinate
    //    offset += sizeof(QVector3D);

    //    // Tell OpenGL programmable pipeline how to locate vertex texture coordinate data
    //    int texcoordLocationRoads = program->attributeLocation("a_texcoord");
    //    program->enableAttributeArray(texcoordLocationRoads);
    //    glVertexAttribPointer(texcoordLocationRoads, 2, GL_FLOAT, GL_FALSE, sizeof(VertexData), (const void *)offset);

    //    glBindTexture(GL_TEXTURE_2D,textures[2]);

    //    glDrawElements(GL_LINES, roadIndexCount, GL_UNSIGNED_INT, 0);
    //    glBindBuffer(GL_ARRAY_BUFFER, 0);

    //Draw road with quads
    glBindBuffer(GL_ARRAY_BUFFER, roadQuadsBuffer[0]);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, roadQuadsBuffer[1]);
    offset=0;
    int vertexLocationRoadQuads = program->attributeLocation("a_position");
    program->enableAttributeArray(vertexLocationRoadQuads);
    glVertexAttribPointer(vertexLocationRoadQuads, 3, GL_FLOAT, GL_FALSE, sizeof(VertexData), (const void *)offset);

    // Offset for texture coordinate
    offset += sizeof(QVector3D);

    // Tell OpenGL programmable pipeline how to locate vertex texture coordinate data
    int texcoordLocationRoadQuads = program->attributeLocation("a_texcoord");
    program->enableAttributeArray(texcoordLocationRoadQuads);
    glVertexAttribPointer(texcoordLocationRoadQuads, 2, GL_FLOAT, GL_FALSE, sizeof(VertexData), (const void *)offset);

    glBindTexture(GL_TEXTURE_2D,textures[3]);
    glDrawElements(GL_QUADS, roadIndexCount*2, GL_UNSIGNED_INT, 0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

}

void GeometryEngine::drawPOIs()
{

    //Draw POIs
    glBindBuffer(GL_ARRAY_BUFFER, poiBuffer[0]);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, poiBuffer[1]);
    offset=0;
    int vertexLocationPOIs = program->attributeLocation("a_position");
    program->enableAttributeArray(vertexLocationPOIs);
    glVertexAttribPointer(vertexLocationPOIs, 3, GL_FLOAT, GL_FALSE, sizeof(VertexData), (const void *)offset);

    // Offset for texture coordinate
    offset += sizeof(QVector3D);

    // Tell OpenGL programmable pipeline how to locate vertex texture coordinate data
    int texcoordLocationPOIs = program->attributeLocation("a_texcoord");
    program->enableAttributeArray(texcoordLocationPOIs);
    glVertexAttribPointer(texcoordLocationPOIs, 2, GL_FLOAT, GL_FALSE, sizeof(VertexData), (const void *)offset);
    glBindTexture(GL_TEXTURE_2D,textures[1]);
    glDrawElements(GL_TRIANGLE_STRIP, poiCount*36-2, GL_UNSIGNED_INT, 0);
    //glBindBuffer(GL_ARRAY_BUFFER, 0);

}

void GeometryEngine::drawSelectedPoint()
{
    //Draw Ray/Selected Point
    glBindBuffer(GL_ARRAY_BUFFER,selectedPointBuffer);

    // Offset for position
    offset = 0;

    // Tell OpenGL programmable pipeline how to locate vertex position data
    int vertexLocationRay= program->attributeLocation("a_position");
    program->enableAttributeArray(vertexLocationRay);
    //program->setAttributeBuffer(vertexLocation,GL_FLOAT,offset,3);
    glVertexAttribPointer(vertexLocationRay, 3, GL_FLOAT, GL_FALSE, sizeof(VertexData), (const void *)offset);

    // Offset for texture coordinate
    offset += sizeof(QVector3D);

    // Tell OpenGL programmable pipeline how to locate vertex texture coordinate data
    int texcoordLocationRay = program->attributeLocation("a_texcoord");
    program->enableAttributeArray(texcoordLocationRay);
    glVertexAttribPointer(texcoordLocationRay, 2, GL_FLOAT, GL_FALSE, sizeof(VertexData), (const void *)offset);
    //program->setAttributeBuffer(texcoordLocation,GL_FLOAT,offset,2);
    offset += sizeof(QVector2D);

    //qDebug()<<"Draw point";
    glColor3f(1.0,1.0,1.0);
    program->setUniformValue("useColor",true);
    glBindTexture(GL_TEXTURE_2D,textures[4]);
    //glBindTexture(GL_TEXTURE_2D,textures);
    glDrawArrays(GL_QUADS,0,4);
program->setUniformValue("useColor",false);
}

void GeometryEngine::drawSelectedPOIs()
{
    //  qDebug()<<"Trying to draw selected pois";

    //Draw selected POIs

    glBindBuffer(GL_ARRAY_BUFFER,selectedPOIBuffer);

    // Offset for position
    offset = 0;

    // Tell OpenGL programmable pipeline how to locate vertex position data
    int vertexLocationSelectedPOIs= program->attributeLocation("a_position");
    program->enableAttributeArray(vertexLocationSelectedPOIs);
    //program->setAttributeBuffer(vertexLocation,GL_FLOAT,offset,3);
    glVertexAttribPointer(vertexLocationSelectedPOIs, 3, GL_FLOAT, GL_FALSE, sizeof(VertexData), (const void *)offset);

    // Offset for texture coordinate
    offset += sizeof(QVector3D);

    // Tell OpenGL programmable pipeline how to locate vertex texture coordinate data
    int texcoordLocationSelectedPOIs = program->attributeLocation("a_texcoord");
    program->enableAttributeArray(texcoordLocationSelectedPOIs);
    glVertexAttribPointer(texcoordLocationSelectedPOIs, 2, GL_FLOAT, GL_FALSE, sizeof(VertexData), (const void *)offset);


    glBindTexture(GL_TEXTURE_2D,textures[5]);

    glDrawArrays(GL_QUADS,0,selectedPoiCount*4);
    //qDebug()<<"Succeeded in drawing selected pois "<<selectedPOIs.size();


}

void GeometryEngine::drawRoute()
{
    //qDebug()<<"Drawing route";
    glBindBuffer(GL_ARRAY_BUFFER, routeBuffer[0]);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, routeBuffer[1]);
    offset=0;
    int vertexLocationRoute = program->attributeLocation("a_position");
    program->enableAttributeArray(vertexLocationRoute);
    glVertexAttribPointer(vertexLocationRoute, 3, GL_FLOAT, GL_FALSE, sizeof(VertexData), (const void *)offset);

    // Offset for texture coordinate
    offset += sizeof(QVector3D);

    // Tell OpenGL programmable pipeline how to locate vertex texture coordinate data
    int texcoordLocationRoute = program->attributeLocation("a_texcoord");
    program->enableAttributeArray(texcoordLocationRoute);
    glVertexAttribPointer(texcoordLocationRoute, 2, GL_FLOAT, GL_FALSE, sizeof(VertexData), (const void *)offset);
    glColor3f(0.5f,0.5f,1.0f);
    program->setUniformValue("useColor",true);
    glBindTexture(GL_TEXTURE_2D,textures[0]);
    glDrawElements(GL_QUADS, routeIndexCount*2, GL_UNSIGNED_INT, 0);
  //  glBindBuffer(GL_ARRAY_BUFFER, 0);
    //qDebug()<<"Drawing route : routeIndexCount: "<<routeIndexCount;
    program->setUniformValue("useColor",false);
}

void GeometryEngine::drawRouteStartEnd()
{
    //qDebug()<<"Drawing route";
    glBindBuffer(GL_ARRAY_BUFFER, startEndRouteBuffer);

    offset=0;
    int vertexLocationStartEndRoute= program->attributeLocation("a_position");
    program->enableAttributeArray(vertexLocationStartEndRoute);
    glVertexAttribPointer(vertexLocationStartEndRoute, 3, GL_FLOAT, GL_FALSE, sizeof(VertexData), (const void *)offset);

    // Offset for texture coordinate
    offset += sizeof(QVector3D);

    // Tell OpenGL programmable pipeline how to locate vertex texture coordinate data
    int texcoordLocationStartEndRoute = program->attributeLocation("a_texcoord");
    program->enableAttributeArray(texcoordLocationStartEndRoute );
    glVertexAttribPointer(texcoordLocationStartEndRoute , 2, GL_FLOAT, GL_FALSE, sizeof(VertexData), (const void *)offset);
    glColor3f(1.0,0,0);
    //program->setUniformValue("useColor",true);
    glBindTexture(GL_TEXTURE_2D,textures[6]);
    glDrawArrays(GL_QUADS,0,8);
    //program->setUniformValue("useColor",false);
}

void GeometryEngine::drawSelectedPOIImages()
{

    //Draw selected POIs
    for( int i=0;i<selectedPOIImagesBuffer.size();i++){


    glBindBuffer(GL_ARRAY_BUFFER,selectedPOIImagesBuffer.at(i));

    // Offset for position
    offset = 0;

    // Tell OpenGL programmable pipeline how to locate vertex position data
    int vertexLocationSelectedPOIs= program->attributeLocation("a_position");
    program->enableAttributeArray(vertexLocationSelectedPOIs);
    //program->setAttributeBuffer(vertexLocation,GL_FLOAT,offset,3);
    glVertexAttribPointer(vertexLocationSelectedPOIs, 3, GL_FLOAT, GL_FALSE, sizeof(VertexData), (const void *)offset);

    // Offset for texture coordinate
    offset += sizeof(QVector3D);

    // Tell OpenGL programmable pipeline how to locate vertex texture coordinate data
    int texcoordLocationSelectedPOIs = program->attributeLocation("a_texcoord");
    program->enableAttributeArray(texcoordLocationSelectedPOIs);
    glVertexAttribPointer(texcoordLocationSelectedPOIs, 2, GL_FLOAT, GL_FALSE, sizeof(VertexData), (const void *)offset);


    glBindTexture(GL_TEXTURE_2D,selectedPOIImagesTextures.at(i));

    glDrawArrays(GL_QUADS,0,4);
    //qDebug()<<"Succeeded in drawing selected pois "<<selectedPOIs.size();
    }
}

void GeometryEngine::initMapBoundary()
{

    mapWidth=mapScale/1000 * openglRenderer->width();
    mapHeight=mapScale/1000 * openglRenderer->height();
    // Vertex data for face 0
//    boundary[0]=VertexData(QVector3D(-mapSize, -mapSize,  mapZ), QVector2D(0, 0));  // v0
//    boundary[1]=VertexData(QVector3D( mapSize, -mapSize,  mapZ), QVector2D(1.0,0/*0.33, 0.0*/)); // v1
//    boundary[2]=VertexData(QVector3D(-mapSize,  mapSize,  mapZ), QVector2D(0,1/*0.0, 0.5*/));  // v2
//    boundary[3]=VertexData(QVector3D( mapSize,  mapSize,  mapZ), QVector2D(1.0,1.0/*0.33, 0.5*/)); // v3

    boundary[0]=VertexData(QVector3D(-mapWidth, -mapHeight,  mapZ), QVector2D(0, 0));  // v0
    boundary[1]=VertexData(QVector3D( mapWidth, -mapHeight,  mapZ), QVector2D(1.0,0/*0.33, 0.0*/)); // v1
    boundary[2]=VertexData(QVector3D(-mapWidth,  mapHeight,  mapZ), QVector2D(0,1/*0.0, 0.5*/));  // v2
    boundary[3]=VertexData(QVector3D( mapWidth,  mapHeight,  mapZ), QVector2D(1.0,1.0/*0.33, 0.5*/)); // v3

    mapIndices=new GLuint[4];
    mapIndices[0]=0;
    mapIndices[1]=1;
    mapIndices[2]=2;
    mapIndices[3]=3;

    GLfloat colors[] = {
        0.0,  1.0,  1.0,  1.0,    // white
        //      1.0,  0.0,  0.0,  1.0,    // red
        //       0.0,  1.0,  0.0,  1.0,    // green
        //       0.0,  0.0,  1.0,  1.0     // blue
    };

    // Transfer vertex data to VBO 0
    glBindBuffer(GL_ARRAY_BUFFER, mapBuffer[0]);
    glBufferData(GL_ARRAY_BUFFER, 4 * sizeof(VertexData), boundary, GL_STATIC_DRAW);

    // Transfer index data to VBO 1
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mapBuffer[1]);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, 4 * sizeof(GLuint), mapIndices, GL_STATIC_DRAW);

    // Transfer color data to VBO 2
    glBindBuffer(GL_ARRAY_BUFFER, mapBuffer[2]);
    glBufferData(GL_ARRAY_BUFFER, 4 * sizeof(GLfloat), colors, GL_STATIC_DRAW);



}
GLfloat GeometryEngine::getMapWidth() const
{
    return mapWidth;
}

void GeometryEngine::setMapWidth(const GLfloat &value)
{
    mapWidth = value;
}

GLfloat GeometryEngine::getMapHeight() const
{
    return mapHeight;
}

void GeometryEngine::setMapHeight(const GLfloat &value)
{
    mapHeight = value;
}





void GeometryEngine::initCube()
{
    //    int cubeZ=3.0;
    //    VertexData vertices[]=
    //        {
    //            // Vertex data for face 0
    //             VertexData(QVector3D(-1.0, -1.0,  1.0+cubeZ), QVector2D(0.0, 0.0)),  // v0
    //             VertexData(QVector3D( 1.0, -1.0,  1.0+cubeZ), QVector2D(0.33, 0.0)), // v1
    //             VertexData(QVector3D(-1.0,  1.0,  1.0+cubeZ), QVector2D(0.0, 0.5)),  // v2
    //             VertexData(QVector3D( 1.0,  1.0,  1.0+cubeZ), QVector2D(0.33, 0.5)), // v3

    //            // Vertex data for face 1
    //             VertexData(QVector3D( 1.0, -1.0,  1.0+cubeZ), QVector2D( 0.0, 0.5)), // v4
    //             VertexData(QVector3D( 1.0, -1.0, -1.0+cubeZ), QVector2D(0.33, 0.5)), // v5
    //             VertexData(QVector3D( 1.0,  1.0,  1.0+cubeZ), QVector2D(0.0, 1.0)),  // v6
    //             VertexData(QVector3D( 1.0,  1.0, -1.0+cubeZ), QVector2D(0.33, 1.0)), // v7

    //            // Vertex data for face 2
    //             VertexData(QVector3D( 1.0, -1.0, -1.0+cubeZ), QVector2D(0.66, 0.5)), // v8
    //             VertexData(QVector3D(-1.0, -1.0, -1.0+cubeZ), QVector2D(1.0, 0.5)),  // v9
    //             VertexData(QVector3D( 1.0,  1.0, -1.0+cubeZ), QVector2D(0.66, 1.0)), // v10
    //             VertexData(QVector3D(-1.0,  1.0, -1.0+cubeZ), QVector2D(1.0, 1.0)),  // v11

    //            // Vertex data for face 3
    //             VertexData(QVector3D(-1.0, -1.0, -1.0+cubeZ), QVector2D(0.66, 0.0)), // v12
    //             VertexData(QVector3D(-1.0, -1.0,  1.0+cubeZ), QVector2D(1.0, 0.0)),  // v13
    //             VertexData(QVector3D(-1.0,  1.0, -1.0+cubeZ), QVector2D(0.66, 0.5)), // v14
    //             VertexData(QVector3D(-1.0,  1.0,  1.0+cubeZ), QVector2D(1.0, 0.5)),  // v15

    //            // Vertex data for face 4
    //             VertexData(QVector3D(-1.0, -1.0, -1.0+cubeZ), QVector2D(0.33, 0.0)), // v16
    //             VertexData(QVector3D( 1.0, -1.0, -1.0+cubeZ), QVector2D(0.66, 0.0)), // v17
    //             VertexData(QVector3D(-1.0, -1.0,  1.0+cubeZ), QVector2D(0.33, 0.5)), // v18
    //             VertexData(QVector3D( 1.0, -1.0,  1.0+cubeZ), QVector2D(0.66, 0.5)), // v19

    //            // Vertex data for face 5
    //             VertexData(QVector3D(-1.0,  1.0,  1.0+cubeZ), QVector2D(0.33, 0.5)), // v20
    //             VertexData(QVector3D( 1.0,  1.0,  1.0+cubeZ), QVector2D(0.66, 0.5)), // v21
    //             VertexData(QVector3D(-1.0,  1.0, -1.0+cubeZ), QVector2D(0.33, 1.0)), // v22
    //             VertexData(QVector3D( 1.0,  1.0, -1.0+cubeZ), QVector2D(0.66, 1.0))  // v23
    //        };
    // For cube we would need only 8 vertices but we have to
    // duplicate vertex for each face because texture coordinate
    // is different.
    VertexData vertices[] = {
        // Vertex data for face 0
        VertexData(QVector3D(-1.0, -1.0,  1.0), QVector2D(0.0, 0.0)),  // v0
        VertexData(QVector3D( 1.0, -1.0,  1.0), QVector2D(0.33, 0.0)), // v1
        VertexData(QVector3D(-1.0,  1.0,  1.0), QVector2D(0.0, 0.5)),  // v2
        VertexData(QVector3D( 1.0,  1.0,  1.0), QVector2D(0.33, 0.5)), // v3

        // Vertex data for face 1
        VertexData(QVector3D( 1.0, -1.0,  1.0), QVector2D( 0.0, 0.5)), // v4
        VertexData(QVector3D( 1.0, -1.0, -1.0), QVector2D(0.33, 0.5)), // v5
        VertexData(QVector3D( 1.0,  1.0,  1.0), QVector2D(0.0, 1.0)),  // v6
        VertexData(QVector3D( 1.0,  1.0, -1.0), QVector2D(0.33, 1.0)), // v7

        // Vertex data for face 2
        VertexData(QVector3D( 1.0, -1.0, -1.0), QVector2D(0.66, 0.5)), // v8
        VertexData(QVector3D(-1.0, -1.0, -1.0), QVector2D(1.0, 0.5)),  // v9
        VertexData(QVector3D( 1.0,  1.0, -1.0), QVector2D(0.66, 1.0)), // v10
        VertexData(QVector3D(-1.0,  1.0, -1.0), QVector2D(1.0, 1.0)),  // v11

        // Vertex data for face 3
        VertexData(QVector3D(-1.0, -1.0, -1.0), QVector2D(0.66, 0.0)), // v12
        VertexData(QVector3D(-1.0, -1.0,  1.0), QVector2D(1.0, 0.0)),  // v13
        VertexData(QVector3D(-1.0,  1.0, -1.0), QVector2D(0.66, 0.5)), // v14
        VertexData(QVector3D(-1.0,  1.0,  1.0), QVector2D(1.0, 0.5)),  // v15

        // Vertex data for face 4
        VertexData(QVector3D(-1.0, -1.0, -1.0), QVector2D(0.33, 0.0)), // v16
        VertexData(QVector3D( 1.0, -1.0, -1.0), QVector2D(0.66, 0.0)), // v17
        VertexData(QVector3D(-1.0, -1.0,  1.0), QVector2D(0.33, 0.5)), // v18
        VertexData(QVector3D( 1.0, -1.0,  1.0), QVector2D(0.66, 0.5)), // v19

        // Vertex data for face 5
        VertexData(QVector3D(-1.0,  1.0,  1.0), QVector2D(0.33, 0.5)), // v20
        VertexData(QVector3D( 1.0,  1.0,  1.0), QVector2D(0.66, 0.5)), // v21
        VertexData(QVector3D(-1.0,  1.0, -1.0), QVector2D(0.33, 1.0)), // v22
        VertexData(QVector3D( 1.0,  1.0, -1.0), QVector2D(0.66, 1.0))  // v23
    };

    // Indices for drawing cube faces using triangle strips.
    // Triangle strips can be connected by duplicating indices
    // between the strips. If connecting strips have opposite
    // vertex order then last index of the first strip andr first
    // index of the second strip needs to be duplicated. If
    // connecting strips have same vertex order then only last
    // index of the first strip needs to be duplicated.
    GLuint indices[] = {
        0,  1,  2,  3,  3,     // Face 0 - triangle strip ( v0,  v1,  v2,  v3)
        4,  4,  5,  6,  7,  7, // Face 1 - triangle strip ( v4,  v5,  v6,  v7)
        8,  8,  9, 10, 11, 11, // Face 2 - triangle strip ( v8,  v9, v10, v11)
        12, 12, 13, 14, 15, 15, // Face 3 - triangle strip (v12, v13, v14, v15)
        16, 16, 17, 18, 19, 19, // Face 4 - triangle strip (v16, v17, v18, v19)
        20, 20, 21, 22, 23      // Face 5 - triangle strip (v20, v21, v22, v23)
    };

    glBindBuffer(GL_ARRAY_BUFFER, vboIds[0]);
    glBufferData(GL_ARRAY_BUFFER, 24 * sizeof(VertexData), vertices, GL_STATIC_DRAW);

    // Transfer index data to VBO 1
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,vboIds[1]);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, 34 * sizeof(GLuint), indices, GL_STATIC_DRAW);

    //    // Offset for position
    //    quintptr offset = 0;

    //    // Tell OpenGL programmable pipeline how to locate vertex position data
    //    int vertexLocation = program->attributeLocation("a_position");
    //    program->enableAttributeArray(vertexLocation);
    //    glVertexAttribPointer(vertexLocation, 3, GL_FLOAT, GL_FALSE, sizeof(VertexData), (const void *)offset);

    //    // Offset for texture coordinate
    //    offset += sizeof(QVector3D);

    //    // Tell OpenGL programmable pipeline how to locate vertex texture coordinate data
    //    int texcoordLocation = program->attributeLocation("a_texcoord");
    //    program->enableAttributeArray(texcoordLocation);
    //    glVertexAttribPointer(texcoordLocation, 2, GL_FLOAT, GL_FALSE, sizeof(VertexData), (const void *)offset);

}

void GeometryEngine::initVertexData()
{

    std::map<long, Node>::iterator nodeit;
    //n*4 + (n-1)*2 . The (n-1)*2 is used for proper indexing between each quad
    //e.g. for 4 vertices, indices are 0,1,2,3
    //for 8 vertices, indices are 0,1,2,3, 3,4, 4,5,6,7 to draw quads properly.
    //Thus n*4 +(n-1)*2 = n*6-2;
    //int indexCount=dbHandler.getNodeCount()*6-2;
    // const int nodeCount=dbHandler.getNodeCount();
    //const int indexCount=nodeCount*6-2;
    //const int indexCount=nodeCount*4;
    nodeCount=leCreusot->getNodes().size();

    //VertexData vertices[nodeCount*4];
    //GLushort indices[indexCount];
    vertices=new VertexData[nodeCount*4];
    indices=new GLuint[nodeCount*4];
    //Default quad size
    GLfloat quadSize=0.01f;
    int index=0;
    int i=0;
    Z=nodeZ;
    Node node;
    for(nodeit=leCreusot->getNodes().begin();nodeit!=leCreusot->getNodes().end();++nodeit,++i){
        node=nodeit->second;

        //Convert latitudes longitudes to 3D coordinates
        GLfloat Y=mapScale*Metrics::toMapCoordY(node.getLatitude())/openglRenderer->height()-mapScale/2;
        GLfloat X=mapScale*Metrics::toMapCoordX(node.getLongitude())/openglRenderer->width()-mapScale/2;
        // qDebug()<<i<<":"<<node.ID()<<":"<<X<<","<<Y;
        //Build a quad from X,Y and Z
        vertices[4*i]= VertexData(QVector3D(X-quadSize/2, Y-quadSize/2,  Z), QVector2D(0.0, 0.0));
        indices[4*i]=index++;

        vertices[4*i+1]=VertexData(QVector3D(X-quadSize/2,  Y+quadSize/2,  Z), QVector2D(0.0, 1.0));
        indices[4*i+1]=index++;


        vertices[4*i+2]=VertexData(QVector3D( X+quadSize/2,  Y+quadSize/2,  Z), QVector2D(1.0, 1.0));
        indices[4*i+2]=index++;
        vertices[4*i+3]=VertexData(QVector3D( X+quadSize/2, Y-quadSize/2,  Z), QVector2D(1.0, 0.0));
        indices[4*i+3]=index++;

    }


    glBindBuffer(GL_ARRAY_BUFFER, verticesBuffer[0]);
    glBufferData(GL_ARRAY_BUFFER, nodeCount *4* sizeof(VertexData), vertices, GL_STATIC_DRAW);
    //qDebug()<<"Vertices Buffer initializing "<<nodeCount;
    // Transfer index data to VBO 1
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, verticesBuffer[1]);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER,( nodeCount*4) * sizeof(GLuint), indices, GL_STATIC_DRAW);

}

//!Set up the vertices for Points of Interest
void GeometryEngine::initPOIData()
{
    PointOfInterest poi;
    Z=poiZ;
    GLfloat quadSize=0.01f;
    poiCount=leCreusot->getPOIs().size();

    poiVertices=new VertexData[poiCount * 24];
    poiIndices=new GLuint[poiCount * 36-2];
    int index=0;
    GLfloat cubeWidth=0.005f,cubeHeight=0.005f;

    for(int i=0;i<leCreusot->getPOIs().size();i++){
        poi=leCreusot->getPOIs().at(i);
        GLfloat X=mapScale*Metrics::toMapCoordX(poi.getLongitude())/openglRenderer->width()-mapScale/2;
        GLfloat Y=mapScale*Metrics::toMapCoordY(poi.getLatitude())/openglRenderer->height()-mapScale/2;
        // qDebug()<<X<<","<<Z;


        //Build a quad from X,Y and Z
        //        poiVertices[5*i]= VertexData(QVector3D(X-quadSize/2, Y-quadSize/2,  Z), QVector2D(0.0, 0.0));


        //        poiVertices[5*i+1]=VertexData(QVector3D(X-quadSize/2,  Y+quadSize/2,  Z), QVector2D(0.0, 1.0));



        //        poiVertices[5*i+2]=VertexData(QVector3D( X+quadSize/2,  Y+quadSize/2,  Z), QVector2D(1.0, 1.0));

        //        poiVertices[5*i+3]=VertexData(QVector3D( X+quadSize/2, Y-quadSize/2,  Z), QVector2D(1.0, 0.0));

        //        poiVertices[5*i+4]=VertexData(QVector3D( X, Y,  Z-0.01), QVector2D(1.0, 0.0));

        // Vertex data for face 0
        poiVertices[24*i]=VertexData(QVector3D(X-cubeWidth, Y-cubeWidth, Z+ cubeHeight), QVector2D(0.0, 0.0));  // v0
        poiVertices[24*i+1]=VertexData(QVector3D(X+ cubeWidth, Y-cubeWidth,Z+  cubeHeight), QVector2D(0.33, 0.0)); // v1
        poiVertices[24*i+2]=VertexData(QVector3D(X-cubeWidth,  Y+cubeWidth, Z+ cubeHeight), QVector2D(0.0, 0.5));  // v2
        poiVertices[24*i+3]=VertexData(QVector3D(X+ cubeWidth, Y+ cubeWidth, Z+ cubeHeight), QVector2D(0.33, 0.5)); // v3


        // Vertex data for face 1
        poiVertices[24*i+4]=VertexData(QVector3D( X+cubeWidth, Y-cubeWidth, Z+ cubeHeight), QVector2D( 0.0, 0.5)); // v4
        poiVertices[24*i+5]=VertexData(QVector3D( X+cubeWidth*0.5, Y-cubeWidth*0.5, Z-cubeHeight), QVector2D(0.33, 0.5)); // v5
        poiVertices[24*i+6]=VertexData(QVector3D( X+cubeWidth,  Y+cubeWidth,Z+  cubeHeight), QVector2D(0.0, 1.0));  // v6
        poiVertices[24*i+7]=VertexData(QVector3D( X+cubeWidth*0.5,  Y+cubeWidth*0.5, Z-cubeHeight), QVector2D(0.33, 1.0)); // v7

        // Vertex data for face 2
        poiVertices[24*i+8]=VertexData(QVector3D( X+cubeWidth*0.5, Y-cubeWidth*0.5, Z-cubeHeight), QVector2D(0.66, 0.5)); // v8
        poiVertices[24*i+9]=VertexData(QVector3D(X-cubeWidth*0.5, Y-cubeWidth*0.5, Z-cubeHeight), QVector2D(1.0, 0.5));  // v9
        poiVertices[24*i+10]=VertexData(QVector3D(X+ cubeWidth*0.5,  Y+cubeWidth*0.5, Z-cubeHeight), QVector2D(0.66, 1.0)); // v10
        poiVertices[24*i+11]=VertexData(QVector3D(X-cubeWidth*0.5,  Y+cubeWidth*0.5, Z-cubeHeight), QVector2D(1.0, 1.0));  // v11

        // Vertex data for face 3
        poiVertices[24*i+12]=VertexData(QVector3D(X-cubeWidth*0.5, Y-cubeWidth*0.5, Z-cubeHeight), QVector2D(0.66, 0.0)); // v12
        poiVertices[24*i+13]=VertexData(QVector3D(X-cubeWidth, Y-cubeWidth, Z+ cubeHeight), QVector2D(1.0, 0.0));  // v13
        poiVertices[24*i+14]=VertexData(QVector3D(X-cubeWidth*0.5,  Y+cubeWidth*0.5, Z-cubeHeight), QVector2D(0.66, 0.5)); // v14
        poiVertices[24*i+15]=VertexData(QVector3D(X-cubeWidth,  Y+cubeWidth, Z+ cubeHeight), QVector2D(1.0, 0.5));  // v15

        // Vertex data for face 4
        poiVertices[24*i+16]=VertexData(QVector3D(X-cubeWidth*0.5, Y-cubeWidth*0.5, Z-cubeHeight), QVector2D(0.33, 0)); // v16
        poiVertices[24*i+17]=VertexData(QVector3D( X+cubeWidth*0.5, Y-cubeWidth*0.5, Z-cubeHeight), QVector2D(0.66, 0.0)); // v17
        poiVertices[24*i+18]=VertexData(QVector3D(X-cubeWidth, Y-cubeWidth,  Z+cubeHeight), QVector2D(0.33, 0.5)); // v18
        poiVertices[24*i+19]=VertexData(QVector3D( X+cubeWidth, Y-cubeWidth,  Z+cubeHeight), QVector2D(0.66, 0.5)); // v19

        // Vertex data for face 5
        poiVertices[24*i+20]=VertexData(QVector3D(X-cubeWidth,  Y+cubeWidth, Z+ cubeHeight), QVector2D(0.33, 0.5)); // v20
        poiVertices[24*i+21]=VertexData(QVector3D( X+cubeWidth,  Y+cubeWidth, Z+ cubeHeight), QVector2D(0.66, 0.5)); // v21
        poiVertices[24*i+22]=VertexData(QVector3D(X-cubeWidth*0.5,  Y+cubeWidth*0.5, Z-cubeHeight), QVector2D(0.33, 1.0)); // v22
        poiVertices[24*i+23]=VertexData(QVector3D( X+cubeWidth*0.5,  Y+cubeWidth*0.5, Z-cubeHeight), QVector2D(0.66, 1.0));  // v23

    }

    for(int i=0;i<leCreusot->getPOIs().size()*36-2;i++){
        poiIndices[i]=index;
        if(!((i-3)%6==0 || (i-5)%6==0)){
            ++index;
        }

    }

    glBindBuffer(GL_ARRAY_BUFFER, poiBuffer[0]);
    glBufferData(GL_ARRAY_BUFFER, poiCount *24* sizeof(VertexData), poiVertices, GL_STATIC_DRAW);

    // Transfer index data to VBO 1
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, poiBuffer[1]);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER,( poiCount*36-2) * sizeof(GLuint), poiIndices, GL_STATIC_DRAW);


    //    poiVertices=new VertexData[poiCount * 4];
    //    poiIndices=new GLuint[poiCount * 4];
    //    int index=0;
    //    for(int i=0;i<leCreusot->getPOIs().size();i++){
    //        poi=leCreusot->getPOIs().at(i);
    //        GLfloat X=mapScale*Metrics::toMapCoordX(poi.getLongitude())/openglRenderer->width()-mapScale/2;
    //        GLfloat Y=mapScale*Metrics::toMapCoordY(poi.getLatitude())/openglRenderer->height()-mapScale/2;
    //       // qDebug()<<X<<","<<Z;


    //        //Build a quad from X,Y and Z
    //        poiVertices[4*i]= VertexData(QVector3D(X-quadSize/2, Y-quadSize/2,  Z), QVector2D(0.0, 0.0));
    //        poiIndices[4*i]=index++;

    //        poiVertices[4*i+1]=VertexData(QVector3D(X-quadSize/2,  Y+quadSize/1,  Z), QVector2D(0.0, 1.0));
    //        poiIndices[4*i+1]=index++;


    //        poiVertices[4*i+2]=VertexData(QVector3D( X+quadSize/2,  Y+quadSize/1,  Z), QVector2D(1.0, 1.0));
    //        poiIndices[4*i+2]=index++;
    //        poiVertices[4*i+3]=VertexData(QVector3D( X+quadSize/2, Y-quadSize/2,  Z), QVector2D(1.0, 0.0));
    //        poiIndices[4*i+3]=index++;
    //   }

    //    glBindBuffer(GL_ARRAY_BUFFER, poiBuffer[0]);
    //    glBufferData(GL_ARRAY_BUFFER, poiCount *4* sizeof(VertexData), poiVertices, GL_STATIC_DRAW);

    //    // Transfer index data to VBO 1
    //    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, poiBuffer[1]);
    //    glBufferData(GL_ELEMENT_ARRAY_BUFFER,( poiCount*4) * sizeof(GLuint), poiIndices, GL_STATIC_DRAW);
}

void GeometryEngine::reinitPOIData()
{
    qDebug()<<"Got request to update poi in view...";
    //Remove old poiBuffer and create new one
    poisUpdating=true;
    glDeleteBuffers(2,poiBuffer);
    glGenBuffers(2,poiBuffer);
    initPOIData();
    poisUpdating=false;
    openglRenderer->updateGL();
}



void GeometryEngine::initSelectedPOIData(bool poiRemoved=false)
{

    //Update the list of selected POIs to draw pins

    PointOfInterest poi;
    if(selectedPoiCount>0){
        //   delete &selectedPOIs;
        selectedPoiCount=0;
        //glDeleteBuffers(1,&selectedPOIBuffer);
    }

    //glGenBuffers(1,&selectedPOIBuffer);
    selectedPoiVertices=new VertexData[selectedPOIs.size()*4];
    //qDebug()<<"SElected poi count"<<selectedPOIs.size();
    GLfloat X=0,Y=0,Z= poiZ+0.01f,quadSize=0.01f;
    GLfloat minX=Metrics::maxX,maxX=Metrics::minX,minY=Metrics::maxY,maxY=Metrics::minY;
    for(int i=0;i<selectedPOIs.size();i++){
        poi=selectedPOIs.at(i);

        //Convert latitudes longitudes to 3D coordinates
        Y=mapScale*Metrics::toMapCoordY(poi.getLatitude())/openglRenderer->height()-mapScale/2;
        X=mapScale*Metrics::toMapCoordX(poi.getLongitude())/openglRenderer->width()-mapScale/2;
        if(minX>X)
            minX=X;
        if(minY>Y)
            minY=Y;
        if(maxX<X)
            maxX=X;
        if(maxY<Y)
            maxX=Y;

            qDebug()<<i<<":"<<poi.getName()<<poi.getLatitude()<<":"<<poi.getLongitude()<<":"<<X<<","<<Y;
        //Build a quad from X,Y and Z
        selectedPoiVertices[4*i]= VertexData(QVector3D(X-quadSize/2*1.5, Y-quadSize/2,  Z), QVector2D(0.0, 0.0));
        selectedPoiVertices[4*i+1]=VertexData(QVector3D(X-quadSize/2*1.5,  Y+quadSize/2,  Z), QVector2D(0.0, 1.0));
        selectedPoiVertices[4*i+2]=VertexData(QVector3D( X+quadSize/2*1.5,  Y+quadSize/2,  Z), QVector2D(1.0, 1.0));
        selectedPoiVertices[4*i+3]=VertexData(QVector3D( X+quadSize/2*1.5, Y-quadSize/2,  Z), QVector2D(1.0, 0.0));


    }

    if(!poiRemoved){
    //Find X,Y,Z that covers all the pois
        Z=(maxY-minY)*(openglRenderer->getMinWorldZ())/(Metrics::maxY-Metrics::minY);
  //   X=minX+(maxX-minX)/2;
   // Y=minY+(maxY-minX)/2;
    qDebug()<<"Translate to : "<<X<<","<<Y<<","<<Z;
    openglRenderer->translateTo(-X,-Y,Z-1.5);
    }
    glBindBuffer(GL_ARRAY_BUFFER, selectedPOIBuffer);
    glBufferData(GL_ARRAY_BUFFER, selectedPOIs.size()*4* sizeof(VertexData), selectedPoiVertices, GL_STATIC_DRAW);
    selectedPoiCount= selectedPOIs.size();
    openglRenderer->updateGL();
}

void GeometryEngine::initRouteData(PathResult pathResult)
{

    GLfloat startX=0,startY=0,endX=0,endY=0;
    //Set up route vertices
    if(routeVerticesCount>0){
        routeVerticesCount=0;
        //glDeleteBuffers(2,routeBuffer);
    }
    //  glGenBuffers(2,routeBuffer);


    std::vector<Node> routeNodes=pathResult.getnodes();
    //qDebug()<<routeNodes.size()<<" nodes in route";


    routeVerticesCount=routeNodes.size();
    routeIndexCount = 2*(routeVerticesCount- 1);

    routeIndices=new GLuint[routeIndexCount];
    int routeIndicesIndex=0;

    //Set up road vertices
    //Will need to change this to iterate through the nodes of the actual route
    for(int i=1;i<routeIndexCount;i++){
        routeIndices[i-1]=routeIndicesIndex;
        ++routeIndicesIndex;
        routeIndices[i++]=routeIndicesIndex;
        //qDebug()<<"Init route :"<<routeIndices[i-2]<<","<<routeIndices[i-1]<<","<<routeIndices[i];
    }



    //For every road segment, we draw a quad instead of a line.
    routeQuads=new VertexData[routeIndexCount*2];
    routeQuadIndices=new GLuint[routeIndexCount*2];

    GLfloat routeWidth=0.004;
    GLfloat X1=0.0f,Y1=0.0f,X2=0.0f,Y2=0.0f;
    Z=roadQuadZ+0.01f;
    GLfloat minX=Metrics::maxX,maxX=Metrics::minX,minY=Metrics::maxY,maxY=Metrics::minY;

    for(int i=0;i<routeIndexCount;i++){
       // qDebug()<<"Route node Index "<<i;
        //Find the extreme pois in this list and adjust the viewport such that all of them are visible

        if(i%2!=0){
            //Find the slope of the line routeVertices[i-1]---routeVertices[i]
            //Find two points on each side of roadVertices[i-1] and roadVertices[i] at a distance of
            //roadWidth/2 along the line perpendicular to the line joining those two vertices.
            Y2=mapScale*Metrics::toMapCoordY(routeNodes.at(routeIndices[i]).getLatitude())/openglRenderer->height()-mapScale/2;
            X2=mapScale*Metrics::toMapCoordX(routeNodes.at(routeIndices[i]).getLongitude())/openglRenderer->width()-mapScale/2;
            Y1=mapScale*Metrics::toMapCoordY(routeNodes.at(routeIndices[i-1]).getLatitude())/openglRenderer->height()-mapScale/2;
            X1=mapScale*Metrics::toMapCoordX(routeNodes.at(routeIndices[i-1]).getLongitude())/openglRenderer->width()-mapScale/2;
            //Store the startX and startY vertex position to draw start button
            if(i-1 ==0){
                startX=X1; startY=Y1;
            }
            //Store the endX and endY
            endX=X2; endY=Y2;

            if(minX>X2)
                minX=X2;
            if(minY>Y2)
                minY=Y2;
            if(maxX<X2)
                maxX=X2;
            if(maxY<Y2)
                maxX=Y2;

            GLfloat alpha=atan2(Y2-Y1,X2-X1);
            //4 corners of the rectangle around the line
            //Note on indices: For every two road indices, we generate a rectangle
            //i.e For nodes at indices 1, 3, 5, 7 etc., we need to generate 4 vertices
            //i=1,    We generate rectangle for roadvertices 0 and 1.
            //--- 4*(1-1)/2 + {0,1,2,3} = 0,1,2,3
            //i=3
            //-- 4*(3-1)/2+{0,1,2,3}=4,5,6,7
            //i=5
            //-- 4*(5-1)/2+{0,1,2,3}=8,9,10,11 etc.
            routeQuads[2*(i-1)]=VertexData(QVector3D(X1 + routeWidth * sin(alpha),Y1 - routeWidth *cos(alpha),Z),QVector2D(0,0));
            routeQuadIndices[2*(i-1)]=2*(i-1);

            routeQuads[2*(i-1)+1]=VertexData(QVector3D(X2 + routeWidth  * sin(alpha),Y2 - routeWidth *cos(alpha),Z),QVector2D(1,0));
            routeQuadIndices[2*(i-1)+1]=2*(i-1)+1;


            routeQuads[2*(i-1)+2]=VertexData(QVector3D(X2 - routeWidth  * sin(alpha),Y2 + routeWidth *cos(alpha),Z),QVector2D(1,1));
            routeQuadIndices[2*(i-1)+2]=2*(i-1)+2;

            routeQuads[2*(i-1)+3]=VertexData(QVector3D(X1 - routeWidth * sin(alpha),Y1 + routeWidth *cos(alpha),Z),QVector2D(0,1));
            routeQuadIndices[2*(i-1)+3]=2*(i-1)+3;

        }

    }

    initStartEndRouteData(startX,startY,endX,endY);

    Z=(maxY-minY)*(openglRenderer->getMinWorldZ())/(Metrics::maxY-Metrics::minY);
    //GLfloat X=minX+(maxX-minX)/2;
    //GLfloat Y=minY+(maxY-minX)/2;

    openglRenderer->translateTo(-startX,-startY,Z-1.5);

    //qDebug()<<"Inited route data";
}

void GeometryEngine::clearRouteData()
{
    //Set up route vertices
    if(routeVerticesCount>0){
        routeVerticesCount=0;
        glDeleteBuffers(2,routeBuffer);
    }
      glGenBuffers(2,routeBuffer);


    routeIndexCount = 0;

    drawPath=false;

}



//void GeometryEngine::initRouteData(vector<PointOfInterest> pois)
//{
//    GLfloat startX=0,startY=0,endX=0,endY=0;
//    //Set up route vertices
//    if(routeVerticesCount>0){
//        routeVerticesCount=0;
//        //glDeleteBuffers(2,routeBuffer);
//    }
//    //  glGenBuffers(2,routeBuffer);

//    std::vector<PointOfInterest> routeNodes=pois;
//    qDebug()<<routeNodes.size()<<" nodes in route";


//    routeVerticesCount=routeNodes.size();
//    routeIndexCount = 2*(routeVerticesCount- 1);

//    routeIndices=new GLuint[routeIndexCount];
//    int routeIndicesIndex=0;

//    //Set up road vertices
//    //Will need to change this to iterate through the nodes of the actual route
//    for(int i=1;i<routeIndexCount;i++){
//        routeIndices[i-1]=routeIndicesIndex;
//        ++routeIndicesIndex;
//        routeIndices[i++]=routeIndicesIndex;
//        //qDebug()<<"Init route :"<<routeIndices[i-2]<<","<<routeIndices[i-1]<<","<<routeIndices[i];
//    }



//    //For every road segment, we draw a quad instead of a line.
//    routeQuads=new VertexData[routeIndexCount*2];
//    routeQuadIndices=new GLuint[routeIndexCount*2];

//    GLfloat routeWidth=0.004;
//    GLfloat X1=0.0f,Y1=0.0f,X2=0.0f,Y2=0.0f;
//    Z=roadQuadZ+0.01f;
//    GLfloat minX=Metrics::maxX,maxX=Metrics::minX,minY=Metrics::maxY,maxY=Metrics::minY;

//    for(int i=0;i<routeIndexCount;i++){
//       // qDebug()<<"Route node Index "<<i;
//        //Find the extreme pois in this list and adjust the viewport such that all of them are visible

//        if(i%2!=0){
//            //Find the slope of the line routeVertices[i-1]---routeVertices[i]
//            //Find two points on each side of roadVertices[i-1] and roadVertices[i] at a distance of
//            //roadWidth/2 along the line perpendicular to the line joining those two vertices.
//            Y2=mapScale*Metrics::toMapCoordY(routeNodes.at(routeIndices[i]).getLatitude())/openglRenderer->height()-mapScale/2;
//            X2=mapScale*Metrics::toMapCoordX(routeNodes.at(routeIndices[i]).getLongitude())/openglRenderer->width()-mapScale/2;
//            Y1=mapScale*Metrics::toMapCoordY(routeNodes.at(routeIndices[i-1]).getLatitude())/openglRenderer->height()-mapScale/2;
//            X1=mapScale*Metrics::toMapCoordX(routeNodes.at(routeIndices[i-1]).getLongitude())/openglRenderer->width()-mapScale/2;
//            //Store the startX and startY vertex position to draw start button
//            if(i-1 ==0){
//                startX=X1; startY=Y1;
//            }
//            //Store the endX and endY
//            endX=X2; endY=Y2;

//            if(minX>X2)
//                minX=X2;
//            if(minY>Y2)
//                minY=Y2;
//            if(maxX<X2)
//                maxX=X2;
//            if(maxY<Y2)
//                maxX=Y2;

//            GLfloat alpha=atan2(Y2-Y1,X2-X1);
//            //4 corners of the rectangle around the line
//            //Note on indices: For every two road indices, we generate a rectangle
//            //i.e For nodes at indices 1, 3, 5, 7 etc., we need to generate 4 vertices
//            //i=1,    We generate rectangle for roadvertices 0 and 1.
//            //--- 4*(1-1)/2 + {0,1,2,3} = 0,1,2,3
//            //i=3
//            //-- 4*(3-1)/2+{0,1,2,3}=4,5,6,7
//            //i=5
//            //-- 4*(5-1)/2+{0,1,2,3}=8,9,10,11 etc.
//            routeQuads[2*(i-1)]=VertexData(QVector3D(X1 + routeWidth * sin(alpha),Y1 - routeWidth *cos(alpha),Z),QVector2D(0,0));
//            routeQuadIndices[2*(i-1)]=2*(i-1);

//            routeQuads[2*(i-1)+1]=VertexData(QVector3D(X2 + routeWidth  * sin(alpha),Y2 - routeWidth *cos(alpha),Z),QVector2D(1,0));
//            routeQuadIndices[2*(i-1)+1]=2*(i-1)+1;


//            routeQuads[2*(i-1)+2]=VertexData(QVector3D(X2 - routeWidth  * sin(alpha),Y2 + routeWidth *cos(alpha),Z),QVector2D(1,1));
//            routeQuadIndices[2*(i-1)+2]=2*(i-1)+2;

//            routeQuads[2*(i-1)+3]=VertexData(QVector3D(X1 - routeWidth * sin(alpha),Y1 + routeWidth *cos(alpha),Z),QVector2D(0,1));
//            routeQuadIndices[2*(i-1)+3]=2*(i-1)+3;

//        }

//    }

//    initStartEndRouteData(startX,startY,endX,endY);

//    Z=(maxY-minY)*(openglRenderer->getMinWorldZ())/(Metrics::maxY-Metrics::minY);
//    GLfloat X=minX+(maxX-minX)/2;
//    GLfloat Y=minY+(maxY-minX)/2;

//    openglRenderer->translate(-X,-Y-1,Z-1.5);

//    qDebug()<<"Inited route data for pois";
//}

////!This function does not actually set up the route for drawing, but sets up number of Points of Interest within a given range
//void GeometryEngine::initRouteData(Node startNode)
//{
//    qDebug()<<"In initroute data";
//    //This function is going to draw POIs within a certain distance range
//    float distance=1; //km
//    //Set up route vertices
//    if(routeVerticesCount>0){
//        routeVerticesCount=0;
//        //glDeleteBuffers(2,routeBuffer);
//    }
//    //  glGenBuffers(2,routeBuffer);

//    std::vector<PointOfInterest>::iterator it;

//    //PathResult* result = MainWindow::instance->getPathFinderengine().findPath(selectedPOIs.at(0).getId(),selectedPOIs.at(selectedPoiCount-1).getId(),true);

//    //std::vector<PointOfInterest> routeNodes=MainWindow::instance->getPathFinderengine().findPoiWithinDistance(distance/3,"Education",startNode.getId());

//    selectedPOIs.clear();

//    //Find the extreme pois in this list and adjust the viewport such that all of them are visible
//    for(int i=0;i<routeNodes.size();i++){

//        //Get the worldZ for this range of Latitudes
//        selectedPOIs.push_back(routeNodes.at(i));
//    }



//    initSelectedPOIData();
//}

void GeometryEngine::initStartEndRouteQuads()
{
    startEndRouteQuads=new VertexData[8];

}

void GeometryEngine::initStartEndRouteData(GLfloat startX, GLfloat startY, GLfloat endX, GLfloat endY)
{
    GLfloat quadSize=0.04f;
    //Offset startX and endX a bit to the right
    startX+=0.01f;
    endX+=0.01f;
    Z=poiZ+0.01f;
    //Start route quad, left half of the texture startEnd.png

    startEndRouteQuads[0]=VertexData(QVector3D(startX-quadSize/2, startY-quadSize/2,  Z), QVector2D(0.0,0));
    startEndRouteQuads[1]=VertexData(QVector3D(startX-quadSize/2,  startY+quadSize/2,  Z), QVector2D(0.0, 1.0));
    startEndRouteQuads[2]=VertexData(QVector3D( startX+quadSize/2,  startY+quadSize/2,  Z), QVector2D(0.5, 1.0));
    startEndRouteQuads[3]=VertexData(QVector3D( startX+quadSize/2, startY-quadSize/2,  Z), QVector2D(0.5, 0));


    //End route quad, right half of the texture startEnd.png
    startEndRouteQuads[4]=VertexData(QVector3D(endX-quadSize/2, endY-quadSize/2,  Z), QVector2D(0.5, 0.0));
    startEndRouteQuads[5]=VertexData(QVector3D(endX-quadSize/2,  endY+quadSize/2,  Z), QVector2D(0.5, 1.0));
    startEndRouteQuads[6]=VertexData(QVector3D( endX+quadSize/2,  endY+quadSize/2,  Z), QVector2D(1, 1.0));
    startEndRouteQuads[7]=VertexData(QVector3D( endX+quadSize/2, endY-quadSize/2,  Z), QVector2D(1.0, 0));

    glBindBuffer(GL_ARRAY_BUFFER, startEndRouteBuffer);
    glBufferData(GL_ARRAY_BUFFER, 8 * sizeof(VertexData), startEndRouteQuads, GL_STATIC_DRAW);

}

void GeometryEngine::initPOIImageBuffer(PointOfInterest poi)
{
    //qDebug()<<" Init poi images "<<poi.getId();
    //Get the image from disk based on the id
    //For now get the image from disk
    QFileInfo info(RoutePreprocessor::xmlDataFolder);
    QDir dir=info.dir();

    //Get parent directory of XML_Data folder
    if(dir.cdUp()){
    QString path=dir.path()+"/Images/" +QString::number(poi.getId())+".jpg";
    //qDebug()<<"Init POI Image buffer:"<<path;

    QFile file(path);
    if(file.exists()){
   // qDebug()<<path;
    //The path works
    GLuint imageBuffer,texture;
    glGenBuffers(1,&imageBuffer);

    //Generate texture and save in list
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    texture =openglRenderer->bindTexture(QImage(path));


    //Generate vertices and save in list
    VertexData* vertexData=new VertexData[4];


    GLfloat Y=mapScale*Metrics::toMapCoordY(poi.getLatitude())/openglRenderer->height()-mapScale/2;
    GLfloat X=mapScale*Metrics::toMapCoordX(poi.getLongitude())/openglRenderer->width()-mapScale/2;
    //Offset the image display a bit
    X-=0.02f; Y+=0.04f;
    GLfloat quadSize=0.05f;
    Z=poiZ+0.02f;
    vertexData[0]=VertexData(QVector3D(X-quadSize/2, Y-quadSize/2,  Z), QVector2D(0.0,0));
    vertexData[1]=VertexData(QVector3D(X-quadSize/2,  Y+quadSize/2,  Z), QVector2D(0.0, 1.0));
    vertexData[2]=VertexData(QVector3D(X+quadSize/2,  Y+quadSize/2,  Z), QVector2D(1, 1.0));
    vertexData[3]=VertexData(QVector3D( X+quadSize/2, Y-quadSize/2,  Z), QVector2D(1, 0));

    glBindBuffer(GL_ARRAY_BUFFER, imageBuffer);
    glBufferData(GL_ARRAY_BUFFER, 4 * sizeof(VertexData), vertexData, GL_STATIC_DRAW);

    //Save everything to our vector
    selectedPOIImagesBuffer.push_back(imageBuffer);
    selectedPOIImagesTextures.push_back(texture);
    selectedPOIImagesVertices.push_back(vertexData);
    }
       //qDebug()<<"No image for the selected poi found" ;
    }
    else{
        qDebug()<<"Check the path QRoutePreProcessor::xmlDataFolder" ;
    }

}

void GeometryEngine::reinitPOIImageBuffers()
{
       //Delete   existing structures
    //Potential memory leak, TODO: sort out deletion of image buffers later
    selectedPOIImagesBuffer.clear();
    selectedPOIImagesTextures.clear();
    selectedPOIImagesVertices.clear();
    for(int i=0;i<selectedPOIs.size();i++){
        initPOIImageBuffer(selectedPOIs.at(i));
    }
    openglRenderer->updateGL();
}




void GeometryEngine::initRoadData()
{
    //qDebug()<<"Init Road Data";

    setUpRoadVertices();

    //qDebug()<<"After:"<<roadCount<<","<<roadIndexCount;
    //    glBindBuffer(GL_ARRAY_BUFFER, roadsBuffer[0]);
    //    glBufferData(GL_ARRAY_BUFFER, roadCount * sizeof(VertexData), roadVertices, GL_STATIC_DRAW);

    //    // Transfer index data to VBO 1
    //    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, roadsBuffer[1]);
    //    glBufferData(GL_ELEMENT_ARRAY_BUFFER,( roadIndexCount) * sizeof(GLuint), roadIndices, GL_STATIC_DRAW);

    glBindBuffer(GL_ARRAY_BUFFER, roadQuadsBuffer[0]);
    glBufferData(GL_ARRAY_BUFFER, roadNodeCount*4 * sizeof(VertexData), roadQuads, GL_STATIC_DRAW);

    // Transfer index data to VBO 1
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, roadQuadsBuffer[1]);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER,( roadIndexCount*2) * sizeof(GLuint), roadQuadIndices, GL_STATIC_DRAW);


}



void GeometryEngine::setGLWidget(OpenGLRenderer *widget)
{
    openglRenderer=widget;
}

//!Set up the vertices between which roads are need to be drawn
/*!
 * Get all the vertices for all the roads from the dbHandler roadVertices structure.
 * Then for each road, set up the vertices to be drawn as road.
 *e.g. Road  r1 has vertices n1, n2, road r2 has vertices n3,n4,n5, and so on.
 * Road vertices contains all such nodes as one list. We need to set up the indices for these
 *vertices for drawing. In the above example, indices corresponding to r1 will be 0 and 1.
 *Indices corresponding to r2 will be 2,3, 3,4, and 4,5. For the next road, the index will start
 *with 6.
 */
void GeometryEngine::setUpRoadVertices()
{

    std::map<std::string, Road>::iterator it;
    roadNodeCount=0;

    for (it=leCreusot->getRoads().begin();it!=leCreusot->getRoads().end();++it){
        roadNodeCount+= ((Road)(it->second)).Contour.size();
    }

    roadIndexCount = 2*(roadNodeCount - leCreusot->getRoads().size());
    //qDebug()<<"roadnode count"<<roadNodeCount;

    roadVertices=new VertexData[roadNodeCount];
    roadIndices=new GLuint[roadIndexCount];

    std::string roadId,prevRoadId="";

    int roadVertexIndex=0;
    int roadIndicesIndex=0;
    Z=roadZ;
    //Set up road vertices
    for(it=leCreusot->getRoads().begin();it!=leCreusot->getRoads().end();++it){
        Road road=(Road)(it->second);
        //qDebug()<<road.RoadType.c_str();
        roadId=road.ID;
        std::vector<Node>::iterator cit= road.Contour.begin();

        while(cit!=road.Contour.end()){
            Node node=*cit;
            GLfloat X=mapScale*Metrics::toMapCoordX(node.getLongitude())/openglRenderer->width()-mapScale/2;
            GLfloat Y=mapScale*Metrics::toMapCoordY(node.getLatitude())/openglRenderer->height()-mapScale/2;

            if(prevRoadId==roadId){
                roadIndices[roadIndicesIndex] =(roadVertexIndex-1);
                ++roadIndicesIndex;
                roadIndices[roadIndicesIndex]=(roadVertexIndex);
                ++roadIndicesIndex;
                //    std::cout<<"roadIndicesIndec : "<<roadIndicesIndex <<" ("<<roadIndices[roadIndicesIndex-2]<<","<<roadIndices[roadIndicesIndex-1]<<")"<<endl;

            }

            roadVertices[roadVertexIndex]=VertexData(QVector3D(X,Y,Z),QVector2D(0.0,0.0));
            roadVertices[roadVertexIndex]=VertexData(QVector3D(X,Y,Z),QVector2D(0.0,0.0));
            //Use the X coordinate of the texture to indicate oneway roads
            if(road.OneWay)
                roadVertices[roadVertexIndex].texCoord.setX(1);
            else
                roadVertices[roadVertexIndex].texCoord.setX(0);

            //Use the Y coordinate to indicate the type of the road
            //--1 primary ,widest , 2 - secondary,narrower than primary,
            //3- tertiary, narrower than secondary, 4-everything else, narrowest
            if(road.RoadType=="primary"){
                roadVertices[roadVertexIndex].texCoord.setY(1);
            }
            else if(road.RoadType=="secondary"){
                roadVertices[roadVertexIndex].texCoord.setY(2);
            }
            else if(road.RoadType=="tertiary"){
                roadVertices[roadVertexIndex].texCoord.setY(3);
            }
            else{
                roadVertices[roadVertexIndex].texCoord.setY(4);
            }

            prevRoadId=roadId;

            //Advance to next node in this road
            ++cit;
            //Advance the road Vertex index
            ++roadVertexIndex;

        }
    }
    //qDebug()<<"Road Index count here : "<<roadIndexCount;

    //Set up indices for the road segments


    //For every road segment, we draw a quad instead of a line.
    roadQuads=new VertexData[roadIndexCount*2];
    roadQuadIndices=new GLuint[roadIndexCount*2];
    int roadQuadIndex=0;
    GLfloat roadWidth=0;
    GLfloat X1=0.0f,Y1=0.0f,X2=0.0f,Y2=0.0f;
    Z=roadQuadZ;
    bool oneWay=false;
    for(int i=0;i<roadIndexCount;i++){
            roadWidth=0.004;
        //   while(roadIndexit!=leCreusot->roadIndices.end()){
        //        roadIndices[i]=*roadIndexit;
        //        qDebug()<<*roadIndexit;
        //        ++roadIndexit;
        // std::cout<<roadIndices[i]<<" ";
        if(i%2!=0){
            //Find the slope of the line roadVertices[i-1]---roadVertices[i]
            //Find two points on each side of roadVertices[i-1] and roadVertices[i] at a distance of
            //roadWidth/2 along the line perpendicular to the line joining those two vertices.
            //  qDebug()<<"Road : "<<i;
            X2=roadVertices[roadIndices[i]].position.x();
            Y2=roadVertices[roadIndices[i]].position.y();
            X1=roadVertices[roadIndices[i-1]].position.x();
            Y1=roadVertices[roadIndices[i-1]].position.y();
            GLfloat alpha=atan2(Y2-Y1,X2-X1);
            //4 corners of the rectangle around the line
            //Note on indices: For every two road indices, we generate a rectangle
            //i.e For nodes at indices 1, 3, 5, 7 etc., we need to generate 4 vertices
            //i=1,    We generate rectangle for roadvertices 0 and 1.
            //--- 4*(1-1)/2 + {0,1,2,3} = 0,1,2,3
            //i=3
            //-- 4*(3-1)/2+{0,1,2,3}=4,5,6,7
            //i=5
            //-- 4*(5-1)/2+{0,1,2,3}=8,9,10,11 etc.

            if(roadVertices[roadIndices[i-1]].texCoord.y()==1.0){
                roadWidth=0.008;
                //Primary road.
                roadQuads[2*(i-1)]=VertexData(QVector3D(X1 + roadWidth * sin(alpha),Y1 - roadWidth*cos(alpha),Z),QVector2D(0.4,0));
                roadQuads[2*(i-1)+1]=VertexData(QVector3D(X2 + roadWidth * sin(alpha),Y2 - roadWidth*cos(alpha),Z),QVector2D(0.6,0));
                roadQuads[2*(i-1)+2]=VertexData(QVector3D(X2 - roadWidth * sin(alpha),Y2 + roadWidth*cos(alpha),Z),QVector2D(0.6,1));
                roadQuads[2*(i-1)+3]=VertexData(QVector3D(X1 - roadWidth * sin(alpha),Y1 + roadWidth*cos(alpha),Z),QVector2D(0.4,1));

            }
            else if(roadVertices[roadIndices[i-1]].texCoord.y()==2.0){
                //Secondary road.
                roadWidth=0.006;
                roadQuads[2*(i-1)]=VertexData(QVector3D(X1 + roadWidth * sin(alpha),Y1 - roadWidth*cos(alpha),Z),QVector2D(0.6,0));
                roadQuads[2*(i-1)+1]=VertexData(QVector3D(X2 + roadWidth * sin(alpha),Y2 - roadWidth*cos(alpha),Z),QVector2D(0.8,0));
                roadQuads[2*(i-1)+2]=VertexData(QVector3D(X2 - roadWidth * sin(alpha),Y2 + roadWidth*cos(alpha),Z),QVector2D(0.8,1));
                roadQuads[2*(i-1)+3]=VertexData(QVector3D(X1 - roadWidth * sin(alpha),Y1 + roadWidth*cos(alpha),Z),QVector2D(0.6,1));

            }
            else if(roadVertices[roadIndices[i-1]].texCoord.y()==3.0){
                //Tertiary road.
                roadWidth=0.004;
                roadQuads[2*(i-1)]=VertexData(QVector3D(X1 + roadWidth * sin(alpha),Y1 - roadWidth*cos(alpha),Z),QVector2D(0.8,0));
                roadQuads[2*(i-1)+1]=VertexData(QVector3D(X2 + roadWidth * sin(alpha),Y2 - roadWidth*cos(alpha),Z),QVector2D(1,0));
                roadQuads[2*(i-1)+2]=VertexData(QVector3D(X2 - roadWidth * sin(alpha),Y2 + roadWidth*cos(alpha),Z),QVector2D(1,1));
                roadQuads[2*(i-1)+3]=VertexData(QVector3D(X1 - roadWidth * sin(alpha),Y1 + roadWidth*cos(alpha),Z),QVector2D(0.8,1));

            }
            else{
                //Other roads
                roadWidth=0.002;
                roadQuads[2*(i-1)]=VertexData(QVector3D(X1 + roadWidth * sin(alpha),Y1 - roadWidth*cos(alpha),Z),QVector2D(0,0));
                roadQuads[2*(i-1)+1]=VertexData(QVector3D(X2 + roadWidth * sin(alpha),Y2 - roadWidth*cos(alpha),Z),QVector2D(0.2,0));
                roadQuads[2*(i-1)+2]=VertexData(QVector3D(X2 - roadWidth * sin(alpha),Y2 + roadWidth*cos(alpha),Z),QVector2D(0.2,1));
                roadQuads[2*(i-1)+3]=VertexData(QVector3D(X1 - roadWidth * sin(alpha),Y1 + roadWidth*cos(alpha),Z),QVector2D(0,1));

            }

            //Check if the road is oneway
            if(roadVertices[roadIndices[i-1]].texCoord.x()==1.0){
                //Oneway
                roadQuads[2*(i-1)].setTexCoord(QVector2D(0.2,0));
                roadQuads[2*(i-1)+1].setTexCoord(QVector2D(0.4,0));
                roadQuads[2*(i-1)+2].setTexCoord(QVector2D(0.4,1));
                roadQuads[2*(i-1)+3].setTexCoord(QVector2D(0.2,1));

            }


            //     qDebug()<<X1<<","<<Y1<<" "<<X2<<","<<Y2;


            if(!oneWay){
            }
            else{
            }
            roadQuadIndices[2*(i-1)]=2*(i-1);

            roadQuadIndices[2*(i-1)+1]=2*(i-1)+1;

            roadQuadIndices[2*(i-1)+2]=2*(i-1)+2;

            roadQuadIndices[2*(i-1)+3]=2*(i-1)+3;

            //            roadQuads[4*(i-1)]=VertexData(QVector3D(X1 + roadWidth * sin(alpha),Y1 - roadWidth*cos(alpha),Z),QVector2D(0,0));
            //            roadQuadIndices[4*(i-1)]=4*(i-1);

            //            roadQuads[4*(i-1)+1]=VertexData(QVector3D(X2 + roadWidth * sin(alpha),Y2 - roadWidth*cos(alpha),Z),QVector2D(1,0));
            //            roadQuadIndices[4*(i-1)+1]=4*(i-1)+1;


            //            roadQuads[4*(i-1)+2]=VertexData(QVector3D(X2 - roadWidth * sin(alpha),Y2 + roadWidth*cos(alpha),Z),QVector2D(1,1));
            //            roadQuadIndices[4*(i-1)+2]=4*(i-1)+2;

            //            roadQuads[4*(i-1)+3]=VertexData(QVector3D(X1 - roadWidth * sin(alpha),Y1 + roadWidth*cos(alpha),Z),QVector2D(0,1));
            //            roadQuadIndices[4*(i-1)+3]=4*(i-1)+3;


        }

        //Advance the road quad index
        ++roadQuadIndex;
        //    ++i;
    }

    //Each segment is made up of 2 nodes.
    //For each road, if there are n nodes, the number of road segments would be n-1.
    //Thus for r roads, the total road segments would be N-r, where N = total nodes.
    //roadCount=leCreusot->roadVertices.size()-leCreusot->getRoads().size();

    // qDebug()<<"Road vertexindex:"<<roadVertexIndex;
    // qDebug()<<"Road segment count : "<<dbHandler.roadVertices.size()-leCreusot->Roads.size();

    //openglRenderer->initRoadData(roadVertices,roadIndexData,leCreusot->Roads.size(),roadIndexCount);

}


void GeometryEngine::setRay(QVector3D ray)
{

    GLfloat X=ray.x(), Y=ray.y(),Z=ray.z(),quadSize=0.004f;
    Z=roadZ;
    quad[0]=VertexData(QVector3D(X-quadSize*1.5, Y-quadSize,  Z), QVector2D(0, 0));  // v0
    quad[1]=VertexData(QVector3D(X- quadSize*1.5, Y+quadSize,  Z), QVector2D(0,1/*0.33, 0.0*/)); // v1
    quad[2]=VertexData(QVector3D(X+quadSize*1.5,  Y+quadSize,  Z), QVector2D(1,1/*0.0, 0.5*/));  // v2
    quad[3]=VertexData(QVector3D( X+quadSize*1.5,  Y-quadSize,  Z), QVector2D(1.0,0/*0.33, 0.5*/)); // v3

    this->ray[0]=VertexData(ray,QVector2D(0,0));
    this->ray[1]=VertexData(QVector3D(ray.x(),ray.y(),mapZ),QVector2D(0,1));
    glBindBuffer(GL_ARRAY_BUFFER, selectedPointBuffer);
    glBufferData(GL_ARRAY_BUFFER, 4 * sizeof(VertexData), quad, GL_STATIC_DRAW);
}

GLfloat GeometryEngine::getMapZ()
{
    return mapZ;
}

GLfloat GeometryEngine::getMapSize()
{
    return mapSize;
}

void GeometryEngine::POIAdded(PointOfInterest poi)
{
    //qDebug()<<poi.getName();

    for(int i=0;i<leCreusot->getPOIs().size();i++){
        if(poi.getId()==leCreusot->getPOIs()[i].getId()){
            //qDebug()<<"Found poi in list";
            //Also generate the buffer to display image for this poi
            selectedPOIs.push_back(poi);
            initPOIImageBuffer(poi);
            GLfloat X=mapScale*Metrics::toMapCoordX(poi.getLongitude())/openglRenderer->width()-mapScale/2;
            GLfloat Y=mapScale*Metrics::toMapCoordY(poi.getLatitude())/openglRenderer->height()-mapScale/2;
//            qDebug()<<poi.getLongitude()<<", X="<<X;
//            qDebug()<<poi.getLatitude()<<",Y="<<Y;

//            //            poiVertices[24*i+4]=VertexData(QVector3D( X+cubeWidth, Y-cubeWidth, Z+ cubeHeight), QVector2D( 0.0, 0.5)); // v4
//            //            poiVertices[24*i+5]=VertexData(QVector3D( X+cubeWidth*0.5, Y-cubeWidth*0.5, Z-cubeHeight), QVector2D(0.33, 0.5)); // v5
//            //            poiVertices[24*i+6]=VertexData(QVector3D( X+cubeWidth,  Y+cubeWidth,Z+  cubeHeight), QVector2D(0.0, 1.0));  // v6
//            //            poiVertices[24*i+7]=VertexData(QVector3D( X+cubeWidth*0.5,  Y+cubeWidth*0.5, Z-cubeHeight), QVector2D(0.33, 1.0)); // v7

//            poiVertices[24*i].texCoord=QVector2D(0.0,0.5);
//            poiVertices[24*i+1].texCoord=QVector2D(0.33,0.5);
//            poiVertices[24*i+2].texCoord=QVector2D(0.0,1.0);
//            poiVertices[24*i+3].texCoord=QVector2D(0.33,1.0);

//            glBindBuffer(GL_ARRAY_BUFFER, poiBuffer[0]);
//            glBufferData(GL_ARRAY_BUFFER, poiCount *24* sizeof(VertexData), poiVertices, GL_STATIC_DRAW);

//            // Transfer index data to VBO 1
//            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, poiBuffer[1]);
//            glBufferData(GL_ELEMENT_ARRAY_BUFFER,( poiCount*36-2) * sizeof(GLuint), poiIndices, GL_STATIC_DRAW);
            //openglRenderer->translateTo(-X,-Y,-1.5);
            break;
        }
    }
    //qDebug()<<"Initing selected poi";
    initSelectedPOIData();

}

void GeometryEngine::POIRemoved(PointOfInterest poi)
{
    //qDebug()<<"Remove Request for poi received : "<<poi.getId()<<" : "<<poi.getName();
    int removePOIIndex=-1;
    for(int i=0;i<selectedPOIs.size();i++){
        if(poi.getId()==selectedPOIs[i].getId()){

            GLfloat X=mapScale*Metrics::toMapCoordX(poi.getLongitude())/openglRenderer->width()-mapScale/2;
            GLfloat Y=mapScale*Metrics::toMapCoordY(poi.getLatitude())/openglRenderer->height()-mapScale/2;
         //   qDebug()<<"Removing poi :"<<poi.getId()<<":"<<poi.getName();
            //qDebug()<<poi.getLongitude()<<", X="<<X;
            //qDebug()<<poi.getLatitude()<<",Y="<<Y;

            //            poiVertices[24*i]=VertexData(QVector3D(X-cubeWidth, Y-cubeWidth, Z+ cubeHeight), QVector2D(0.0, 0.0));  // v0
            //            poiVertices[24*i+1]=VertexData(QVector3D(X+ cubeWidth, Y-cubeWidth,Z+  cubeHeight), QVector2D(0.33, 0.0)); // v1
            //            poiVertices[24*i+2]=VertexData(QVector3D(X-cubeWidth,  Y+cubeWidth, Z+ cubeHeight), QVector2D(0.0, 0.5));  // v2
            //            poiVertices[24*i+3]=VertexData(QVector3D(X+ cubeWidth, Y+ cubeWidth, Z+ cubeHeight), QVector2D(0.33, 0.5)); // v3


            //This code is to change the heads of the pois. We dont need this as we are displaying pins
            //            poiVertices[24*i].texCoord=QVector2D(0.0,0.0);
//            poiVertices[24*i+1].texCoord=QVector2D(0.33,0.0);
//            poiVertices[24*i+2].texCoord=QVector2D(0.0,0.5);
//            poiVertices[24*i+3].texCoord=QVector2D(0.33,0.5);

//            glBindBuffer(GL_ARRAY_BUFFER, poiBuffer[0]);
//            glBufferData(GL_ARRAY_BUFFER, poiCount *24* sizeof(VertexData), poiVertices, GL_STATIC_DRAW);

//            // Transfer index data to VBO 1
//            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, poiBuffer[1]);
//            glBufferData(GL_ELEMENT_ARRAY_BUFFER,( poiCount*36-2) * sizeof(GLuint), poiIndices, GL_STATIC_DRAW);

            //openglRenderer->translate(QVector3D(-X,-Y,-1.5));

            //selectedPOIs.erase(selectedPOIs.begin()+i);
            removePOIIndex=i;

            break;
        }
    }
    if(removePOIIndex>-1){
        selectedPOIs.erase(selectedPOIs.begin()+removePOIIndex);
    }
    initSelectedPOIData(true);
    reinitPOIImageBuffers();
}

void GeometryEngine::POIsRemoved(QString currentCat)
{

    std::string cat=currentCat.toStdString();
    std::transform(cat.begin(), cat.end(), cat.begin(), ::tolower);
    selectedPoiCount=0;
    vector<PointOfInterest> poisToBeRemoved=MainWindow::instance->getPathFinderengine().getPoiMap()[cat];

    for(int i=0;i<poisToBeRemoved.size();i++){
        for(int j=0;j<selectedPOIs.size();j++){
            if(selectedPOIs[j].getId()==poisToBeRemoved[i].getId()){
                selectedPOIs.erase(selectedPOIs.begin()+j);
            }
        }
    }



    selectedPoiCount=selectedPOIs.size();
    initSelectedPOIData(true);
    reinitPOIImageBuffers();
}

void GeometryEngine::clearSelectedPOIs()
{
    if(selectedPoiCount>0){
        selectedPoiCount=0;
        selectedPOIs.clear();
        glDeleteBuffers(1, &selectedPOIBuffer);
        for(int i=0;i<selectedPOIImagesBuffer.size();i++){
            glDeleteBuffers(1,&(selectedPOIImagesBuffer.at(i)));
        }
        selectedPOIImagesBuffer.clear();
        selectedPOIImagesTextures.clear();
        selectedPOIImagesVertices.clear();
    }

    glGenBuffers(1,&selectedPOIBuffer);

    openglRenderer->updateGL();
}


void GeometryEngine::drawRoute(PathResult pathResult)
{
    //qDebug()<<"Drawing path";
    initRouteData(pathResult);
    glBindBuffer(GL_ARRAY_BUFFER, routeBuffer[0]);
    glBufferData(GL_ARRAY_BUFFER, routeVerticesCount*4 * sizeof(VertexData), routeQuads, GL_STATIC_DRAW);

    // Transfer index data to VBO 1
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, routeBuffer[1]);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER,( routeIndexCount*2) * sizeof(GLuint), routeQuadIndices, GL_STATIC_DRAW);

    drawPath=true;
}

void GeometryEngine::drawPOIsInCategory(QString currentCat)
{
    qDebug()<<"Drawing pois in category "<<currentCat;

    std::string cat=currentCat.toStdString();
    std::transform(cat.begin(), cat.end(), cat.begin(), ::tolower);

    selectedPOIs= MainWindow::instance->getPathFinderengine().getPoiMap()[cat];

    selectedPoiCount=selectedPOIs.size();
    initSelectedPOIData();
    reinitPOIImageBuffers();
//    glBindBuffer(GL_ARRAY_BUFFER, routeBuffer[0]);
//    glBufferData(GL_ARRAY_BUFFER, routeVerticesCount*4 * sizeof(VertexData), routeQuads, GL_STATIC_DRAW);

//    // Transfer index data to VBO 1
//    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, routeBuffer[1]);
//    glBufferData(GL_ELEMENT_ARRAY_BUFFER,( routeIndexCount*2) * sizeof(GLuint), routeQuadIndices, GL_STATIC_DRAW);

    //    drawPath=true;
}

void GeometryEngine::drawPOIsInDistance(vector<PointOfInterest> pois)
{
    selectedPOIs=pois;

    selectedPoiCount=selectedPOIs.size();
    initSelectedPOIData();
    reinitPOIImageBuffers();
}

void GeometryEngine::clearRoute()
{
    clearRouteData();
    openglRenderer->updateGL();
}

void GeometryEngine::clicked(QVector2D pos)
{
    double lon=Metrics::toLongitude((pos.x()+mapScale/2)*openglRenderer->width()/mapScale);
    double lat=Metrics::toLatitude((pos.y()+mapScale/2)*openglRenderer->height()/mapScale);

    //emit clickReturn(QVector2D(lat,lon));


    GLfloat X=pos.x(), Y=pos.y(),Z=1.004,quadSize=0.104f;
    Z=roadZ+0.01f;
    quad[0]=VertexData(QVector3D(X-quadSize*1.5, Y-quadSize,  Z), QVector2D(0, 0));  // v0
    quad[1]=VertexData(QVector3D(X- quadSize*1.5, Y+quadSize,  Z), QVector2D(0,1/*0.33, 0.0*/)); // v1
    quad[2]=VertexData(QVector3D(X+quadSize*1.5,  Y+quadSize,  Z), QVector2D(1,1/*0.0, 0.5*/));  // v2
    quad[3]=VertexData(QVector3D( X+quadSize*1.5,  Y-quadSize,  Z), QVector2D(1.0,0/*0.33, 0.5*/)); // v3
    //qDebug()<<quad[0].toString().c_str();
    //qDebug()<<quad[1].toString().c_str();
    //this->ray[0]=VertexData(ray,QVector2D(0,0));
    //this->ray[1]=VertexData(QVector3D(ray.x(),ray.y(),mapZ),QVector2D(0,1));
    glBindBuffer(GL_ARRAY_BUFFER, selectedPointBuffer);
    glBufferData(GL_ARRAY_BUFFER, 4 * sizeof(VertexData), quad, GL_STATIC_DRAW);
    //qDebug()<<X<<","<<Y;
    //openglRenderer->translateTo(-X,-Y,-5);
}
