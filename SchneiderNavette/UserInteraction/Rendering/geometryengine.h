
#ifndef GEOMETRYENGINE_H
#define GEOMETRYENGINE_H

#include <QGLFunctions>
#include <QGLShaderProgram>
#include <QOpenGLVertexArrayObject>
#include <QOpenGLBuffer>

#include "Processing/metrics.h"
#include "vertexdata.h"
#include "Model/rawdata.h"
#include "Processing/pathfinderengine.h"
class OpenGLRenderer;
class DatabaseHandler;
class GeometryEngine : public QObject,protected QGLFunctions
{
Q_OBJECT
public:
    GeometryEngine();
     ~GeometryEngine();

    void init(QGLShaderProgram* program, RawData *rawMapData);
    void drawStuff();
    void drawMap();
    void drawVertices();
    void drawRoads();
    void drawPOIs();
    void drawSelectedPoint();
    void drawSelectedPOIs();
    void drawRoute();
    void drawRouteStartEnd();
    void drawSelectedPOIImages();

    void initCube();
    void initVertexData();
    void initRoadData();
    void initPOIData();

    void initSelectedPOIData(bool poiRemoved);
    void initRouteData(PathResult pathResult);
    void clearRouteData();
  // void initRouteData(Node startNode);
    void initStartEndRouteQuads();
    void initStartEndRouteData(GLfloat startX, GLfloat startY, GLfloat endX, GLfloat endY);
    void initPOIImageBuffer(PointOfInterest poi);
    void reinitPOIImageBuffers();

    GLuint textures[7];

    GLuint vboIds[2];
    void setGLWidget(OpenGLRenderer* widget);
    void setUpRoadVertices();
    void setRay(QVector3D ray);
    GLfloat getMapZ();
    GLfloat getMapSize();
    GLfloat getMapHeight() const;
    void setMapHeight(const GLfloat &value);

    GLfloat getMapWidth() const;
    void setMapWidth(const GLfloat &value);
signals:
    void clickReturn(QVector2D);
public slots:
    void POIAdded(PointOfInterest);
    void POIRemoved(PointOfInterest);
    void POIsRemoved(QString);
    void clearSelectedPOIs();
    void drawRoute(PathResult pathResult);
    void drawPOIsInCategory(QString currentCat);
    void drawPOIsInDistance(vector<PointOfInterest>);
    void clearRoute();
    void clicked(QVector2D);
    void reinitPOIData();
private:
    void initMapBoundary();
    GLuint cubeBuffer[2],verticesBuffer[2],mapBuffer[3],roadsBuffer[2],roadQuadsBuffer[2],routeBuffer[2];
    GLuint selectedPointBuffer;
    GLuint startEndRouteBuffer;
    GLuint poiBuffer[2];
    GLuint selectedPOIBuffer;
    vector<int> erasedPoiIndices;
    vector<GLuint> selectedPOIImagesBuffer;
    vector<VertexData*> selectedPOIImagesVertices;
    vector<GLuint> selectedPOIImagesTextures;
    VertexData ray[2];
    RawData* leCreusot;
    //QOpenGLBuffer* vboIds[6];
  //  QOpenGLVertexArrayObject* vaos[4];

    GLfloat mapScale;
    QGLShaderProgram* program;
    QGLFunctions glFunctions;
    OpenGLRenderer *openglRenderer;

    VertexData* poiVertices;
    VertexData* vertices;
    VertexData* roadVertices;
    VertexData* roadQuads;
    VertexData* selectedPoiVertices;
    VertexData* routeVertices;
    VertexData* routeQuads;
    VertexData* startEndRouteQuads;

    GLuint* poiIndices;
    GLuint* indices;
    GLuint* mapIndices;
    GLuint* roadIndices;
    GLuint* roadQuadIndices;
    GLuint* routeIndices;
    GLuint* routeQuadIndices;

    int nodeCount;
    int roadNodeCount;
    int roadIndexCount;
    int poiCount;
    int selectedPoiCount;
    int routeVerticesCount;
    int routeIndexCount;
    GLfloat mapSize;
    VertexData boundary[4];
    GLfloat roadQuadZ,roadZ,nodeZ,poiZ,Z,mapZ;
    VertexData quad[4];
    bool inited;
    std::vector<PointOfInterest> selectedPOIs;
    quintptr offset;
    bool drawPath;
    bool poisUpdating;
    GLfloat mapWidth,mapHeight;
};

#endif // GEOMETRYENGINE_H
