#ifdef GL_ES
// Set default precision to medium
precision mediump int;
precision mediump float;
#endif

uniform mat4 mvp_matrix;

attribute vec4 a_position;
attribute vec2 a_texcoord;
attribute vec4 objectColor;

uniform bool useColor;
varying vec2 v_texcoord;
varying vec4 vobjectColor;

void main()
{
    // Calculate vertex position in screen space
    gl_Position = mvp_matrix * a_position;
    //gl_Position = gl_ModelViewMatrix * gl_Vertex;

    gl_FrontColor=gl_Color;

    // Pass texture coordinate to fragment shader
    // Value will be automatically interpolated to fragments inside polygon faces
    v_texcoord = a_texcoord;
    vobjectColor=objectColor;

}

