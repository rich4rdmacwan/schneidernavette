//#include <GL/glew.h>

#include <QResizeEvent>
#include <QDebug>
#include "openglrenderer.h"
#include "QGLShaderProgram"
#include <QApplication>
#include <cmath>
#include "Processing/metrics.h"
#include "GL/glext.h"
/**
 * @brief OpenGLRenderer::OpenGLRenderer
 * Adapted from Qt's Cube example
 * @param parent
 */

RawData *rawMapData;

OpenGLRenderer::OpenGLRenderer( RawData *mapData,QWidget *parent) :
    QGLWidget(parent),
    rotationAngle(0),worldX(-3.6),worldY(0.6),worldZ(-5),
    xStep(0.4f),yStep(0.4f),zStep(0.4f),pitchAngle(0.0f),yawAngle(0.0f),prevWorldZ(0)
{
    //Through trial and error
    worldX=-3.6;//-1.15;
    worldY=0.6;//0.025;

    mapMoveSpeed=40;
    rawMapData = mapData;
    geometries.setGLWidget(this);
    minWorldZ=-15;
    maxWorldZ=-1.1;
    routeWidthPx=5;


}

OpenGLRenderer::~OpenGLRenderer()
{
    deleteTexture(cubeTexture);
    deleteTexture(terrainTexture);
    deleteTexture(roadTexture);
    deleteTexture(selectedPoiTexture);
    deleteTexture(poiTexture);
}



void OpenGLRenderer::initializeGL()
{

  //  context()->makeCurrent();
    initializeGLFunctions();
    const GLubyte* shaderVersion=glGetString(GL_SHADING_LANGUAGE_VERSION);
    std::cout<<"OpenGL Shading Language Version : "<<shaderVersion<<std::endl;
    qglClearColor(QColor(200,220,248));

    initShaders();
    initTextures();


    // Enable depth buffer
    glEnable(GL_DEPTH_TEST);

    // Enable back face culling
    //glEnable(GL_CULL_FACE);


    geometries.init(&program, rawMapData);

    // Use QBasicTimer because its faster than QTimer
    //timer.start(12, this);

}


void OpenGLRenderer::initShaders()
{
    // Override system locale until shaders are compiled
    setlocale(LC_NUMERIC, "C");

    // Compile vertex shader
    if (!program.addShaderFromSourceFile(QGLShader::Vertex, ":/UserInteraction/Rendering/Basic.vsh"))
        close();

    // Compile fragment shader
    if (!program.addShaderFromSourceFile(QGLShader::Fragment, ":/UserInteraction/Rendering/Basic.fsh"))
        close();

    // Link shader pipeline
    if (!program.link())
        close();

    // Bind shader pipeline for use
    if (!program.bind())
        close();

    // Restore system locale
    setlocale(LC_ALL, "");
}
void OpenGLRenderer::initTextures()
{

   // glActiveTexture(GL_TEXTURE0);
    // Load cube.png image
    //glEnable(GL_TEXTURE_2D);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    terrainTexture = bindTexture(QImage(":/terrain0.jpg"));

    geometries.textures[0]=terrainTexture;

    cubeTexture = bindTexture(QImage(":/Images/cube.png"));
    geometries.textures[1]=cubeTexture;


    lineTexture=bindTexture(QImage(":/Images/buildingtop.jpg"));
    geometries.textures[2]=lineTexture;

    roadTexture=bindTexture(QImage(":/Images/coloredroad.jpg"));
    geometries.textures[3]=roadTexture;

    poiTexture=bindTexture(QImage(":/Images/bluepin.png"));
    geometries.textures[4]=poiTexture;

    selectedPoiTexture=bindTexture(QImage(":/Images/redpin.png"));
    geometries.textures[5]=selectedPoiTexture;

    startEndRouteTexture=bindTexture(QImage(":/Images/startend.png"));
    geometries.textures[6]=startEndRouteTexture;
//    // Set nearest filtering mode for texture minification
//    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

//    // Set bilinear filtering mode for texture magnification
//    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

//    // Wrap texture coordinates by repeating
//    // f.ex. texture coordinate (1.1, 1.2) is same as (0.1, 0.2)
//    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
//    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
}


GLfloat OpenGLRenderer::getCameraZ()
{
    return worldZ;
}



void OpenGLRenderer::defaults()
{
       worldZ=-5.0;
       rotationAngle=0;
      // worldX=0;worldY=0;
       //qDebug()<<"Defau;ts";
       pitchAngle=0;
       updateGL();
}

void OpenGLRenderer::resizeGL(int w, int h)
{

    // Set OpenGL viewport to cover whole widget
    glViewport(0, 0, w, h);

    // Calculate aspect ratio
   aspect = qreal(w) / qreal(h ? h : 1);
    // Set near plane to 3.0, far plane to 7.0, field of view 45 degrees
    zNear =0.01; zFar = 1000.0; fov = 60.0;

    // Reset projection
    projection.setToIdentity();

    //projection.ortho(0,w,h,0,zNear,zFar);
    // Set perspective projection
    projection.perspective(fov, aspect, zNear, zFar);

    Metrics::updateVariables(this->width(),this->height());

 //   qDebug()<<"N,F ="<<projection.column(0).x()<<","<<projection.column(0).w();

}


void OpenGLRenderer::paintGL()
{
    // Clear color and depth buffer
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);



    // Calculate model view transformation
    QMatrix4x4 matrix;

    matrix.translate(worldX, worldY, worldZ);
    //matrix.lookAt(QVector3D(worldX,worldY,worldZ-2),QVector3D(worldX,worldY,worldZ),QVector3D(0,0,-1));
    //matrix.translate(0, 0, -5);
    matrix.rotate(rotation);

    //matrix.scale(zoom);
    modelViewMatrix=matrix;
    // Set modelview-projection matrix
    program.setUniformValue("mvp_matrix", projection * matrix);

    // Use texture unit 0 which contains cube.png
    program.setUniformValue("texture", 0);
    // Draw cube geometry
    geometries.drawStuff();

}

void OpenGLRenderer::zoomStep()
{

    if(worldZ<-4){
        zStep=0.2;
        xStep=0.2;
        yStep=0.2;
    }
    if(worldZ>-4 && worldZ <-3){
        zStep=0.1;
        xStep=0.1;
        yStep=0.1;
    }
    if(worldZ>-3 && worldZ <-2){
        zStep=0.05;
        yStep=0.05;
        xStep=0.05;
    }
    if(worldZ>-2 && worldZ < -1.5){
        zStep=0.02;
        xStep=0.02;
        yStep=0.02;
    }
    if(worldZ>-1.5 && worldZ < -1.1){
        zStep=0.01;
        xStep=0.01;
        yStep=0.01;
    }
    if(worldZ>-1 && worldZ < -0.75){
        zStep=0.005;
        xStep=0.005;
        yStep=0.005;
    }
    if(worldZ>-0.75 && worldZ < -0.6){
        zStep=0.001;
        xStep=0.001;
        yStep=0.001;
    }

    //qDebug()<<worldZ;
    //Update route width


}



void OpenGLRenderer::keyPressEvent(QKeyEvent *event)
{

    int key=event->key();
    bool shift=event->modifiers() & Qt::ShiftModifier;
    QVector3D n ;

    switch(key) // process key presses
    {
    case Qt::Key_Left:
        rotationAngle=1;
        if(pitchAngle==0)
        {
        n= QVector3D(0, 0,1);

        }
        else{
        n= QVector3D(0, sin(Metrics::degrees_to_radians(-pitchAngle)), cos(Metrics::degrees_to_radians(-pitchAngle)));

//        rotation = QQuaternion::fromAxisAndAngle(n, -pitchAngle);

//        QVector3D zRot=QVector3D(rotation.x(),rotation.y(),rotation.y());
//        rotationAxis = (rotationAxis +2* zRot).normalized();
//        rotation = QQuaternion::fromAxisAndAngle(rotationAxis, rotationAngle) * rotation;

        }

        //qDebug()<<rotation.x()<<","<<rotation.y()<<","<<rotation.z();
        rotationAxis = (rotationAxis +2* n).normalized();
        rotationAngle=1;

        rotation = QQuaternion::fromAxisAndAngle(rotationAxis, rotationAngle) * rotation;

        break;
    case Qt::Key_Right:
        if(pitchAngle==0)
        {
        n= QVector3D(0, 0, 1);

        }
        else{
        n= QVector3D(0, sin(Metrics::degrees_to_radians(-pitchAngle)), cos(Metrics::degrees_to_radians(-pitchAngle)));

        //rotation = QQuaternion::fromAxisAndAngle(rotationAxis, cos(Metrics::degrees_to_radians(-pitchAngle))) ;

//        qDebug()<<rotation.x()<<","<<rotation.y()<<","<<rotation.z();
        }
        rotationAxis = (rotationAxis -2* n).normalized();
        rotationAngle=1;
        rotation = QQuaternion::fromAxisAndAngle(rotationAxis, rotationAngle) * rotation;

        break;
    case Qt::Key_Up:
        n= QVector3D(1, 0.0, 0.0);
        rotationAxis = (rotationAxis - n).normalized();
        rotationAngle=1;

        if(pitchAngle>-60){
        pitchAngle-=1;
        rotation = QQuaternion::fromAxisAndAngle(rotationAxis, rotationAngle) * rotation;
        }
        break;
    case Qt::Key_Down:
        n= QVector3D(1, 0.0,0.0);
        rotationAxis = (rotationAxis + n).normalized();
        rotationAngle=1;
        //Limit perspective view
        if(pitchAngle<0){
        pitchAngle+=1;
        rotation = QQuaternion::fromAxisAndAngle(rotationAxis, rotationAngle) * rotation;
        }
        break;
    case Qt::Key_Z : // zoom in

        if(shift){
            //zoom*=zoomStep;
            if(worldZ>minWorldZ)
                worldZ-=zStep;

            if(worldZ > -0.6){
                zStep=0.0005;
            }

        }
        else{
              //  zoom/=zoomStep;
            if(worldZ<maxWorldZ)
            worldZ+=zStep;

            if(worldZ<-4){
                zStep=0.1;
            }

        }
        zoomStep();

        break;
    case Qt::Key_A:

        if(worldX>-7.175)
        worldX-=xStep;
        rotation = QQuaternion::fromAxisAndAngle(rotationAxis, rotationAngle) * rotation;
        break;
    case Qt::Key_D:
        if(worldX<-0.97)
        worldX+=xStep;
        rotation = QQuaternion::fromAxisAndAngle(rotationAxis, rotationAngle) * rotation;
        break;
    case Qt::Key_W:
            worldY-=yStep*cos(Metrics::degrees_to_radians(abs(pitchAngle)));
        if(pitchAngle!=0)
            worldZ+=zStep*sin(Metrics::degrees_to_radians(abs(pitchAngle)));
        rotation = QQuaternion::fromAxisAndAngle(rotationAxis, rotationAngle) * rotation;
        break;
    case Qt::Key_S:
            worldY+=yStep*cos(Metrics::degrees_to_radians(abs(pitchAngle)));;
            if(pitchAngle!=0)
            worldZ-=zStep*sin(Metrics::degrees_to_radians(abs(pitchAngle)));;
            rotation = QQuaternion::fromAxisAndAngle(rotationAxis, rotationAngle) * rotation;
        break;
   case Qt::Key_R:
            defaults();
            break;
    }

    //qDebug()<<"Pitch angle:"<<pitchAngle;
    updateGL();
}

void OpenGLRenderer::keyReleaseEvent(QKeyEvent *)
{
    rotationAngle=0;
}





QVector2D lastMouseMovePosition;

void OpenGLRenderer::mousePressEvent(QMouseEvent *e)
{
    // Save mouse press position
    mousePressPosition = QVector2D(e->localPos());
    mouseMovePosition=mousePressPosition;
    rotationAngle=0;

    lastMouseMovePosition = mousePressPosition;
}

void OpenGLRenderer::mouseReleaseEvent(QMouseEvent *e)
{
    // Mouse release position - mouse press position
    //QVector2D diff = QVector2D(e->localPos()) - mousePressPosition;

    // Rotation axis is perpendicular to the mouse position difference
    // vector
  //  QVector3D n = QVector3D(diff.y(), diff.x(), 0.0).normalized();

    // Accelerate angular speed relative to the length of the mouse sweep
   // qreal acc = diff.length() / 100.0;

    // Calculate new rotation axis as weighted sum
   // rotationAxis = (rotationAxis * angularSpeed + n * acc).normalized();

    // Increase angular speed
    //angularSpeed += acc;
    QApplication::setOverrideCursor(Qt::ArrowCursor);
    rotationAngle=0;
    setFocus();

    QVector2D mousePosition = QVector2D(e->localPos());
    if (mousePosition == lastMouseMovePosition){
        GLfloat viewPortTopLeftX=worldX-worldZ/minWorldZ* geometries.getMapWidth();
        GLfloat viewPortTopLeftY=worldY-worldZ/minWorldZ* geometries.getMapHeight();

        GLfloat viewPortX=(mousePosition.x()/this->width())*(worldZ/minWorldZ)*geometries.getMapWidth();
        GLfloat viewPortY=(mousePosition.y()/this->height())*(worldZ/minWorldZ)*geometries.getMapHeight();

        GLfloat mapX=viewPortTopLeftX+viewPortX;
        GLfloat mapY=viewPortTopLeftY+viewPortY;
        //qDebug()<<"MapX ="<<mapX<<", MapY="<<mapY;



        //emit clicked(QVector2D(mapX,mapY));
        //qDebug()<<zoom;
        //QVector3D glpos=getGLCoordinates(mousePosition.x(),mousePosition.y());
       // qDebug()<<glpos.x()<<","<<glpos.y()<<glpos.z();
        //geometries.setRay(QVector3D(glpos.x()-worldX/worldZ,glpos.y()-worldY/worldZ,glpos.z()-worldZ/worldZ));
        //QVector2D latlong=QVector2D(Metrics::toLatitude(e->y())*this->height(),Metrics::toLongitude(e->x())*this->width())/15;
        //qDebug()<<latlong.x()<<","<<latlong.y();
        //emit clicked(e);

    }
}


void OpenGLRenderer::mouseMoveEvent(QMouseEvent *e)
{

    // Mouse release position - mouse press position
    QVector2D diff = QVector2D(e->localPos()) - mouseMovePosition;

    mouseMovePosition = QVector2D(e->localPos());


    //angularSpeed = acc;
    //  qDebug()<<acc;
    //rotation = QQuaternion::fromAxisAndAngle(rotationAxis, angularSpeed) * rotation;
    worldX+=diff.x()/40;
    worldY-=diff.y()/40;
    QApplication::setOverrideCursor(Qt::ClosedHandCursor);

    updateGL();


    //lastMouseMovePosition = mouseMovePosition;
}

void OpenGLRenderer::timerEvent(QTimerEvent *)
{
    // Decrease angular speed (friction)

//    rotationAngle *= 0.99;

//    // Stop rotation when speed goes below threshold
//    if (rotationAngle < 0.01) {
//        rotationAngle = 0.0;
//        timer.stop();
//    } else {
//        // Update rotation

//        rotation = QQuaternion::fromAxisAndAngle(rotationAxis, rotationAngle) * rotation;
//        // Update scene
//        updateGL();
//    }
    if((fabs(worldX-targetWorldX)>0.1))
        worldX+=offsetWorldX/mapMoveSpeed;
    if((fabs(worldY-targetWorldY)>0.1))
        worldY+=offsetWorldY/mapMoveSpeed;
    if((fabs(worldZ-targetWorldZ)>0.1))
        worldZ+=offsetWorldZ/mapMoveSpeed;

    if((fabs(worldX-targetWorldX)<0.1) && (fabs(worldY-targetWorldY)<0.1) && (fabs(worldX-targetWorldX)<0.1)) {
        //qDebug()<<"Stop";
        timer.stop();
        zoomStep();
    }
//    qDebug()<<worldX<<"--->"<<targetWorldX<<" : "<<fabs(worldX-targetWorldX);
//    qDebug()<<worldY<<"--->"<<targetWorldY<<" : "<<fabs(worldY-targetWorldY);
//    qDebug()<<worldZ<<"--->"<<targetWorldZ<<" : "<<fabs(worldZ-targetWorldZ);
//    qDebug()<<" ";
    updateGL();
}

void OpenGLRenderer::wheelEvent(QWheelEvent *wevent)
{

    if(wevent->angleDelta().y()>0){
        if(worldZ<maxWorldZ)
        worldZ+=zStep;

        if(worldZ<-4){
            zStep=0.2;
        }
    }
    else{
        if(worldZ>minWorldZ)
            worldZ-=zStep;

        if(worldZ > -0.6){
            zStep=0.0005;
        }

    }

zoomStep();

    setFocus();
    updateGL();

}

QVector3D OpenGLRenderer::getGLCoordinates(int x, int y)
{
    GLint viewport[4];
    GLdouble modelview[16];
    GLdouble projection[16];
    GLfloat winX, winY, winZ;
    GLdouble posX, posY, posZ;

    glGetDoublev( GL_MODELVIEW_MATRIX, modelview );
    glGetDoublev( GL_PROJECTION_MATRIX, projection );
    glGetIntegerv( GL_VIEWPORT, viewport );

    winX = (float)x;
    winY = (float)viewport[3] - (float)y;
    glReadPixels( x, int(winY), 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &winZ );

    //gluUnProject( winX, winY, winZ, modelview, projection, viewport, &posX, &posY, &posZ);

//        float normalized_x, normalized_y;
//        QMatrix4x4 Unview;
//        QVector4D near_point, camera_pos, ray_dir, ray_origin;

//        float w=(worldZ-zNear) * geometries.getMapSize()/(geometries.getMapZ()-zNear);
//        normalized_x = 2.0 * x/this->width() - 1.0;
//        normalized_y = 1.0 - 2.0 * y/this->height();
//       // qDebug()<<"Widt: "<<w;
//        Unview = (projection*modelViewMatrix).inverted();

//        near_point = Unview * QVector4D(normalized_x, normalized_y, 0, 1);
//        camera_pos = modelViewMatrix.inverted().column(3);
//        ray_dir = (near_point - camera_pos).normalized();

        return QVector3D(posX/worldZ,posY/worldZ,0);
        //return QVector3D(ray_dir.x(),ray_dir.y(),ray_dir.z());
}



void OpenGLRenderer::translateTo(GLfloat X, GLfloat Y, GLfloat Z)
{

    //Calculate how much we need to translate. Then translate slowly using the timerEvent
    targetWorldX=X;
    targetWorldY=Y;
    if(Z>maxWorldZ)
    {
        Z=maxWorldZ-1.5;
    }
    targetWorldZ=Z;
    qDebug()<<"Translating to "<<Z<<"maxWorldZ = "<<maxWorldZ;
    offsetWorldX=X-worldX;
    offsetWorldY=Y-worldY;
    offsetWorldZ=Z-worldZ;
    if(pitchAngle!=0){

        targetWorldZ=-2.3;
//        QVector3D n= QVector3D(-1, 0.0,0.0);
//        rotationAxis = (rotationAxis - n).normalized();
//        rotationAngle=-pitchAngle;
//        rotation = QQuaternion::fromAxisAndAngle(rotationAxis, rotationAngle) * rotation;
        //targetWorldX+=targetWorldZ*sin(Metrics::degrees_to_radians(pitchAngle));
        //targetWorldY-=targetWorldZ*sin(Metrics::degrees_to_radians(pitchAngle));
        pitchAngle=0;
    }

    //defaults();
    timer.start(30,this);

}

GLfloat OpenGLRenderer::getMinWorldZ()
{
    return minWorldZ;

}

GLfloat OpenGLRenderer::getMaxWorldZ()
{
    return maxWorldZ;

}

GLfloat OpenGLRenderer::getWorldX()
{
    return worldX;
}

GLfloat OpenGLRenderer::getWorldY()
{
    return worldY;
}

GeometryEngine *OpenGLRenderer::getGeometryEngine()
{
    return &geometries;
}

