#ifndef VERTEXDATA_H
#define VERTEXDATA_H
#include <QVector2D>
#include <QVector3D>
#include <sstream>
struct VertexData
{
    QVector3D position;
    QVector2D texCoord;
    VertexData(){}
    void setTexCoord(QVector2D t){
        texCoord=t;
    }

    VertexData(QVector3D p,QVector2D t){
        position=p;
        texCoord=t;
    }

    std::string toString(){

        std::ostringstream str;

        str<<"Position : [ "<<position.x()<<","<<position.y()<<","<<position.z()<<"] | Texture : [ "<<texCoord.x()<<","<<texCoord.y()<<"]";
        return str.str();
    }

};





#endif // VERTEXDATA_H


