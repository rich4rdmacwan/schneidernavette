#ifdef GL_ES
// Set default precision to medium
precision mediump int;
precision mediump float;
#endif

uniform sampler2D texture;
uniform bool useColor;

varying vec2 v_texcoord;
varying vec4 vobjectColor;

void main()
{
    // Set fragment color from texture
    if(useColor)
        gl_FragColor = gl_Color;
    else
        gl_FragColor = texture2D(texture, v_texcoord);

}


