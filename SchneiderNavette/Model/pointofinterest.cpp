#include "pointofinterest.h"


QString PointOfInterest::getAddress()
{
    return QString::fromStdString(address);
}

void PointOfInterest::setAddress(QString value)
{
    address = value.toStdString();
}

QString PointOfInterest::getName()
{
    return QString::fromStdString(name);
}

void PointOfInterest::setName(const QString value)
{
    name = value.toStdString();
}

int PointOfInterest::getNearestNodeId()
{
    return nearestNodeId;
}

void PointOfInterest::setNearestNodeId(int value)
{
    nearestNodeId = value;
}

QString PointOfInterest::getInfo()
{
    return QString::fromStdString(info);
}

void PointOfInterest::setInfo(QString value)
{
    info = value.toStdString();
}

QString PointOfInterest::getType()
{
    return QString::fromStdString(type);
}

void PointOfInterest::setType(QString value)
{
    type = value.toStdString();
}

QString PointOfInterest::toString()
{
    QString ret = QString(this->getName());
    return ret;
}
PointOfInterest::PointOfInterest()
{
}
