#include "road.h"

Road::Road()
{
}

Road::Road(std::string roadName, std::vector<Node> contour, std::string roadType, bool oneWay) {
    this->RoadName = roadName;
    this->Contour = contour;
    this->RoadType = roadType;
    this->OneWay = oneWay;
}
