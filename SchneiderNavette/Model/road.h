#ifndef road_H
#define road_H

#include <QString>
#include <vector>
#include "node.h"
#include <boost/serialization/vector.hpp>

class Road
{

    friend class boost::serialization::access;
    template<typename Archive>
    void serialize(Archive &ar, const unsigned int version)
     {
         ar & BOOST_SERIALIZATION_NVP(ID);
         ar & BOOST_SERIALIZATION_NVP(RoadName);
         ar & BOOST_SERIALIZATION_NVP(RoadType);
         ar & BOOST_SERIALIZATION_NVP(OneWay);
         ar & BOOST_SERIALIZATION_NVP(Contour);
    }


public:
    Road();
    Road(std::string roadName,std::vector<Node> contour,std::string roadType, bool oneWay);

    std::string ID;
    std::string RoadName;
    std::vector<Node> Contour;
    std::string RoadType;
    bool OneWay;
};

BOOST_SERIALIZATION_ASSUME_ABSTRACT(Road)

#endif // road_H
