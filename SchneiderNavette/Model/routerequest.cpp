#include "routerequest.h"


int RouteRequest::getRequestType() const
{
    return requestType;
}

void RouteRequest::setRequestType(int value)
{
    requestType = value;
}
RouteRequest::RouteRequest(const PointOfInterest &poi)
{
    this->setPOI(poi);
}

RouteRequest::RouteRequest(const QString &cat)
{
    this->setCategory(cat);
}

void RouteRequest::setPOI(PointOfInterest poi)
{
    this->requestType = 0;
    this->_poi = poi;
}

PointOfInterest RouteRequest::getPOI()
{
    return this->_poi;
}

void RouteRequest::setCategory(QString cat)
{
    this->requestType = 1;
    this->categoryName = cat;
}

QString RouteRequest::getCategory()
{
    return this->categoryName;
}

Node *RouteRequest::getNode()
{
    return _node;
}

void RouteRequest::setNode(Node *myNode)
{
    _node = myNode;
}

QString RouteRequest::toString()
{
    if (this->requestType == 0) { // print POI
        return QString("[ ") +
                QString(this->_poi.getType()) +
                QString(" ] ") +
                QString(this->_poi.getName());
    }
    else {
        return QString("[ Any ") + QString(this->categoryName) + QString(" ]");
    }
}
