#ifndef RAWDATA_H
#define RAWDATA_H

#include <map>
#include <QString>
#include <QVector2D>
#include "node.h"
#include "road.h"
#include "pointofinterest.h"
#include <iostream>
#include <vector>

#include <boost/serialization/map.hpp>


class RawData
{
private:
    std::vector<PointOfInterest> POIs;
    typedef std::map<long, Node> mapNodes;
    std::map<long, Node> Nodes;
    typedef std::map<std::string, Road> mapRoads;
    std::map<std::string, Road> Roads;

        friend class boost::serialization::access;
    template<typename Archive>
    void serialize(Archive &ar, const unsigned int version)
     {
         ar & BOOST_SERIALIZATION_NVP(Nodes);
         ar & BOOST_SERIALIZATION_NVP(Roads);

    }

public:
    RawData();

    std::vector<PointOfInterest> &getPOIs();
    void setPOIs(std::vector<PointOfInterest> &value);
    Node *getNearestNode(QVector2D coords);
    int getNodeIndex (QString databaseId);
    QString getNodeID (int index);


    std::list<Node> roadVertices;
    std::list<int> roadIndices;
std::map<long, Node>& getNodes();
    void setNodes( std::map<long, Node> &value);
    std::map<std::string, Road>& getRoads();
    void setRoads( std::map<std::string, Road> &value);

    long nodeCount;
    int roadCount;
    int poiCount;

};

BOOST_SERIALIZATION_ASSUME_ABSTRACT(RawData)

#endif // RAWDATA_H
