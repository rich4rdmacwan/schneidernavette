#ifndef POINTOFINTEREST_H
#define POINTOFINTEREST_H

#include <QString>
#include <iostream>
#include "node.h"

#include <boost/archive/tmpdir.hpp>
#include <boost/serialization/list.hpp>
#include <boost/archive/xml_iarchive.hpp>
#include <boost/archive/xml_oarchive.hpp>

class PointOfInterest : public Node
{
private:
    std::string address;
    std::string info;
    std::string name;
    std::string type;
    int nearestNodeId;
    //need to include the photos also

    friend class boost::serialization::access;
    template<typename Archive>
    void serialize(Archive &ar, const unsigned int version)
     {
         ar & BOOST_SERIALIZATION_NVP(id);
         //std::string nameStr = name.toStdString();
         ar & BOOST_SERIALIZATION_NVP(name);
         //std::string information = info.toStdString();
         ar & BOOST_SERIALIZATION_NVP(info);
         //std::string category = type.toStdString();
         ar & BOOST_SERIALIZATION_NVP(type);
         //std::string add = address.toStdString();
         ar & BOOST_SERIALIZATION_NVP(address);
         ar & BOOST_SERIALIZATION_NVP(longitude);
         ar & BOOST_SERIALIZATION_NVP(latitude);
    }

public:
    PointOfInterest();
    QString getAddress();
    void setAddress( QString value);
    QString getName();
    void setName( QString value);
    int getNearestNodeId() ;
    void setNearestNodeId(int value);
    QString getInfo() ;
    void setInfo(QString value);
    QString getType();
    void setType(QString value);

    QString toString();
};

BOOST_SERIALIZATION_ASSUME_ABSTRACT(PointOfInterest)

#endif // POINTOFINTEREST_H
