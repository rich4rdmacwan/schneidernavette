#include "rawdata.h"
#include <cmath>

#include <QDebug>

std::vector<PointOfInterest> &RawData::getPOIs()
{
    return POIs;
}

void RawData::setPOIs(std::vector<PointOfInterest> &value)
{
    POIs = value;
}

Node *RawData::getNearestNode(QVector2D coords)
{
    Node *retNode = NULL;
    double lat1 = coords.x();
    double lon1 = coords.y();

    //qDebug() << "Searching for " << lat1 << ", " << lon1 << endl;

    // Iterate through all nodes and find closest one.
    double minDist = -1;
    int i = 0;
    std::map<long, Node>::iterator it = this->Nodes.begin();

    for (;it != this->Nodes.end();it++, i++) {
        Node *myNode = &(it->second);
        double lat2 = myNode->getLatitude();
        double lon2 = myNode->getLongitude();
        double dist = sqrt(pow(lat1-lat2, 2) + pow(lon1-lon2, 2));
        if (minDist == -1 || dist < minDist) {
            minDist = dist;
            retNode = myNode;
        }

        if (i < 5) {
           // qDebug() << "i = " << i << " minDist = " << minDist << endl;
        }
    }

    return retNode;
}


std::map<long, Node> &RawData::getNodes()
{
    return Nodes;
}

void RawData::setNodes(std::map<long, Node> &value)
{
    Nodes = value;
}

std::map<std::string, Road> &RawData::getRoads()
{
    return Roads;
}

void RawData::setRoads(std::map<std::string, Road> &value)
{
    Roads = value;
}
RawData::RawData()
{
}



