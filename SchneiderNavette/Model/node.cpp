#include "node.h"


bool Node::getTentativeVisited() const
{
    return tentativeVisited;
}

void Node::setTentativeVisited(bool value)
{
    tentativeVisited = value;
}

bool Node::getVisited() const
{
    return visited;
}

void Node::setVisited(bool value)
{
    visited = value;
}

bool Node::getStuck() const
{
    return stuck;
}

void Node::setStuck(bool value)
{
    stuck = value;
}
Node::Node()
{
    id=-1;
    visited=false;
    tentativeVisited = false;
    stuck = false;
    heuristicCost=0;
    realCost=0;
}

Node::Node(long id, double lat, double lon) {
    this->id = id;
    this->latitude = lat;
    this->longitude = lon;
    visited=false;
    tentativeVisited = false;
    stuck = false;
    heuristicCost=0;
    realCost=0;
}

double Node::getLatitude()
{
    return latitude;
}

double Node::getLongitude()
{
    return longitude;
}

void Node::setLatitude(double latitude)
{
    this->latitude=latitude;
}

void Node::setLongitude(double longitude)
{
    this->longitude=longitude;
}

long Node::getId()
{
    return id;
}

void Node::setID(long id)
{
    this->id=id;
}
double Node::getHeuristicCost() const
{
    return heuristicCost;
}

void Node::setHeuristicCost(double value)
{
    heuristicCost = value;
}
double Node::getRealCost() const
{
    return realCost;
}

void Node::setRealCost(double value)
{
    realCost = value;
}
std::list<long> &Node::getConnectedWalkNodeIds()
{
    return connectedWalkNodeIds;
}

void Node::setConnectedWalkNodeIds( std::list<long> &value)
{
    connectedWalkNodeIds = value;
}
std::list<long> &Node::getConnectedDriveNodeIds()
{
    return connectedDriveNodeIds;
}

void Node::setConnectedDriveNodeIds(std::list<long> &value)
{
    connectedDriveNodeIds = value;
}




