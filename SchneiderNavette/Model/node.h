#ifndef node_H
#define node_H
#include <list>
#include <QString>
#include <iomanip>
#include <iostream>
#include <fstream>
#include <string>

#include <boost/archive/tmpdir.hpp>
#include <boost/serialization/list.hpp>
#include <boost/archive/xml_iarchive.hpp>
#include <boost/archive/xml_oarchive.hpp>


class Node
{
    bool visited;
    bool tentativeVisited;
    bool stuck;
    double heuristicCost;
    double realCost;
    std::list<long> connectedWalkNodeIds;
    std::list<long> connectedDriveNodeIds;

    friend class boost::serialization::access;
    template<typename Archive>
    void serialize(Archive &ar, const unsigned int version)
     {
         ar & BOOST_SERIALIZATION_NVP(visited);
         ar & BOOST_SERIALIZATION_NVP(longitude);
         ar & BOOST_SERIALIZATION_NVP(latitude);
         ar & BOOST_SERIALIZATION_NVP(id);
         ar & BOOST_SERIALIZATION_NVP(connectedDriveNodeIds);
         ar & BOOST_SERIALIZATION_NVP(connectedWalkNodeIds);
    }

protected:
    long id;
    double latitude;
    double longitude;

public:
    Node();
    Node(long id, double lat, double lon);
    double getLatitude();
    double getLongitude();

    void setLatitude(double latitude);
    void setLongitude(double longitude);

    long getId();
    void setID(long id);

    double getHeuristicCost() const;
    void setHeuristicCost(double value);

    double getRealCost() const;
    void setRealCost(double value);

    std::list<long>& getConnectedWalkNodeIds() ;
    void setConnectedWalkNodeIds(std::list<long> &value);

    std::list<long> &getConnectedDriveNodeIds() ;
    void setConnectedDriveNodeIds(std::list<long> &value);



    bool getTentativeVisited() const;
    void setTentativeVisited(bool value);
    bool getVisited() const;
    void setVisited(bool value);
    bool getStuck() const;
    void setStuck(bool value);
};

BOOST_SERIALIZATION_ASSUME_ABSTRACT(Node)

#endif // node_H
