#ifndef ROUTEREQUEST_H
#define ROUTEREQUEST_H

#include <QString>
#include "pointofinterest.h"
#include "node.h"

class RouteRequest
{
    int requestType; // 0 - POI, 1 - category
    QString categoryName;
    PointOfInterest _poi;
    Node *_node;

public:
    RouteRequest(const PointOfInterest &poi);
    RouteRequest(const QString &cat);

    void setPOI(PointOfInterest poi);
    PointOfInterest getPOI();
    void setCategory(QString cat);
    QString getCategory();
    Node* getNode();
    void setNode(Node* myNode);


    QString toString();
    int getRequestType() const;
    void setRequestType(int value);
};

#endif // ROUTEREQUEST_H
