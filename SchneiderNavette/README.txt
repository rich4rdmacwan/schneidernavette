Schneider Navette

--Build
	You will need to modify include files in the SchneiderNavette.pro to point to the correct path for boost and mysql includes.
	The library paths also need to be adjusted  especially on windows or on unix if mysql and boost are NOT installed in the default locations. (/usr/lib)

--Dependencies
	Qt5.1 : Schneider Navette uses Qt libraries. The latest Qt libraries can be downloaded from 
	http://qt-project.org/downloads
	
	Boost Serialization library: Scheider Navette needs boost serialization library in order to work.
	Unlike most Boost libraries, the serializatoin library needs to built manually. Please refer to the links below for installation notes.
	Windows: http://www.boost.org/doc/libs/1_55_0/more/getting_started/windows.html#or-build-binaries-from-source
	Mac/Unix: http://www.boost.org/doc/libs/1_55_0/more/getting_started/unix-variants.html#or-build-custom-binaries

	Mysql: Schneider Navette does not use mysql directly, however it has capability of adapting to a different city.
	In that case, the data for the new city needs to be entered into a mysql database and that data needs to be read using the
	modified application. Kindly contact the authors if you need to do this.


--Installation
	Unix:
	--Execute the install.sh script
	--Provide root password for install
	--Run SchneiderNavette
	Windows:
	--Unfortately, for windows, SchneiderNavette needs to be built from source. Refer to the Build section.




Authors:
Dina Youakim: dina.youakim@gmail.com
Igor Peric:	  igorperic@live.com
Daniel Barmaimon: danielbarmaimon@gmail.com
Richard Macwan: rich4rd.macwan@gmail.com