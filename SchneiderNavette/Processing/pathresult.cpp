#include "pathresult.h"




double PathResult::getPathDistance()
{
    return pathDistance;
}

void PathResult::setPathDistance(double value)
{
    pathDistance = value;
}

double PathResult::getPathTravelTime()
{
    return pathTravelTime;
}

void PathResult::setPathTravelTime(double value)
{
    pathTravelTime = value;
}
PathResult::PathResult()
{
    nodes.clear();
    pathTravelTime=0;
    pathDistance=0;
}

std::vector<Node > &PathResult::getnodes()
{
    return nodes;
}

void PathResult::setnodes(std::vector<Node> &value)
{
    nodes = value;
}
