#ifndef METRICS_H
#define METRICS_H
#define _USE_MATH_DEFINES
#include "Model/node.h"
#include "cmath"
#include "Model/pointofinterest.h"
class Metrics
{
    static double haversine(double latitude1,double longitude1, double latitude2,double longitude2);
    static double earth_radius;

public:
    Metrics();
    static double distance_between_nodes_in_km(Node& node1,Node& node2);
    static double distance_between_nodes_in_km(PointOfInterest& node1,PointOfInterest& node2);
    static double distance_between_nodes_in_km(PointOfInterest& node1,Node& node2);
    static double toMapCoordX(double longitude);
    static double toMapCoordY(double latitude);
    static double toLatitude(double Ycoord);
    static double toLongitude(double Xcoord);
    static double degrees_to_radians(double degrees);
    static double radians_to_degrees(double radians);
    static void updateVariables(double width, double height);

    static double maxY;
    static double minY;
    static double maxX;
    static double minX;

    static double maxLat;
    static double minLat;
    static double maxLon;
    static double minLon;
    static double ScaleQuotX;
    static double ScaleQuotY;

};

#endif // METRICS_H
