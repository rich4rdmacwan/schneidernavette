#include "metrics.h"
#include "UserInteraction/Rendering/openglrenderer.h"
#include <QDebug>
double Metrics::earth_radius=6371;
double Metrics::maxLat=0;
double Metrics::minLat=0;
double Metrics::maxLon=0;
double Metrics::minLon=0;
double Metrics::ScaleQuotX=0;
double Metrics::ScaleQuotY=0;
double Metrics::maxX=0;
double Metrics::minX=10000;
double Metrics::maxY=0;
double Metrics::minY=10000;

double Metrics::haversine(double latitude1, double longitude1, double latitude2, double longitude2)
{

    double R = earth_radius; // km
    double dLat = fabs(degrees_to_radians(latitude1-latitude2));
    double dLon = fabs(degrees_to_radians(longitude1-longitude2));

    double a = sin(dLat/2) *sin(dLat/2) +
            sin(dLon/2) * sin(dLon/2) * cos(latitude1) * cos(latitude2);
    double c = 2 *  atan2(sqrt(a), sqrt(1-a));
    double d = R * c;
    return d;
}





double Metrics::toMapCoordX(double longitude)
{
    double val=(longitude - minLon) * ScaleQuotX;
    if(val<0){
        qDebug()<<"Metrics : Longitude :"<<longitude<<"-"<<minLon<<" * "<<ScaleQuotX<<"="<<val;
        val=0;
    }

    return val;
}
double Metrics::toMapCoordY(double latitude)
{
    // qDebug()<<"Y : "<<coord<<"-"<<minLat<<"*"<<ScaleQuotY;
   double val=(latitude - minLat) * ScaleQuotY;
    if(val<0){
        qDebug()<<"Metrics: Latitude : "<<latitude<<"-"<<minLat<<","<<val;
        val=0;
    }
    //qDebug()<<val;
    return val;
}

double Metrics::toLatitude(double Ycoord)
{
    double val=Ycoord/ScaleQuotY+minLat;
    return val;
}

double Metrics::toLongitude(double Xcoord)
{
    double val=Xcoord/ScaleQuotX+minLon;
    return val;
}

Metrics::Metrics()
{
}

double Metrics::distance_between_nodes_in_km(Node &node1, Node &node2)
{
        return haversine(node1.getLatitude(),node1.getLongitude(),node2.getLatitude(),node2.getLongitude());
}

double Metrics::distance_between_nodes_in_km(PointOfInterest &node1, PointOfInterest &node2)
{
    return haversine(node1.getLatitude(),node1.getLongitude(),node2.getLatitude(),node2.getLongitude());
}

double Metrics::distance_between_nodes_in_km(PointOfInterest &node1, Node &node2)
{
    return haversine(node1.getLatitude(),node1.getLongitude(),node2.getLatitude(),node2.getLongitude());
}



double Metrics::degrees_to_radians(double angle)
{

    return ((angle) / 180.0 * M_PI);
}

double Metrics::radians_to_degrees(double radians)
{
    return ((radians) * (180.0 / M_PI));
}

//!Update multipliers, and min, max latitudes and longitudes. Needs to be called every time the window is resized
void Metrics::updateVariables(double width,double height)
{
    ScaleQuotX = width/ (maxLon - minLon);
    ScaleQuotY = height/ (maxLat - minLat);

    minX=toMapCoordX(minLon);
    maxX=toMapCoordX(maxLon);
    minY=toMapCoordY(minLat);
    maxY=toMapCoordY(maxLat);

}

