#ifndef PATHFINDERENGINE_H
#define PATHFINDERENGINE_H
#include "pathresult.h"
#include "Model/rawdata.h"
#include "metrics.h"
#include "Model/routerequest.h"
#include <iostream>
#include <math.h>
#include <queue>
#include <stack>

using namespace std;

struct NodesComparator {
    bool operator()( const Node* node1,
    const Node * node2 ) const
    {
      if( node1->getRealCost()+node1->getHeuristicCost() < node2->getRealCost()+node2->getHeuristicCost() )
          return false;
        else
          return true;
    }
};

class PathFinderEngine
{
private:
    RawData mapData;
    std::map< std::string,std::vector<PointOfInterest> > poiMap;
    void reconstructPath(std::map<long, Node *> came_from, Node *node, std::vector<Node> &pathNodes);
    double calculateEuclidienneDistance(double x1, double y1, double x2, double y2);
    double calculateEuclidienneDistance(Node *sourceNode, PointOfInterest *destNode);
    void calculatePathDistanceAndTime (PathResult* pathResult, bool isDrivingMode);

public:
    PathFinderEngine();
    void testComparator();
    void pushNodes();
    void popNodes();
    void resetNodes();
    PathResult* findPath(long startNodeId, long endNodeId, bool isDrivingMode);
    PathResult* findPathThroughPoi(long startNodeId, std::vector<RouteRequest> throughList,long endNodeId, bool isDrivingMode);
    PathResult* findPathThroughPoiWithinDistance(long startNodeId, std::vector<RouteRequest> throughList,long endNodeId,double distance, bool isDrivingMode);
    std::vector<PointOfInterest> findPoiWithinDistance(double distance, std::vector<RouteRequest> through, long nodeid);
    RawData getMapData() const;
    void setMapData(const RawData &value);
    typedef priority_queue<Node *,std::vector<Node*>,NodesComparator> NodesOpenSet;
    NodesOpenSet openSet;
    std::map< std::string,std::vector<PointOfInterest> > getPoiMap() ;
    void convertPoiListIntoMap();
};

#endif // PATHFINDERENGINE_H
