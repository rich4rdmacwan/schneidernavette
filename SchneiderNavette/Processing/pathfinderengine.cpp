#include "pathfinderengine.h"
#include <fstream>
#include <QDebug>

using namespace std;

PathFinderEngine::PathFinderEngine()
{
}

void PathFinderEngine::testComparator()
{
    std::map<long,Node>::iterator it = mapData.getNodes().begin();
    int i =0;
    while(it!=mapData.getNodes().end())
    {
        if(i==10)
            break;
        Node* node = &(it->second);
        node->setRealCost(10-i);
        node->setHeuristicCost(10-i);
        qDebug()<<"ID "<<(it->second).getId()<<" Heuristic value "<<(node->getRealCost()+node->getHeuristicCost());
        i++;
        it++;
    }
}

void PathFinderEngine::pushNodes()
{

    qDebug()<<"data initialized";
     std::map<long,Node>::iterator it1 = mapData.getNodes().begin();
     int i=0;
     qDebug()<<(it1->second).getId();
     while(it1!=mapData.getNodes().end())
     {
         if(i==10)
             break;
         Node * node = &(it1->second);
         openSet.push(node);
         qDebug()<<"push order ID "<<node->getId();
         i++;
         it1++;
     }
}

void PathFinderEngine::popNodes()
{

     int i=0;
     std::map<long,Node>::iterator it2 = mapData.getNodes().begin();
     while(it2!=mapData.getNodes().end())
     {
         if(i==10)
             break;
         Node * node = openSet.top();
         openSet.pop();
         qDebug()<<"top order ID "<<node->getId()<<" Heur "<<node->getRealCost()+node->getHeuristicCost();
         it2++;
         i++;
     }

}

void PathFinderEngine::resetNodes()
{
    std::map<long,Node>::iterator it2 = mapData.getNodes().begin();

    while(it2!=mapData.getNodes().end())
    {
        Node* node=&(it2->second);
        node->setRealCost(0);
        node->setHeuristicCost(0);
        node->setVisited(false);
        node->setTentativeVisited(false);
        node->setStuck(false);
        ++it2;
    }
}



PathResult *PathFinderEngine::findPath(long startNodeId, long endNodeId, bool isDrivingMode)
{
    //Create an openSet as priority queue that will hold nodes to be visitied, initially it has only the start node
    openSet = NodesOpenSet ();
    //Create closedSet that will hold at each step the node with least cost + heuristic
    stack<Node*> closedSet;
    //A map that hold as key a node ID, and as value the node through which we get to this node
    std::map<long,Node*> came_from;
    //List of nodes constructing the shortest path, this is the result that should be sent to the UI for display
    PathResult* result = new PathResult();

    std::list<long>::iterator neighborsIt;
    Node *startNode =  & mapData.getNodes()[startNodeId];
    Node * endNode =  & mapData.getNodes()[endNodeId];
    Node * current;
    Node * neighbor ;
    openSet.push(startNode);
    while (!openSet.empty())
    {
        //current is the node with the smallest total cost among all the possible neighbor movements from the openSet
        current = openSet.top();
        if(current->getStuck())
        {
            continue;
        }
        if(!current->getTentativeVisited() && !current->getVisited())
        {
            current->setTentativeVisited(true);
            //set real cost and heuristic cost of the current node
            current->setRealCost(calculateEuclidienneDistance(startNode->getLatitude(),startNode->getLongitude(),current->getLatitude(),current->getLongitude()));
            current->setHeuristicCost(calculateEuclidienneDistance(current->getLatitude(),current->getLongitude(),endNode->getLatitude(),endNode->getLongitude()));
        }
        if(current->getId()==endNode->getId())
        {
            endNode->setVisited(true);
            endNode->setTentativeVisited(true);
            long keyToLastNode = ((Node*)(came_from.end()->second))->getId();
            came_from[keyToLastNode] = endNode;
            reconstructPath (came_from,endNode,result->getnodes());
            calculatePathDistanceAndTime(result,isDrivingMode);
            return result;
        }
        //Remove current from the openSet and add it to closedSet as it is a candidate node on the shortest path
        openSet.pop();
        closedSet.push(current);
        current->setVisited(true);
        //NOT SURE SHOULD I RESET ALL THE TIME I POP OR NOT
        //current->setTentativeVisited(false);
        double tentativeTotalCost = 0;
        //If the requested path is a driving path, loop on the neighbors of current node that are on the driving path list
        if(isDrivingMode)
        {
            if(current->getConnectedDriveNodeIds().size()!=0)
            {
                neighborsIt = current->getConnectedDriveNodeIds().begin();
                while(neighborsIt!=current->getConnectedDriveNodeIds().end())
                {
                    neighbor = & mapData.getNodes()[*neighborsIt];
                    neighbor->setRealCost(calculateEuclidienneDistance(startNode->getLatitude(),startNode->getLongitude(),neighbor->getLatitude(),neighbor->getLongitude()));
                    neighbor->setHeuristicCost(calculateEuclidienneDistance(neighbor->getLatitude(),neighbor->getLongitude(),endNode->getLatitude(),endNode->getLongitude()));
                    tentativeTotalCost = current->getRealCost()+calculateEuclidienneDistance(current->getLatitude(),current->getLongitude(),neighbor->getLatitude(), neighbor->getLongitude());
                    if(neighbor->getVisited() && tentativeTotalCost>=neighbor->getRealCost())
                    {
                        neighborsIt ++;
                        continue;
                    }
                    if(!neighbor->getTentativeVisited() || tentativeTotalCost<neighbor->getRealCost())
                    {
                         came_from[neighbor->getId()] = current;
                         neighbor->setRealCost(tentativeTotalCost);
                         neighbor->setHeuristicCost(calculateEuclidienneDistance(neighbor->getLatitude(),neighbor->getLongitude(),endNode->getLatitude(),endNode->getLongitude()));

                        if(!neighbor->getTentativeVisited())
                        {
                            neighbor->setTentativeVisited(true);
                            openSet.push(neighbor);
                        }

                    }
                    neighborsIt ++;
                }
            }
            //This node doesn't have any neighbor we need to go back one step through the closed set
            //and check the next lower node.
            else
            {
                ((Node*)closedSet.top())->setStuck(true);
                //need to erase
                std::map<long,Node*>::iterator it;
                for(it = came_from.begin();it!=came_from.end();++it)
                {
                    if (it->second->getId() == current->getId())
                    {
                        came_from.erase(came_from.find(it->first));
                    }
                }
                closedSet.pop();
            }
        }
        else
        {
            if(current->getConnectedWalkNodeIds().size()!=0)
            {
                neighborsIt = current->getConnectedWalkNodeIds().begin();
                while(neighborsIt!=current->getConnectedWalkNodeIds().end())
                {
                    neighbor = & mapData.getNodes()[*neighborsIt];
                    neighbor->setRealCost(calculateEuclidienneDistance(startNode->getLatitude(),startNode->getLongitude(),neighbor->getLatitude(),neighbor->getLongitude()));
                    neighbor->setHeuristicCost(calculateEuclidienneDistance(neighbor->getLatitude(),neighbor->getLongitude(),endNode->getLatitude(),endNode->getLongitude()));
                    tentativeTotalCost = current->getRealCost()+calculateEuclidienneDistance(current->getLatitude(),current->getLongitude(),neighbor->getLatitude(), neighbor->getLongitude());
                    if(neighbor->getVisited() && tentativeTotalCost>=neighbor->getRealCost())
                    {
                        neighborsIt ++;
                        continue;
                    }
                    if(!neighbor->getTentativeVisited() || tentativeTotalCost<neighbor->getRealCost())
                    {
                         came_from[neighbor->getId()] = current;
                         neighbor->setRealCost(tentativeTotalCost);
                         neighbor->setHeuristicCost(calculateEuclidienneDistance(neighbor->getLatitude(),neighbor->getLongitude(),endNode->getLatitude(),endNode->getLongitude()));

                        if(!neighbor->getTentativeVisited())
                        {
                            neighbor->setTentativeVisited(true);
                            openSet.push(neighbor);
                        }

                    }
                    neighborsIt ++;
                }
            }
            //This node doesn't have any neighbor we need to go back one step through the closed set
            //and check the next lower node.
            else
            {
                ((Node*)closedSet.top())->setStuck(true);
                //need to erase
                std::map<long,Node*>::iterator it;
                for(it = came_from.begin();it!=came_from.end();++it)
                {
                    if (it->second->getId() == current->getId())
                    {
                        came_from.erase(came_from.find(it->first));
                    }
                }
                closedSet.pop();
            }
        }

    }

}

PathResult *PathFinderEngine::
findPathThroughPoi(long startNodeId, std::vector<RouteRequest> throughList,long endNodeId, bool isDrivingMode)
{
    PathResult* result=new PathResult();
    PathResult *tempResult=new PathResult();
    Node* currentStartNode = &mapData.getNodes()[startNodeId];
    for(int i=0;i<throughList.size();i++)
    {
        if (((RouteRequest)throughList[i]).getRequestType()==0)
        {
            long destId = ((RouteRequest)throughList[i]).getNode()->getId();
            tempResult = findPath(currentStartNode->getId(),destId,isDrivingMode);
            resetNodes();
            result->getnodes().insert(result->getnodes().end(), tempResult->getnodes().begin(), tempResult->getnodes().end());
            currentStartNode = &mapData.getNodes()[destId];
        }
        else
        {
            //for a category loop through the map, get the closest one of this type, then get the shortest path to it
            std::string currentCat = ((RouteRequest)throughList[i]).getCategory().toStdString();
            std::transform(currentCat.begin(), currentCat.end(), currentCat.begin(), ::tolower);

            std::vector<PointOfInterest> candidatePois = poiMap[currentCat];
            double closestPoiDistance = 1000;
            long closestPoiId = currentStartNode->getId();
            int closestPoiIndex=-1;
            for(int i=0;i<candidatePois.size();i++)
            {
                double currentDistance = calculateEuclidienneDistance(currentStartNode->getLatitude(),currentStartNode->getLongitude(),((Node)candidatePois[i]).getLatitude(),((Node)candidatePois[i]).getLongitude());
                if(currentDistance<closestPoiDistance)
                {
                    closestPoiDistance = currentDistance;
                    closestPoiId = ((PointOfInterest)candidatePois[i]).getId();
                    closestPoiIndex=i;
                }
            }
            Node* closesNodeToPoi = mapData.getNearestNode(QVector2D(((PointOfInterest)candidatePois[closestPoiIndex]).getLatitude(), ((PointOfInterest)candidatePois[closestPoiIndex]).getLongitude()));
            tempResult = findPath(currentStartNode->getId(),closesNodeToPoi->getId(),isDrivingMode);
            resetNodes();
            result->getnodes().insert(result->getnodes().end(), tempResult->getnodes().begin(), tempResult->getnodes().end());
            currentStartNode = closesNodeToPoi;
        }

    }
    if (endNodeId != -1)
    {
        tempResult= findPath(currentStartNode->getId(),endNodeId,isDrivingMode);
        resetNodes();
        result->getnodes().insert(result->getnodes().end(), tempResult->getnodes().begin(), tempResult->getnodes().end());
    }
    calculatePathDistanceAndTime(result,isDrivingMode);
    return result;
}

PathResult *PathFinderEngine::findPathThroughPoiWithinDistance(long startNodeId, std::vector<RouteRequest> throughList, long endNodeId, double distance, bool isDrivingMode)
{
    PathResult* result=new PathResult();
    PathResult *tempResult=new PathResult();
    Node* currentStartNode = &mapData.getNodes()[startNodeId];
    for(int i=0;i<throughList.size();i++)
    {
        if (((RouteRequest)throughList[i]).getRequestType()==0)
        {
            long destId = ((RouteRequest)throughList[i]).getNode()->getId();
            tempResult = findPath(currentStartNode->getId(),destId,isDrivingMode);
            resetNodes();
            result->getnodes().insert(result->getnodes().end(), tempResult->getnodes().begin(), tempResult->getnodes().end());
            currentStartNode = &mapData.getNodes()[destId];
        }
        else
        {
            //for a category loop through the map, get the closest one of this type, then get the shortest path to it
            std::string currentCat = ((RouteRequest)throughList[i]).getCategory().toStdString();
            std::transform(currentCat.begin(), currentCat.end(), currentCat.begin(), ::tolower);

            std::vector<PointOfInterest> candidatePois = poiMap[currentCat];
            double closestPoiDistance = 1000;
            long closestPoiId = currentStartNode->getId();
            int closestPoiIndex=-1;
            for(int i=0;i<candidatePois.size();i++)
            {
                double currentDistance = calculateEuclidienneDistance(currentStartNode->getLatitude(),currentStartNode->getLongitude(),((Node)candidatePois[i]).getLatitude(),((Node)candidatePois[i]).getLongitude());
                if(currentDistance<closestPoiDistance)
                {
                    closestPoiDistance = currentDistance;
                    closestPoiId = ((PointOfInterest)candidatePois[i]).getId();
                    closestPoiIndex=i;
                }
            }
            Node* closesNodeToPoi = mapData.getNearestNode(QVector2D(((PointOfInterest)candidatePois[closestPoiIndex]).getLatitude(), ((PointOfInterest)candidatePois[closestPoiIndex]).getLongitude()));
            tempResult = findPath(currentStartNode->getId(),closesNodeToPoi->getId(),isDrivingMode);
            resetNodes();
            result->getnodes().insert(result->getnodes().end(), tempResult->getnodes().begin(), tempResult->getnodes().end());
            currentStartNode = closesNodeToPoi;
        }

    }
    if (endNodeId != -1)
    {
        tempResult= findPath(currentStartNode->getId(),endNodeId,isDrivingMode);
        resetNodes();
        result->getnodes().insert(result->getnodes().end(), tempResult->getnodes().begin(), tempResult->getnodes().end());
    }
    calculatePathDistanceAndTime(result,isDrivingMode);
    if(result->getPathDistance()<distance)
    {
        return result;
    }
    //if yes just break and go back to the UI and warn the user that no path exists within this distance limitation
    else
    {
        result->getnodes().clear();
        result->setPathDistance(0);
        result->setPathTravelTime(0);
        return result;
    }
}

std::vector<PointOfInterest> PathFinderEngine::findPoiWithinDistance(double distance,std::vector<RouteRequest> through, long nodeid)
{
    Node startNode=mapData.getNodes().at(nodeid);
     double minDistance=0;
     std::vector<PointOfInterest> candidatePois ;
     std::vector<PointOfInterest> actualPois;
     std::vector<PointOfInterest>::iterator poisIt;
     PointOfInterest currentPoi;
    if(through.empty())
    {
        candidatePois = mapData.getPOIs();
        poisIt = candidatePois.begin();
    }
    else
    {
        std::string poiCategory = through[0].getCategory().toStdString();
        std::transform(poiCategory.begin(), poiCategory.end(), poiCategory.begin(), ::tolower);
        candidatePois = poiMap[poiCategory];
         poisIt = candidatePois.begin();
    }

    while(poisIt!=candidatePois.end())
    {
        currentPoi = *poisIt;
        minDistance = Metrics::distance_between_nodes_in_km(currentPoi,startNode);
        if(minDistance<=distance)
            actualPois.push_back(currentPoi);
        poisIt++;
    }
    return actualPois;
}


RawData PathFinderEngine::getMapData() const
{
    return mapData;
}

void PathFinderEngine::setMapData(const RawData &value)
{
    mapData = value;
}


void PathFinderEngine::reconstructPath(std::map<long,Node*> came_from, Node *node, std::vector<Node > &pathNodes)
{
    if (came_from[node->getId()]!=NULL)
    {
        reconstructPath(came_from,came_from[node->getId()],pathNodes);

    }
    else{
       // resetNodes();
    }
    if (node->getVisited() && node->getTentativeVisited())
    {
        //qDebug()<<"current node "<<node->getId()<<" pushed ";
        Node& current = mapData.getNodes()[node->getId()];
        pathNodes.push_back(current);
    }

}

double PathFinderEngine::
calculateEuclidienneDistance(double x1, double y1, double x2, double y2)
{
    return sqrt((pow(x1-x2,2)) + (pow((y1-y2),2)));
}


void PathFinderEngine::calculatePathDistanceAndTime(PathResult *pathResult, bool isDrivingMode)
{
    double totalDistance = 0, totalTravelTime = 0;
    for(int i=0;i<pathResult->getnodes().size()-1;i++)
    {
        totalDistance+= Metrics::distance_between_nodes_in_km(pathResult->getnodes()[i],pathResult->getnodes()[i+1]);
    }
    pathResult->setPathDistance(totalDistance);
    if(isDrivingMode)
    {
        totalTravelTime = (totalDistance/60)*(60);
    }
    else
    {
        totalTravelTime = (totalDistance/5)*(60);
    }
    pathResult->setPathTravelTime(totalTravelTime);
}


std::map< std::string,std::vector<PointOfInterest> > PathFinderEngine::getPoiMap()
{
    return poiMap;
}

void PathFinderEngine::convertPoiListIntoMap()
{
    std::vector<PointOfInterest>::iterator poiVectorIt = mapData.getPOIs().begin();
    PointOfInterest current;
    while (poiVectorIt!= mapData.getPOIs().end())
    {
        current = *poiVectorIt;
        std::string currentCat = current.getType().toStdString();
        std::transform(currentCat.begin(), currentCat.end(), currentCat.begin(), ::tolower);
        std::vector<PointOfInterest> * tempPoiVector = &poiMap[currentCat];
        tempPoiVector->push_back(current);
        poiMap[currentCat] = *tempPoiVector;
        poiVectorIt++;
    }
}
