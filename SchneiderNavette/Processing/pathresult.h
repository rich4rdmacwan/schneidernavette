#ifndef PATHRESULT_H
#define PATHRESULT_H
#include "Model/node.h"
#include <vector>

class PathResult
{
private:
    std::vector<Node> nodes;
    double pathDistance;
    double pathTravelTime;
public:
    PathResult();
    std::vector<Node> &getnodes() ;
    void setnodes( std::vector<Node> &value);
    double getPathDistance() ;
    void setPathDistance(double value);
    double getPathTravelTime();
    void setPathTravelTime(double value);
};

#endif // PATHRESULT_H
