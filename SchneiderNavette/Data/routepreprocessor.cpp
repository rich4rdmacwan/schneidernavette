#include "routepreprocessor.h"
#include <QDataStream>
#include <QFileInfo>
//We need this because boost::serialization does not work well with Qt's resource framework
QString RoutePreprocessor::xmlDataFolder="/home/data/Studies/ViBOT/Sem1/SoftwareEngineeringProject/schneider/SchneiderNavette/Data/";
//QString RoutePreprocessor::xmlDataFolder="/usr/local/SchneiderNavette/Data/";
RoutePreprocessor::RoutePreprocessor()
{
}

void RoutePreprocessor::saveNodes(std::map<long,Node> nodes){
    // make an archive

    std::ofstream ofs_nodes(getAbsolutePath(xmlDataFolder+"nodes.xml").c_str());
    assert(ofs_nodes.good());
    boost::archive::xml_oarchive oaNodes(ofs_nodes);
    oaNodes << BOOST_SERIALIZATION_NVP(nodes);
}

void RoutePreprocessor::saveRoads (std::map<std::string,Road> roads)
{
        std::ofstream ofs_roads(getAbsolutePath(xmlDataFolder+"roads.xml").c_str());
        assert(ofs_roads.good());
        boost::archive::xml_oarchive oaRoads(ofs_roads);
        oaRoads << BOOST_SERIALIZATION_NVP(roads);
}

void RoutePreprocessor::savePOI (std::vector<PointOfInterest> pois)
{
        //qDebug()<<"poi serialization"<<endl;
        std::ofstream ofs_pois(getAbsolutePath(xmlDataFolder+"pois.xml").c_str(),std::fstream::in | std::fstream::trunc);
        assert(ofs_pois.good());
        boost::archive::xml_oarchive oaPois(ofs_pois);
        oaPois << BOOST_SERIALIZATION_NVP(pois);
        qDebug()<<"Finished serialization"<<endl;
}

std::string RoutePreprocessor::getAbsolutePath(QString path)
{
    return (QFileInfo(path)).absoluteFilePath().toStdString();
}

void RoutePreprocessor::loadNodes(std::map<long,Node> &nodes)
{
    // open the archive

qDebug()<<"Loading data from "<<(xmlDataFolder+"nodes.xml");
    std::ifstream ifs_nodes(getAbsolutePath(xmlDataFolder+"nodes.xml").c_str());
    assert(ifs_nodes.good());
    boost::archive::xml_iarchive iaNodes(ifs_nodes);
    iaNodes >> BOOST_SERIALIZATION_NVP(nodes);
}

void RoutePreprocessor::loadRoads(std::map<std::string,Road> &roads)
{
    std::ifstream ifs_roads(getAbsolutePath(xmlDataFolder+"roads.xml").c_str());
    assert(ifs_roads.good());
    boost::archive::xml_iarchive iaRoads(ifs_roads);
    iaRoads >> BOOST_SERIALIZATION_NVP(roads);
}

void RoutePreprocessor::loadPois(std::vector<PointOfInterest> &pois)
{
    std::ifstream ifs_pois(getAbsolutePath(xmlDataFolder+"pois.xml").c_str());
    assert(ifs_pois.good());
    boost::archive::xml_iarchive iaPois(ifs_pois);
    iaPois >> BOOST_SERIALIZATION_NVP(pois);
}

void RoutePreprocessor::process(RawData *rawdata)
{
    std::map<long, Node>::iterator nodeit=rawdata->getNodes().begin();
    std::map<std::string,Road >::iterator roadit=rawdata->getRoads().begin();
    Node prevNode, nextNode,tempNode;
    Node* mainNode;
    Road road;
    bool isDriving=false;
    bool isWalking=false;

    qDebug()<<"nodes size "<<rawdata->getNodes().size()<<endl;
    for(nodeit=rawdata->getNodes().begin();nodeit!=rawdata->getNodes().end();++nodeit){
        //Get the id of this node. We call this the mainNode
        mainNode= &(nodeit->second);

        //qDebug()<<"Processing node : "<<mainNode.ID()<<"----------------------"<<(double(i++)/rawdata->Nodes.size()*100)<<"%";

        //Iterate through all the roads in the database
        for(roadit=rawdata->getRoads().begin();roadit!=rawdata->getRoads().end();++roadit){
            road=  roadit->second;
            //Get all the nodes belonging to this road
            //Iterate through all the nodes of this road
            if (road.RoadType=="primary" || road.RoadType=="primary_link" || road.RoadType =="residential"|| road.RoadType =="secondary"|| road.RoadType =="secondary_link"|| road.RoadType =="service" || road.RoadType =="steps"|| road.RoadType =="tertiary"|| road.RoadType =="teriary_link"|| road.RoadType =="unclassified")
                isDriving=true;
            if (road.RoadType=="footway" || road.RoadType=="path"|| road.RoadType=="pedestrian"|| road.RoadType=="residential"|| road.RoadType=="service" || road.RoadType=="tertiary_link"|| road.RoadType=="track"|| road.RoadType=="unclassified")
                isWalking = true;
           for(int i=0; i<road.Contour.size();i++)
           {
               //std::cout<<"Processing road : "<<road.ID<<"contour size "<<road.Contour.size()<<endl;

                tempNode = road.Contour[i];
                //If the mainNode is the same as the current node in this Contour, return the
                //nodes with previous and current node ids
                if(tempNode.getId()==mainNode->getId())
                {
                    if(i!=0 && !road.OneWay)
                        prevNode = road.Contour[i-1];
                    else
                        prevNode= Node();
                    if(i!=road.Contour.size()-1)
                        nextNode = road.Contour[i+1];
                    else
                        nextNode=Node();
                    if(prevNode.getId()!=-1)
                    {
                        if(isDriving)
                            mainNode->getConnectedDriveNodeIds().push_back(prevNode.getId());
                        }
                        if(isWalking){
                            mainNode->getConnectedWalkNodeIds().push_back(prevNode.getId());
                        }

                    if(nextNode.getId()!=-1)
                    {
                         if(isDriving)
                             mainNode->getConnectedDriveNodeIds().push_back(nextNode.getId());

                         if(isWalking)
                             mainNode->getConnectedWalkNodeIds().push_back(nextNode.getId());
                    }
                    if(road.RoadName=="Rue de la Pierre"){
                        qDebug()<<"road type "<<isDriving<<" and "<<isWalking<<endl;
                        qDebug()<<"MainNode:"<<mainNode->getId();
                        if(prevNode.getId()!=-1)
                            qDebug()<<"Prev Node Id: "<<prevNode.getId();
                        if(nextNode.getId()!=-1)
                            qDebug()<<"Next Node Id:"<<nextNode.getId();
                }
                }

            }
        }
    }
    qDebug()<<"Finished looping start serialization"<<endl;
    RoutePreprocessor::saveNodes(rawdata->getNodes());
    RoutePreprocessor::saveRoads(rawdata->getRoads());
    RoutePreprocessor::savePOI(rawdata->getPOIs());
}

