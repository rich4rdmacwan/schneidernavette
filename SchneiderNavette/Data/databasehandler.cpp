#include "databasehandler.h"

#include <QDebug>
#include <iostream>
#include "Processing/metrics.h"
DatabaseHandler::DatabaseHandler()
{
    connect = mysql_init(NULL);

    if (!connect) {
        qDebug() << "MySQL init failed!" << endl;
    }

    connect = mysql_real_connect(connect,"localhost","root","","bigschneider",0,NULL,0);
    if (connect) {
        qDebug() << "I connected!" << endl;
    }
    else {
        qDebug() << "I didn't connect. :(" << endl;
    }
}

RawData DatabaseHandler::LoadRawDataFromDB() {

    GetAllNodes();

    GetAllRoads();

    LinkRoadsAndNodes();

    LoadPOIs();

    return returnDataSet;
}


void DatabaseHandler::GetAllRoads() {

    char stdQuery[] = "select * from road";

    int num_fields, i;
    returnDataSet.roadCount=0;
    if(mysql_real_query(connect, stdQuery, strlen(stdQuery)) == 0)/*success*/
    {
        result = mysql_store_result(connect);
        if (result)  // there are rows
        {
            returnDataSet.getRoads().clear();
            num_fields = mysql_num_fields(result);

            while (row = mysql_fetch_row(result))
            {
                Road myRoad;
                // construct road
                myRoad.ID = ((QString)row[0]).toStdString();
                myRoad.RoadName = ((QString)row[1]).toStdString();
                myRoad.RoadType = ((QString)row[2]).toStdString();
                std::string oneWayStr = row[3];
                (std::transform(oneWayStr.begin(), oneWayStr.end(), oneWayStr.begin(), ::tolower));
                if (oneWayStr == "true")
                    myRoad.OneWay = true;
                else
                    myRoad.OneWay = false;


                returnDataSet.getRoads()[myRoad.ID] = myRoad;
            }
            mysql_free_result(result);

        }
    }
}

void DatabaseHandler::GetAllNodes() {

    char stdQuery[] = "select * from node";

    int num_fields;
    int index=0;



    if(mysql_real_query(connect, stdQuery, strlen(stdQuery)) == 0)/*success*/
    {
        result = mysql_store_result(connect);

        if (result)  // there are rows
        {

            returnDataSet.getNodes().clear();
            num_fields = mysql_num_fields(result);
            returnDataSet.nodeCount=mysql_num_rows(result);
            //nodes is a float array of the form x1,y1,z1,x2,y2,z2...xn,yn,zn
            //z value would be set to zero initially.

            double minLatitude=100;
            double maxLatitude=0;
            double minLongitude=100;
            double maxLongitude=0;
            //Pointers to the edge nodes. Will need later for displaying scaling information
            Node minLatNode,minLongNode,maxLatNode,maxLongNode;

            while ((row = mysql_fetch_row(result)))
            {

                Node node;
                // construct node
                node.setID(((QString)row[0]).toLong());

                node.setLatitude(atof(row[1]));
                node.setLongitude(atof(row[2]));
                //qDebug()<<"Lat="<<vertices[index]<<",Lon="<<vertices[index+1]<<"Z="<<vertices[index+2];

                //Update max and min latitudes and longitude values
                if(minLatitude > node.getLatitude()){
                    minLatitude=node.getLatitude();
                    minLatNode=node;
                }

                if(minLongitude > node.getLongitude()){
                    minLongitude=node.getLongitude();
                    minLongNode=node;
                }

                if(maxLatitude < node.getLatitude()){
                    maxLatitude=node.getLatitude();
                    maxLatNode=node;
                }
                if(maxLongitude < node.getLongitude()){
                    maxLongitude=node.getLongitude();
                    maxLongNode=node;
                }
                Metrics::minLat=minLatitude;
                Metrics::maxLat=maxLatitude;
                Metrics::minLon=minLongitude;
                Metrics::maxLon=maxLongitude;
    //double horizontalExtent=Metrics::distance_between_nodes_in_km(maxLongNode,minLongNode);
    //double verticalExtent=Metrics::distance_between_nodes_in_km(maxLatNode,minLatNode);

             // insert node
                returnDataSet.getNodes()[node.getId()] = node;

            }
            mysql_free_result(result);
            qDebug()<<"MinLatitude: "<<minLatitude;
            qDebug()<<"MaxLatitude: "<<maxLatitude;
            qDebug()<<"MinLongitude: "<<minLongitude;
            qDebug()<<"MaxLongitude: "<<maxLongitude;
        }
    }

    qDebug()<<"Number of rows : "<<returnDataSet.nodeCount<<",vertices[] size : "<<index;
}

DatabaseHandler::~DatabaseHandler() {
    mysql_close(connect);
}

void DatabaseHandler::LinkRoadsAndNodes() {

    // with ordering included
    char stdQuery[] = "select * from road_node order by roadId,ContourOrder asc";

    int roadIndex=0;
    std::string roadId,prevRoadId="";
    int totalRoadIndexCount=0;

    if(mysql_real_query(connect, stdQuery, strlen(stdQuery)) == 0)/*success*/
    {
        result = mysql_store_result(connect);
        if (result)  // there are some rows
        {
            int currentRoadIndexCount=0;
            while ((row = mysql_fetch_row(result))) {
                Node *targetNode = &returnDataSet.getNodes()[((QString)row[2]).toLong()];
                Road *targetRoad = &returnDataSet.getRoads()[((QString)row[1]).toStdString()];
                roadId=targetRoad->ID;

                targetRoad->Contour.push_back(*targetNode); // TODO: TRY SWITCHING TO POINTER FOR PERFORMANCE INCREASE
                returnDataSet.roadVertices.push_back(*targetNode);

                //For the first node of a road, this condition will be false.
                //For every other node, we enter the previous and current index.
                if(prevRoadId==roadId){
            //       std::cout<<"("<<roadIndex-1<<","<<roadIndex<<")";
                returnDataSet.roadIndices.push_back(roadIndex-1);
                returnDataSet.roadIndices.push_back(roadIndex);
                currentRoadIndexCount=targetRoad->Contour.size();
                }
                else{
//                    qDebug()<<currentRoadIndexCount;
  //                  qDebug()<<targetRoad->RoadName;

                }
                //Update prevRoadId
                prevRoadId=roadId;
                ++roadIndex;

            }
            //std::cout<<std::endl;
        }
        mysql_free_result(result);
    }
    //qDebug()<<"Last roadIndex="<<roadIndex;
    //qDebug()<<"Road Indice count:"<<roadIndices.size();

}

void DatabaseHandler::LoadPOIs()
{
    qDebug()<<"loading POI"<<endl;
    char stdQuery[] = "select ID,Latitude,Longitude,address,Information,Type,Name,picturepath  from poi";
        int num_fields;

        if(mysql_real_query(connect, stdQuery, strlen(stdQuery)) == 0)/*success*/
        {
            result = mysql_store_result(connect);

            if (result)  // there are rows
            {

                returnDataSet.getPOIs().clear();
                returnDataSet.poiCount=mysql_num_rows(result);
                num_fields = mysql_num_fields(result);

                while ((row = mysql_fetch_row(result)))
                {

                    PointOfInterest poi;
                    // construct poi
                    poi.setID(((QString)row[0]).toInt());
                    poi.setLatitude(atof(row[1]));
                    poi.setLongitude(atof(row[2]));
                    poi.setAddress(row[3]);
                    poi.setInfo(row[4]);
                    poi.setType(row[5]);
                    poi.setName(row[6]);
                    returnDataSet.getPOIs().push_back(poi);
                }
                mysql_free_result(result);
            }
        }
        qDebug()<<"Number of POIs : "<<returnDataSet.poiCount<<endl;
        qDebug()<<"Number of POIs in list : "<<returnDataSet.getPOIs().size()<<endl;
}






