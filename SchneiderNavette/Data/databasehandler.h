#ifndef DATABASEHANDLER_H
#define DATABASEHANDLER_H

#if defined _WIN32 || defined _WIN64
#include <Winsock2.h>
#endif
#include <mysql.h>

#include <list>

#include "Model/rawdata.h"
#include "Model/node.h"
#include "Model/road.h"
#include "UserInteraction/Rendering/openglrenderer.h"
class DatabaseHandler
{

public:
    DatabaseHandler();
    ~DatabaseHandler();

    RawData LoadRawDataFromDB();

private:

    MYSQL *connect;

    MYSQL_RES *result;
    MYSQL_ROW row;

    void GetAllNodes();
    void GetAllRoads();
    void LinkRoadsAndNodes();
    void LoadPOIs();

    RawData returnDataSet;

};

#endif // DATABASEHANDLER_H
