#ifndef ROUTEPREPROCESSOR_H
#define ROUTEPREPROCESSOR_H
#include "Model/rawdata.h"
#include <map>
#include <QDebug>
class RoutePreprocessor
{
public:
    RoutePreprocessor();
    static void process(RawData* rawdata);
    static void loadRoads(std::map<std::string,Road> &roads);
    static void loadPois(std::vector<PointOfInterest> &pois);
    static void loadNodes(std::map<long,Node> &nodes);
    static void saveRoads(std::map<std::string,Road> roads);
    static void saveNodes(std::map<long,Node> nodes);
    static void savePOI (std::vector<PointOfInterest> pois);
    static QString xmlDataFolder;
    static std::string getAbsolutePath(QString path);
};

#endif // ROUTEPREPROCESSOR_H
